<?php

namespace App\Providers;

use App\Models\Hak_akses;
use App\Models\Menu;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use View;
use Auth;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
    * Register any application services.
    *
    * @return void
    */
    public function register()
    {
        //
    }
    
    /**
    * Bootstrap any application services.
    *
    * @return void
    */
    public function boot()
    {
        Paginator::useBootstrap();
        
        View::composer('*', function($view)
        {
            if(Auth::id() == 1){
                $view->with('menu', Menu::where('status','1')->orderBy('modul_id','asc')->orderBy('urutan','asc')->get());
            }else{
                $menu = Hak_akses::where('user_id',Auth::id())->get();
                $view->with('menu', Menu::whereIn('id',explode(',',$menu->pluck('menu_id')->implode(',')))->orderBy('modul_id','asc')->orderBy('urutan','asc')->get());
            }
            $view->with('settings', Setting::get()->toArray());
        });
    }
}
