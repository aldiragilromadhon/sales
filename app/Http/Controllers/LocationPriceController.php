<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\LocationPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationPriceController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('location_price.index', ['location' => Location::all()]);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $request->validate(
                ['location_from_id' => 'required'],
                ['location_to_id' => 'required']
            );
            
            $location = LocationPrice::updateOrCreate(['id'=> $request->id_location_price],[
                'location_from'    => $request->location_from_id,
                'location_to'      => $request->location_to_id,
                'parameter'        => $request->parameter_location,
                'weight'           => $request->weight_location,
                'price'            => $request->price_location
            ]);
            
            if($location->wasRecentlyCreated){
                $message = 'tambah';
            }else{
                $message = 'ubah';
            }
            return response()->json(['code'=>200, 'message'=>$message], 200);
        }
    }
    
    public function store_detail(Request $request)
    {
        if ($request->ajax()) {
            $request->validate(
                ['location_from_id_detail' => 'required'],
                ['location_to_id_detail' => 'required']
            );
            
            $location = LocationPrice::updateOrCreate(['id'=> $request->id_location_price],[
                'location_from'    => $request->location_from_id_detail,
                'location_to'      => $request->location_to_id_detail,
                'parameter'        => $request->parameter_location_detail,
                'weight'           => $request->weight_location_detail,
                'price'            => $request->price_location_detail
            ]);
            
            if($location->wasRecentlyCreated){
                $message = 'tambah';
            }else{
                $message = 'ubah';
            }
            return response()->json(['code'=>200, 'message'=>$message], 200);
        }
    }
    
    public function list(Request $request)
    {
        if ($request->ajax()) {
            
            $location = LocationPrice::select(array('location_prices.*', DB::raw('COUNT(*) as JUMLAH')))
            ->groupBy('location_from','location_to')->get();
            $data = array();
            $no = 0;
            foreach ($location as $s) {
                $row = array();
                $row [] = $s->location_from;
                $row [] = $s->location_to;
                $row [] = ++$no;
                $row [] = $s->asal->name;
                $row [] = $s->tujuan->name;
                $row [] = $s->JUMLAH;
                $data[] = $row;
            }
            return response()->json(array("data"=>$data));
        }
    }
    
    public function list_detail($from,$to)
    {
        $location = LocationPrice::whereRaw('location_from = '.$from.' AND location_to = '.$to)->get();
        $no = 0;
        $row = '';
        foreach ($location as $s) {
            $row .= '<tr>';
            $row .= '<td>'.++$no.'</td>';
            $row .= '<td>'.$s->parameter.'</td>';
            $row .= '<td>'.$s->weight.'</td>';
            $row .= '<td>'.$s->price.'</td>';
            $row .= '</tr>';
        }
        return response()->json($row);
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Location  $location
    * @return \Illuminate\Http\Response
    */
    public function show($from,$to)
    {
        $location = LocationPrice::
        select('location_prices.*', DB::raw('asal.name asal'),DB::raw('tujuan.name tujuan'))->
        join('locations as asal', 'asal.id', '=', 'location_prices.location_from')->
        join('locations as tujuan', 'tujuan.id', '=', 'location_prices.location_to')->
        whereRaw('location_from = '.$from.' AND location_to = '.$to)->first();
        return response()->json(['status'=>($location === null?false:true), 'data'=>$location], 200);
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Location  $location
    * @return \Illuminate\Http\Response
    */
    public function edit(LocationPrice $location)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Location  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, LocationPrice $location)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Location  $location
    * @return \Illuminate\Http\Response
    */
    public function destroy($from,$to)
    {
        return response()->json(['code'=>200, 'message'=> LocationPrice::whereRaw('location_from = '.$from.' AND location_to = '.$to)->delete()], 200);
    }
}
