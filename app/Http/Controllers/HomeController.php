<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index()
    {
        return view('home', [
            'transaksi' => Transaction::select(DB::raw('SUM(IF(tipe_id=1,total_transaksi,0)) BELI, SUM(IF(tipe_id=2,total_transaksi,0)) JUAL '))->
            where('status',1)->where(DB::raw('MONTH(tgl_transaksi)'),date('m'))->first(),
            'products' => Product::select(DB::raw('products.*, SUM(COALESCE(tjual.qty,0)) TOTAL'))->
            leftJoin(DB::raw('(SELECT
            product_id,
            SUM( quantity ) qty 
            FROM
            `transaction_products` 
            JOIN transactions ON transactions.id=transaction_products.transaction_id
            WHERE
            transaction_products.`tipe_id` = 2 
            AND transaction_products.`status` = 1 
            AND transaction_products.`deleted_at` IS NULL 
            AND MONTH ( transactions.tgl_transaksi )= "'.date('m').'" 
            GROUP BY
            product_id) AS tjual'),function($join){
                $join->on('products.id', '=', 'tjual.product_id');
            })->
            groupBy('products.id')->
            get()
        ]);
    }
}
