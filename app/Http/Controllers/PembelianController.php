<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\Transaction_product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use PDF;


class PembelianController extends Controller
{
    public function index()
    {
        return view('pembelian.index',[
            'periode' => Transaction::select(DB::raw("DATE_FORMAT(tgl_transaksi, '%Y-%m') tanggal"),DB::raw('YEAR(tgl_transaksi) year, DATE_FORMAT(tgl_transaksi,"%M") month'))
            ->orderby("tanggal","DESC")
            ->groupby('year','month')
            ->get(),
            'hutang' => Transaction::select(DB::raw('SUM(sisa_tagihan) AS TOTAL'))->where('tipe_id',1)->where('status',0)->first(),
            'lunas' => Transaction::select(DB::raw('SUM(total_transaksi) AS TOTAL'))->where('tipe_id',1)->where('status',1)->first()
        ]);
    }
    
    public function detail($id)
    {
        $params = [
            'transaction' => Transaction::find($id)
        ];
        return view('pembelian.detail', $params);
    }
    
    public function create()
    {
        $params = [
            'product' => Product::all(),
            'contact'=> Contact::where('tipe_id','LIKE','%Supplier%')->get(),
        ];
        return view('pembelian.create', $params);
    }
    
    public function confirm($id)
    {
        $pembelian = Transaction::find($id);
        $pembelian->status = 1;
        $pembelian->save();
        Transaction_product::where('transaction_id', $id)->update(['status' => 1]);
        return redirect('pembelian')->with('success', 'Data pembelian berhasil di terima');
    }
    
    public function destroy($id){
        $pembelian = Transaction::find($id);
        $pembelian->delete();
        return redirect('pembelian')->with('success', 'Data pembelian berhasil di hapus');
    }
    
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pembeliansupplier' => 'required',
            'productpembelian'=> 'required'
        ]);
        $year   = date('Y', strtotime($request->pembeliantanggal));
        $nomor  = Transaction::withTrashed()->whereYear('tgl_transaksi',$year)->orderBy('id', 'desc')->first();
        if(!$nomor){
            $Setting = Setting::get()->toArray();
            $nomor = $Setting[3]['value'];
        }else{
            $nomor_temp	= explode('/',$nomor->no_transaksi);
            $nomor = (integer)$nomor_temp[0] + 1;
        }
        $pembelian = new Transaction();
        $pembelian->tipe_id         = 1;
        $pembelian->contact_id      = $request->pembeliansupplier;
        $pembelian->email           = $request->pembelianemail;
        $pembelian->alamat          = $request->pembelianalamatsupplier;
        $pembelian->alamat_kirim    = $request->pembelianalamatpengiriman;
        $pembelian->user_id         = Auth::id();
        $pembelian->no_transaksi    = sprintf("%03d", $nomor).'/BAS/'.date('d/m/Y', strtotime($request->pembeliantanggal));
        $pembelian->no_referensi    = $request->pembeliannomorreferensi;
        $pembelian->tgl_transaksi   = date('Y-m-d', strtotime($request->pembeliantanggal));
        $pembelian->tgl_jatuh_tempo = date('Y-m-d', strtotime($request->pembelianjatuhtempo));
        $pembelian->nilai_pemotong  = $request->pembelianpemotongan;
        $pembelian->uang_muka       = $request->pembelianuangmuka;
        $pembelian->sisa_tagihan    = $request->pembeliansisatagihan;
        $pembelian->total_transaksi = $request->pembeliansubtotal;
        $pembelian->pesan           = $request->pembelianpesan;
        $pembelian->memo            = $request->pembelianmemo;
        $pembelian->user            = $request->pembelianuser;
        $pembelian->status          = ($request->pembeliansisatagihan == 0 ? 1 : 0);
        $pembelian->approve         = (Auth::id() == 1 ? 1 : 0);
        $pembelian->save();
        for ($i=0; $i < count($request->productpembelian); $i++) { 
            if ($request->productpembelian[$i]) {
                $product[] = [
                    'transaction_id' => $pembelian->id,
                    'tipe_id'=> 1,
                    'product_id' => $request->productpembelian[$i],
                    'unit_id'=> '1',
                    'status' => '0',
                    'diskon' => 0,
                    'pajak'=> 0,
                    'quantity' => round(str_replace(",",".",$request->productquantity[$i]),2),
                    'price'=> round(str_replace(",",".",str_replace(['.','Rp '],"",$request->productharga[$i])),2),
                    'total'=> round(str_replace(",",".",str_replace(['.','Rp '],"",$request->producttotal[$i])),2),
                ];
            }
        }
        Transaction_product::insert($product);
        return response()->json(['code'=>200, 'id'=>$pembelian->id, 'message'=>'Sukses'], 200);
        // return redirect()->route('pembelian')->with('success', 'Data berhasil ditambahkan');
    }
    
    
    public function datatable(Request $req)
    {
        if($req->tanggal){
            $tanggal = $req->tanggal;
        }else{
            $tanggal = date('Y-m');
        }
        $transaction = Transaction::where('tipe_id','1')->where(function ($query) use ($tanggal) {
            $query->whereRaw('DATE_FORMAT(tgl_transaksi,"%Y-%m") = "'.$tanggal.'"')->orWhere('status', '=', 0)->orWhere('approve', '=', 0);
        });
        // if(Auth::id() == 1){
            $transaction = $transaction->get();
        // }else{
            // $transaction = $transaction->where('user_id',Auth::id())->get();
        // }
        
        $data = array();
        foreach ($transaction as $s) {
            $row = array();
            $row [] = $s->id;
            $row [] = '<p class="text-center m-0">'.$s->tgl_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->no_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->contact->perusahaan.' ['.$s->contact->nama.']</p>';
            $row [] = '<div class="badge badge-square badge-'.($s->approve == 0 ? 'danger"><i class="la la-clock-o font-small-3"></i> Pending' : ($s->status == 0 ? 'warning">Open' : 'success">Lunas')).'</div>';
            $row [] = '<p class="text-center m-0">'.number_format($s->status == 0 ? $s->sisa_tagihan : 0,0,',','.').'</p>';
            $row [] = '<p class="text-center m-0">'.number_format($s->total_transaksi,0,',','.').'</p>';
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
    }
    
    
    public function view_pdf($id)
    {
        $pembelian= Transaction::find($id);
        $pdf= PDF::setOptions(['enable_remote' => true,'chroot'=> public_path('/')])->loadview('pembelian.pdf',['pembelian' => $pembelian,'terbilang' => $this->terbilang($pembelian->total_transaksi)]);
        return $pdf->stream('Purchase_order_'.str_replace(' ', '_', $pembelian->contact->perusahaan).'_'.date('d_F_Y').'-'.time().'.pdf');
        // return view('pembelian.pdf',['pembelian' => $pembelian]);
    }
    
    public static function terbilang($nominal)
    {
        $nominal = abs($nominal);
        $angka = array(	'', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 
        'tujuh', 'delapan',	'sembilan',	'sepuluh', 'sebelas');
        
        if ($nominal < 12) {
            return ' '.$angka[$nominal];
        }
        
        if ($nominal < 20) {
            return static::terbilang($nominal - 10). ' belas';
        }
        
        if ($nominal < 100) {
            return static::terbilang($nominal / 10). ' puluh'
            . static::terbilang($nominal % 10);
        }
        
        if ($nominal < 200) {
            return ' seratus'. static::terbilang($nominal - 100);
        }
        
        if ($nominal < 1000) {
            return static::terbilang($nominal / 100). ' ratus'
            . static::terbilang($nominal % 100);
        }
        
        if ($nominal < 2000) {
            return ' seribu'. static::terbilang($nominal - 1000);
        }
        
        if ($nominal < 1000000) {
            return static::terbilang($nominal / 1000). ' ribu' 
            . static::terbilang($nominal % 1000);
        }
        
        if ($nominal < 1000000000) {
            return static::terbilang($nominal / 1000000). ' juta' 
            . static::terbilang($nominal % 1000000);
        }
        
        if ($nominal < 1000000000000) {
            return static::terbilang($nominal / 1000000000). ' milyar'
            . static::terbilang(fmod($nominal, 1000000000));
        }
        
        if ($nominal < 1000000000000000) {
            return static::terbilang($nominal / 1000000000000). ' trilyun'
            . static::terbilang(fmod($nominal, 1000000000000));
        }		
    }
    
    
}
