<?php

namespace App\Http\Controllers;

use App\Models\Emp;
use App\Models\Hak_akses;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Hash;
use Crypt;
use Mail;
use Str;
use DB;
use File;

class UserController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('user.index');
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function profile()
    {
        $user = User::find(Auth::id());
        return view('user.profile',['user'=>$user]);
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param\Illuminate\Http\Request$request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $password = Str::random(8);
        $token      = Str::random(64);
        $request->validate([
            'name_user'=> 'required',
            'email_user' => 'required|email|unique:users,email',
        ]);
        
        DB::beginTransaction();
        $user = new User();
        $user->name= $request->name_user;
        $user->email = $request->email_user;
        $user->password= Hash::make($password);
        $user->save();
        if ($request->tipe_user == 1) {
            $menu = [1,3,4,5,6,7,8,9,10,11,12];
        }else{
            $menu = [1,3,4,5];
        }
        
        for ($i=0; $i < count($menu); $i++) { 
            $hak_akses[] = [
                'user_id' => $user->id,
                'menu_id' => $menu[$i],
            ];
        }
        Hak_akses::insert($hak_akses);
        
        DB::table('password_resets')->insert([
            'email' => $request->email_user,
            'token' => $token, 
            'created_at' => Carbon::now()
        ]);
        
        Mail::send('user.newPassword', ['token' => $token,'password' => $password,'data' => $request], function($message) use($request){
            $message->to($request->email_user);
            $message->subject('New Account');
        });
        
        DB::commit();
        return response()->json(['code'=>200, 'message'=>'Tambah'], 200);
        
    }
    
    public function datatable()
    {
        $user = User::orderByDesc('id')->get();
        $data = array();
        $no = 0;
        foreach ($user as $s) {
            $no++;
            $row = array();
            $row [] = $no;
            $row [] = $s->name;
            $row [] = '<p class="text-center">'.$s->email.'</p>';
            if ($s->id == '1') {
                $row [] = '';
            }else{
                $row [] = '
                <div class="btn-group text-center">
                <button type="button" class="btn btn-sm btn-icon btn-warning secondary" onclick="getData('."'".Crypt::encryptString($s->id)."'".')" title="Edit Kategori"><i class="la la-edit"></i></button>
                <button type="button" class="btn btn-sm btn-icon btn-danger secondary" onclick="deleteData('."'".Crypt::encryptString($s->id)."'".')" title="Hapus Kategori"><i class="la la-remove"></i></button>
                </div>
                ';
            }
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
        // return Datatables::of($data)->make(true);
    }
    
    public function show($id)
    {
        return response()->json(User::with('emp')->find(Crypt::decryptString($id)));
    }
    
    public function edit($id)
    {
    }
    
    public function update(Request $request)
    {
        $fileName = NULL;
        
        $rules = [
            'jabatan_id' => 'required|numeric',
            'atasan_id'=> 'required|numeric',
            'role_user'=> 'required',
            'alias_user' => 'required',
            'name_user'=> 'required',
            'phone_user' => 'required|numeric'
        ];
        
        if($request->hasfile('file_user')) {
            $rules['file_user'] = 'mimes:jpeg,jpg,png|max:2048';
        }
        
        $employee = Emp::find($request->id_emp);
        if ($request->nip_user <> $employee->nip) {
            $rules['nip_user'] = 'required|unique:emps,nip';
        }else{
            $rules['nip_user'] = 'required';
        }
        
        $user = User::find($request->id_user);
        if ($request->email_user <> $user->email) {
            $rules['email_user'] = 'required|unique:users,email';
        }else{
            $rules['email_user'] = 'required';
        }
        
        $request->validate($rules,[
            'nip_user.unique' => 'NIP yang dimasukkan sudah terpakai',
            'email_user.unique' => 'EMAIL yang dimasukkan sudah terpakai'
        ]);
        
        if($request->hasfile('file_user')) {
            File::delete(public_path('images/uploads/'.$employee->file));
            $fileName = time().'_tanda_tangan_'.$request->nip_user;
            $request->file_user->move('images/uploads',$fileName);
        }
        
        $employee->jabatan_id= $request->jabatan_id;
        $employee->atasan_id = $request->atasan_id;
        $employee->nip = $request->nip_user;
        $employee->alias = $request->alias_user;
        $employee->name= $request->name_user;
        $employee->email = $request->email_user;
        $employee->phone_no= $request->phone_user;
        $employee->file= $fileName;
        $employee->save();
        
        $user->name= $request->name_user;
        $user->email = $request->email_user;
        $user->role= $request->role_user;
        $user->save();
        return response()->json(['code'=>200, 'message'=>'Ubah'], 200);
        
    }
    
    public function updateProfile(Request $request)
    {
        $rules = [
            'id_emp_user' => 'required|numeric',
            'id_user_profile' => 'required|numeric',
            'name_user' => 'required',
            'alias_user'=> 'required',
            'email_user'=> 'required|email',
            'phone_user'=> 'required|numeric'
        ];
        
        $employee = Emp::find($request->id_emp_user);
        if ($request->nip_user <> $employee->nip) {
            $rules['nip_user'] = 'required|unique:emps,nip';
        }else{
            $rules['nip_user'] = 'required';
        }
        
        $request->validate($rules,[
            'nip_user.unique' => 'NIP yang dimasukkan sudah terpakai'
        ]);
        
        $employee->nip = $request->nip_user;
        $employee->alias = $request->alias_user;
        $employee->name= $request->name_user;
        $employee->email = $request->email_user;
        $employee->phone_no= $request->phone_user;
        $employee->save();
        
        $user = Emp::find(Auth::id());
        $user->name = $request->name_user;
        $user->save();
        
        $auth = Auth::user();
        $auth->name = $request->name_user;
        $auth->save();
        
        return response()->json(['code'=>200, 'message'=>'Ubah'], 200);
        
    }
    
    public function updatePassword(Request $request)
    {
        $request->validate([
            'id_user_password' => 'required|numeric',
            'new_password_user' => 'required|min:8',
            'confirm_password_user' => 'required|same:new_password_user'
        ]);
        
        $user = User::find(Auth::id());
        $user->password= Hash::make($request->new_password_user);
        $user->save();
        
        return response()->json(['code'=>200, 'message'=>'Ubah'], 200);
        
    }
    
    public function destroy($id)
    {
        $user = User::find(Crypt::decryptString($id));
        $employee = Emp::find($user->emp_id);
        
        File::delete(public_path('images/uploads/'.$employee->file));
        $employee->delete();
        
        $result = $user->delete();
        return response()->json(['code'=>200, 'message'=> $result], 200);
    }
}
