<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Location;
use App\Models\Pengiriman;
use App\Models\RentTruck;
use App\Models\Pengiriman_barang;
use App\Models\Setting;
use App\Models\Surat_jalan;
use App\Models\Surat_jalan_barang;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class AngkutanController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('angkutan.index',[
            'periode' => Pengiriman::select(DB::raw("DATE_FORMAT(tgl_transaksi, '%Y-%m') tanggal"),  DB::raw('YEAR(tgl_transaksi) year, DATE_FORMAT(tgl_transaksi,"%M") month'))->orderBy(DB::raw("DATE_FORMAT(tgl_transaksi, '%Y-%m')"))
            ->groupby('year','month')
            ->get()
        ]);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('angkutan.create', [
            'location'      => Location::all(),
            'contact'       => Contact::where('tipe_id','LIKE','%Pelanggan%')->get(),
            'penjualan'     => Transaction::where('tipe_id',2)->whereNotIn('no_transaksi', function($q){
                $q->select('no_parent')->from('pengirimen')->whereNull('deleted_at');
            })->get()
        ]);
    }
    
    public function detail($id)
    {
        $barang      = Pengiriman::select(DB::raw('
        pengiriman_barangs.id, 
        pengiriman_barangs.produk, 
        SUM(COALESCE(surat_jalan_barangs.tonase,0)) tonase, 
        SUM(COALESCE(surat_jalan_barangs.karung,0)) karung,
        pengiriman_barangs.harga,
        (SUM(COALESCE(surat_jalan_barangs.tonase,0))*pengiriman_barangs.harga) total
        '))
        ->join('pengiriman_barangs', 'pengiriman_barangs.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalans', 'surat_jalans.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalan_barangs', 'surat_jalan_barangs.surat_jalan_id', '=', 'surat_jalans.id')
        ->where('pengirimen.id',$id)
        ->groupBy('pengiriman_barangs.id')
        ->get();
        
        $params = [
            'angkutan'   => Pengiriman::find($id),
            'truck'        => RentTruck::all(),
            'suratjalan'   => $barang
        ];
        return view('angkutan.detail', $params);
    }
    
    public function get_parent(Request $request)
    {
        return Transaction::select('*',DB::raw('DATE_FORMAT(tgl_po, "%d-%m-%Y") as tanggal'))->where('no_transaksi',$request->nomor)->first();
    }
    
    public function check_inv($id)
    {
        if (Pengiriman::where('id',$id)->whereNull('no_parent')->first()) {
            return true;
        } else {
            return false;
        }
    }

    public function check_pay($id)
    {
        if (Pengiriman::where('id',$id)->whereNull('no_bill')->first()) {
            return false;
        } else {
            return true;
        }
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'angkutancustomer'            => 'required',
            'angkutanalamatangkutan'    => 'required',
            'angkutanproduk'              => 'required',
        ]);
        $nomor = Pengiriman::withTrashed()->whereYear('tgl_transaksi',date('Y', strtotime($request->angkutantanggal)))->orderBy('id', 'desc')->first();
        if(!$nomor){
            $Setting = Setting::get()->toArray();
            $nomor = $Setting[3]['value'];
        }else{
            $nomor_temp	= explode('/',$nomor->no_transaksi);
            $nomor = (integer)$nomor_temp[0] + 1;
        }
        
        $pengiriman = new Pengiriman();
        $pengiriman->no_transaksi        = sprintf("%03d", $nomor).'/BAS-DLV/'.date('d/m/Y', strtotime($request->angkutantanggal));
        // $pengiriman->no_parent           = $request->angkutanparent;
        $pengiriman->no_referensi        = $request->angkutannomorreferensi;
        $pengiriman->no_rekening_bill    = $request->angkutanrekening;
        $pengiriman->atas_nama           = $request->angkutanatasnama;
        $pengiriman->nama_bank           = $request->angkutanbank;
        $pengiriman->rute                = $request->angkutanrute;
        $pengiriman->contact_id          = $request->angkutancustomer;
        $pengiriman->detail_location_from= $request->angkutanalamatangkutan;
        $pengiriman->detail_location_to  = $request->angkutanalamatpenerimaan;
        $pengiriman->subtotal            = round($request->angkutansubtotal,2);
        $pengiriman->potongan            = round($request->angkutanpemotongan,2);
        $pengiriman->pajak               = round($request->angkutanpajak,2);
        $pengiriman->tipe_pajak          = $request->angkutantipepajak;
        $pengiriman->total_transaksi     = round($request->angkutantotal,2);
        $pengiriman->tgl_po              = date('Y-m-d', strtotime($request->angkutantanggalpo));
        $pengiriman->tgl_transaksi       = date('Y-m-d', strtotime($request->angkutantanggal));
        $pengiriman->user_id             = Auth::id();
        $pengiriman->user                = $request->angkutanuser;
        $pengiriman->status              = 0;
        $pengiriman->approve              = (Auth::id() == 1 ? 1 : 0);
        $pengiriman->save();
        for ($i=0; $i < count($request->angkutanproduk); $i++) { 
            if ($request->angkutanproduk[$i]) {
                $product[] = [
                    'pengiriman_id' => $pengiriman->id,
                    'produk'        => $request->angkutanproduk[$i],
                    'tonase'        => round(str_replace(",",".",str_replace(['.','Rp '],"",$request->angkutantonase[$i])),2),
                    'karung'        => round(str_replace(",",".",str_replace(['.','Rp '],"",$request->angkutankarung[$i])),2),
                    'harga'         => round(str_replace(",",".",str_replace(['.','Rp '],"",$request->angkutanprodukprice[$i])),2),
                    'total'         => round(str_replace(",",".",str_replace(['.','Rp '],"",$request->angkutanproduktotal[$i])),2),
                ];
            }
        }        
        Pengiriman_barang::insert($product);
        
        return response()->json(['code'=>200, 'id'=>$pengiriman->id, 'message'=>'Sukses'], 200);
        
    }
    public function pembayaran(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'idangkutannobill'    => 'required',
            'angkutannobill'      => 'required',
            'angkutantanggalbill' => 'required'
        ]);
        
        $pengiriman = Pengiriman::find($request->idangkutannobill);
        $pengiriman->no_bill = $request->angkutannobill;
        $pengiriman->tgl_bill         = date('Y-m-d', strtotime($request->angkutantanggalbill));
        $pengiriman->save();
        
        return response()->json(['code'=>200, 'id'=>$pengiriman->id, 'message'=>'Sukses'], 200);
        
    }
    
    public function confirm($id)
    {
        $pengiriman = Pengiriman::find($id);
        $pengiriman->status = 1;
        $pengiriman->save();
        return redirect('angkutan')->with('success', 'Pengiriman berhasil di selesaikan');
    }
    
    public function destroy($id)
    {
        $pengiriman = Pengiriman::find($id);
        $surat_jalan = Surat_jalan::where('pengiriman_id', $pengiriman->id)->get();
        foreach ($surat_jalan as $sj) {
            surat_jalan_barang::where('surat_jalan_id', $sj->id)->delete();
        }
        Pengiriman_barang::where('pengiriman_id', $pengiriman->id)->delete();
        $surat_jalan = Surat_jalan::where('pengiriman_id', $pengiriman->id)->delete();
        $pengiriman->delete();
        return redirect('angkutan')->with('success', 'Data pengiriman berhasil di hapus');
        
    }
    
    public function list(Request $req)
    {
        if($req->tanggal){
            $tanggal = $req->tanggal;
        }else{
            $tanggal = date('Y-m');
        }

        $pengiriman = Pengiriman::with('contact')->where(function ($query) use ($tanggal) {
            $query->whereNull('no_parent');
            $query->whereRaw('DATE_FORMAT(tgl_transaksi,"%Y-%m") = "'.$tanggal.'"');
        });
        // if(Auth::id() == 1){
            $pengiriman = $pengiriman->get();
        // }else{
            // $pengiriman = $pengiriman->where('user_id',Auth::id())->get();
        // }
        $data = array();
        // $no = 0;
        foreach ($pengiriman as $s) {
            // $no++;
            $row = array();
            $row [] = $s->id;
            $row [] = '<p class="text-center m-0">'.$s->tgl_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->no_referensi.'</p>';
            $row [] = ($s->no_parent?'<div class="badge text-center badge-success">'.$s->no_parent.'</div>':$s->no_transaksi);
            $row [] = '<p class="text-center m-0">'.$s->contact->nama.' | '.$s->contact->perusahaan.'</p>';
            $row [] = '<div class="badge text-center badge-'.($s->approve == 0 ? 'danger"><i class="la la-clock-o font-small-3"></i> Pending' : ($s->status == 0 ? 'warning">Open' : 'success">Lunas')).'</div>';
            $row [] = '<p class="text-center m-0">'.number_format($s->total_transaksi,0,',','.').'</p>';
            // $row [] = 
            // '<div class="text-center">
            // <button type="button" class="btn btn-sm btn-icon btn-warning secondary" onclick="getData('."'".($s->id)."'".')" title="Edit Lokasi"><i class="la la-edit"></i></button>
            // <button type="button" class="btn btn-sm btn-icon btn-danger secondary" onclick="deleteData('."'".($s->id)."'".')" title="Hapus Lokasi"><i class="la la-remove"></i></button>
            // </div>';
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
    }
    
    public function list_barang($id){
        $pengiriman = Pengiriman::find($id);
        $suratjalan = Surat_jalan::where('pengiriman_id',$id)->orderByDesc('id');
        $pengiriman_temp	= explode('/',($pengiriman->no_parent?$pengiriman->no_parent:$pengiriman->no_transaksi));
        $nomor              = $suratjalan->count();
        $result             = "";
        $no = 0;
        foreach ($pengiriman->barang as $s) {
            $result.='<tr>'.
            '<td>'.(++$no).'</td>'.
            '<td>'.$s->produk.'</td>'.
            '<td>
            <input type="text" class="form-control class-quantity" id="surat_jalan_barang_id" name="surat_jalan_barang_id[]" value="'.$s->id.'" hidden>
            <input type="text" class="form-control class-quantity text-right" id="surat_jalan_tonase" name="surat_jalan_tonase[]" value="0" style="min-width: 170px;"></td>'.
            '<td><input type="text" class="form-control class-quantity text-right" id="surat_jalan_karung" name="surat_jalan_karung[]" value="0" style="min-width: 170px;"></td>';
        }
        // return $result;
        // return response()->json(array("data"=>$result));
        return [
            "id"=>'SJ-'.sprintf("%03d", ($nomor+1)).'/'.$pengiriman_temp[0].'/'.$pengiriman_temp[1].'/'.date('d/m/Y'), 
            "data"=>$result
        ];
    }
    
    public function list_barang_edit($id){
        $suratjalan = Surat_jalan::select('*',DB::raw('DATE_FORMAT(tgl_transaksi, "%d-%m-%Y") as tanggal'))->find($id);
        $result     = "";
        $no = 0;
        foreach ($suratjalan->barang as $s) {
            $result.='<tr>'.
            '<td>'.(++$no).'</td>'.
            '<td>'.$s->pengiriman_barang->produk.'</td>'.
            '<td>
            <input type="text" class="form-control class-quantity" id="surat_jalan_barang_id" name="surat_jalan_barang_id[]" value="'.$s->pengiriman_barang_id.'" hidden>
            <input type="text" class="form-control class-quantity text-right" id="surat_jalan_tonase" name="surat_jalan_tonase[]" value="'.$s->tonase.'" style="min-width: 170px;"></td>'.
            '<td><input type="text" class="form-control class-quantity text-right" id="surat_jalan_karung" name="surat_jalan_karung[]" value="'.$s->karung.'" style="min-width: 170px;"></td>';
        }
        // return $result;
        // return response()->json(array("data"=>$result));
        return [
            "id"=>$suratjalan,
            "data"=>$result
        ];
    }
    
    public function view_pdf($id)
    {
        $pengiriman  = Pengiriman::find($id);
        $barang      = Pengiriman::select(DB::raw('
        pengiriman_barangs.id, 
        pengiriman_barangs.produk, 
        SUM(COALESCE(surat_jalan_barangs.tonase,0)) tonase, 
        SUM(COALESCE(surat_jalan_barangs.karung,0)) karung,
        pengiriman_barangs.harga,
        (SUM(COALESCE(surat_jalan_barangs.tonase,0))*pengiriman_barangs.harga) total
        '))
        ->join('pengiriman_barangs', 'pengiriman_barangs.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalans', 'surat_jalans.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalan_barangs', 'surat_jalan_barangs.surat_jalan_id', '=', 'surat_jalans.id')
        ->where('pengirimen.id',$id)
        ->groupBy('pengiriman_barangs.id')
        ->get();
        $total = 0;
        if($barang->count() > 0){
            foreach($barang as $b){
                $total += $b->total;
            }
            $total = round($total-($pengiriman->potongan*$total/100)-($pengiriman->pajak*$total/100));
        }
        $pdf        = PDF::setOptions(['enable_remote' => true,'chroot'  => public_path('/')])->loadview('angkutan.pdf',[
            'pengiriman' => $pengiriman,
            'barang' => $barang,
            'terbilang' => $this->terbilang($total)
        ]);
        return $pdf->stream('Invoice_'.str_replace(' ', '_', $pengiriman->contact->perusahaan).'_'.date('d_F_Y').'-'.time().'.pdf');
        exit(0);
    }    
    
    public function view_pdf_do($id)
    {
        $pengiriman  = Pengiriman::find($id);
        $pdf        = PDF::setOptions(['enable_remote' => true,'chroot'  => public_path('/')])->loadview('angkutan.pdf_do',['pengiriman' => $pengiriman]);
        return $pdf->stream('DO_'.str_replace(' ', '_', $pengiriman->contact->perusahaan).'_'.date('d_F_Y').'.pdf');
        exit(0);
    }    

    public function view_pdf_payment($id)
    {
        $pengiriman  = Pengiriman::find($id);
        $barang      = Pengiriman::select(DB::raw('
        pengiriman_barangs.id, 
        pengiriman_barangs.produk, 
        SUM(COALESCE(surat_jalan_barangs.tonase,0)) tonase, 
        SUM(COALESCE(surat_jalan_barangs.karung,0)) karung,
        pengiriman_barangs.harga,
        (SUM(COALESCE(surat_jalan_barangs.tonase,0))*pengiriman_barangs.harga) total
        '))
        ->join('pengiriman_barangs', 'pengiriman_barangs.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalans', 'surat_jalans.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalan_barangs', 'surat_jalan_barangs.surat_jalan_id', '=', 'surat_jalans.id')
        ->where('pengirimen.id',$id)
        ->groupBy('pengiriman_barangs.id')
        ->get();
        $total = 0;
        if($barang->count() > 0){
            foreach($barang as $b){
                $total += $b->total;
            }
            $total = round($total-($pengiriman->potongan*$total/100)-($pengiriman->pajak*$total/100));
        }
        $pdf        = PDF::setOptions(['enable_remote' => true,'chroot'  => public_path('/')])->loadview('angkutan.pdf_payment',[
            'pengiriman' => $pengiriman,
            'barang' => $barang,
            'terbilang' => $this->terbilang($total)
        ]);
        return $pdf->stream('BPT_'.str_replace(' ', '_', $pengiriman->contact->perusahaan).'_'.date('d_F_Y').'.pdf');
        exit(0);
    }    
    
    public function view_pdf_sj($id)
    {
        $suratjalan  = Surat_jalan::find($id);
        $pdf        = PDF::setOptions(['enable_remote' => true,'chroot'  => public_path('/')])->loadview('angkutan.pdf_sj',['suratjalan' => $suratjalan]);
        return $pdf->stream('SJ_'.str_replace(' ', '_', $suratjalan->pengiriman->contact->perusahaan).'_'.date('d_F_Y').'-'.time().'.pdf');
        exit(0);
    }    
    
    public function view_pdf_rekap($id)
    {
        $suratjalan  = Surat_jalan::select(DB::raw("
        surat_jalans.id,
        DATE_FORMAT(surat_jalans.tgl_transaksi,'%w') tanggal,
        DATE_FORMAT(surat_jalans.tgl_transaksi,'%d/%m/%Y') tgl_transaksi,
        surat_jalans.no_transaksi,
        no_parent,
        no_polisi,
        SUM( COALESCE(tonase,0) ) tonase,
        SUM( COALESCE(karung,0) ) karung
        "))->
        join('pengirimen', 'pengirimen.id', '=', 'surat_jalans.pengiriman_id')->
        join('surat_jalan_barangs', 'surat_jalan_barangs.surat_jalan_id', '=', 'surat_jalans.id')->
        join('rent_trucks', 'rent_trucks.id', '=', 'surat_jalans.truck_id')->
        where('surat_jalans.pengiriman_id',$id)->
        groupBy('surat_jalans.id')->
        orderBy('surat_jalans.tgl_transaksi','asc')->
        orderBy('surat_jalans.id','asc')->
        get();
        $pdf        = PDF::setOptions(['enable_remote' => true,'chroot'  => public_path('/')])->loadview('angkutan.pdf_rekap',['suratjalan' => $suratjalan]);
        return $pdf->stream('Rekap_'.'_'.date('d_F_Y').'-'.time().'.pdf');
        exit(0);
        // return view('angkutan.pdf_rekap',['suratjalan' => $suratjalan]);
    }    
    
    public static function terbilang($nominal)
    {
        $nominal = abs($nominal);
        $angka = array(	'', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 
        'tujuh', 'delapan',	'sembilan',	'sepuluh', 'sebelas');
        
        if ($nominal < 12) {
            return ' '.$angka[$nominal];
        }
        
        if ($nominal < 20) {
            return static::terbilang($nominal - 10). ' belas';
        }
        
        if ($nominal < 100) {
            return static::terbilang($nominal / 10). ' puluh'
            . static::terbilang($nominal % 10);
        }
        
        if ($nominal < 200) {
            return ' seratus'. static::terbilang($nominal - 100);
        }
        
        if ($nominal < 1000) {
            return static::terbilang($nominal / 100). ' ratus'
            . static::terbilang($nominal % 100);
        }
        
        if ($nominal < 2000) {
            return ' seribu'. static::terbilang($nominal - 1000);
        }
        
        if ($nominal < 1000000) {
            return static::terbilang($nominal / 1000). ' ribu' 
            . static::terbilang($nominal % 1000);
        }
        
        if ($nominal < 1000000000) {
            return static::terbilang($nominal / 1000000). ' juta' 
            . static::terbilang($nominal % 1000000);
        }
        
        if ($nominal < 1000000000000) {
            return static::terbilang($nominal / 1000000000). ' milyar'
            . static::terbilang(fmod($nominal, 1000000000));
        }
        
        if ($nominal < 1000000000000000) {
            return static::terbilang($nominal / 1000000000000). ' trilyun'
            . static::terbilang(fmod($nominal, 1000000000000));
        }		
    }
    
}
