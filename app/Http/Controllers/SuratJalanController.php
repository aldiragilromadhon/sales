<?php

namespace App\Http\Controllers;

use App\Models\Surat_jalan;
use App\Models\Surat_jalan_barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SuratJalanController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $request->validate([
                'no_pengiriman_surat_jalan' => 'required',
                'no_do_surat_jalan' => 'required|unique:surat_jalans,no_transaksi',
                'tanggal_surat_jalan' => 'required'
            ]);
            
            $surat_jalan = Surat_jalan::updateOrCreate(['id' => $request->id_surat_jalan],[
                'pengiriman_id' => $request->no_pengiriman_surat_jalan,
                'tipe' => $request->tipe_surat_jalan,
                'kontainer' => $request->kontainer_surat_jalan,
                'truck_id' => $request->plat_surat_jalan,
                'no_transaksi' => $request->no_do_surat_jalan,
                'tgl_transaksi' => date('Y-m-d', strtotime($request->tanggal_surat_jalan))
            ]);
            Surat_jalan_barang::where('surat_jalan_id', $request->id_surat_jalan)->delete();
            
            for ($i=0; $i < count($request->surat_jalan_barang_id); $i++) { 
                $barang[] = [
                    'surat_jalan_id' => $surat_jalan->id,
                    'pengiriman_barang_id' => $request->surat_jalan_barang_id[$i],
                    'tonase'        => round($request->surat_jalan_tonase[$i]?$request->surat_jalan_tonase[$i]:0,2),
                    'karung'        => round($request->surat_jalan_karung[$i]?$request->surat_jalan_karung[$i]:0,2)
                ];
            }        
            Surat_jalan_barang::insert($barang);
            
            
            if($surat_jalan->wasRecentlyCreated){
                $message = 'tambah';
            }else{
                $message = 'ubah';
            }
            return response()->json(['code'=>200, 'message'=>$message], 200);
            
        }
    }
    
    public function list($id){
        $data = Surat_jalan::select('surat_jalans.*', DB::raw('SUM(surat_jalan_barangs.tonase) As tonase'), DB::raw('SUM(surat_jalan_barangs.karung) As karung'))
        ->leftJoin('surat_jalan_barangs', 'surat_jalan_barangs.surat_jalan_id', '=', 'surat_jalans.id')
        ->where('surat_jalans.pengiriman_id', $id)
        ->groupBy('surat_jalans.id')
        ->get();
        $result = "";
        $tonase = 0;
        $karung = 0;
        foreach ($data as $d) {
            $result.='<tr>'.
            '<td>'.$d->id.'</td>'.
            '<td>'.$d->no_transaksi.'</td>'.
            '<td class="text-center">'.date_format(date_create($d->tgl_transaksi),'d F Y').'</td>'.
            '<td class="text-center">'.($d->truck?$d->truck->no_polisi:$d->kontainer).'</td>'.
            '<td class="text-right">'.(number_format($d->tonase,3,',','.')).'</td>'.
            '<td class="text-right">'.(number_format($d->karung,0,',','.')).'</td>'.
            '</tr>';
            $tonase += $d->tonase;
            $karung += $d->karung;
        }
        $total = '<tr>'.
        '<th colspan="4" class="text-right">Total</th>'.
        '<td class="text-right"><b>'.number_format($tonase,3,',','.').'</b></td>'.
        '<td class="text-right"><b>'.number_format($karung,0,',','.').'</b></td>'.
        '</tr>';

        return ["list"=>$result, "total"=>$total];
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\surat_jalan  $surat_jalan
    * @return \Illuminate\Http\Response
    */
    public function show(surat_jalan $surat_jalan)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\surat_jalan  $surat_jalan
    * @return \Illuminate\Http\Response
    */
    public function edit(surat_jalan $surat_jalan)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Surat_jalan  $surat_jalan
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, surat_jalan $surat_jalan)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\surat_jalan  $surat_jalan
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        Surat_jalan::find($id)->delete();
        Surat_jalan_barang::where('surat_jalan_id',$id)->delete();

        return response()->json(['code'=>200, 'message'=> true], 200);
    }
}
