<?php

namespace App\Http\Controllers;

use App\Models\RentTruck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;

class RentTruckController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('rent_truck.index');
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        //
        
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
        $request->validate([
            'perusahaan'        => 'required',
            'no_polisi'         => 'required',
        ]);
        $nama_file = '-';
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $nama_file = time()."_".$file->getClientOriginalName();
            $file->move(public_path('images'),$nama_file);
        }
        $rentTruck              = new RentTruck();
        $rentTruck->perusahaan  = $request->perusahaan;
        $rentTruck->merek       = $request->merek;
        $rentTruck->no_polisi   = $request->no_polisi;
        $rentTruck->no_hp       = $request->no_hp;
        $rentTruck->tahun       = $request->tahun;
        $rentTruck->tgl_sewa    = date('Y-m-d', strtotime($request->tgl_sewa));
        $rentTruck->tgl_berakhir =  date('Y-m-d', strtotime($request->tgl_berakhir));
        $rentTruck->status      = 0;
        $rentTruck->foto        = $nama_file;
        
        return response()->json(['code'=>200, 'message'=>$rentTruck->save()], 200);
    }
    
    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = RentTruck::all();
            return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Models\RentTruck  $rentTruck
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $rt = RentTruck::find($id);
        return response()->json(['status'=>($rt === null?false:true), 'data'=>$rt], 200);
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\RentTruck  $rentTruck
    * @return \Illuminate\Http\Response
    */
    public function edit(RentTruck $rentTruck)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\RentTruck  $rentTruck
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, RentTruck $rentTruck)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\RentTruck  $rentTruck
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return response()->json(['code'=>200, 'message'=> RentTruck::find($id)->delete()], 200);
    }
}
