<?php

namespace App\Http\Controllers;

use App\Models\pengiriman_barang;
use Illuminate\Http\Request;

class PengirimanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pengiriman_barang  $pengiriman_barang
     * @return \Illuminate\Http\Response
     */
    public function show(pengiriman_barang $pengiriman_barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pengiriman_barang  $pengiriman_barang
     * @return \Illuminate\Http\Response
     */
    public function edit(pengiriman_barang $pengiriman_barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pengiriman_barang  $pengiriman_barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pengiriman_barang $pengiriman_barang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pengiriman_barang  $pengiriman_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(pengiriman_barang $pengiriman_barang)
    {
        //
    }
}
