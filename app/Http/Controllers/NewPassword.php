<?php

namespace App\Http\Controllers;

use App\Models\Password_reset;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;

class NewPassword extends Controller
{
    public function validation($token = '')
    {
        // dd($token);
        $user = Password_reset::where('token', $token)->first();
        if ($user) {
            return view('auth.passwords.new', compact('user'));
        }
        return redirect()->route('login')->with('failed', 'Password reset link is expired');
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password'
        ]);
        
        $user = User::where('email', $request->email)->first();
        if ($user) {
            $user['password'] = Hash::make($request->password);
            $user->save();
            Password_reset::where('token',$request->token)->delete();
            return redirect()->route('login')->with('success', 'Success! password has been created');
        }
        return back()->with('failed', 'Terjadi kesalahan');
    }
}
    