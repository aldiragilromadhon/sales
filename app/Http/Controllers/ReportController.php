<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Transaction_product;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Pengiriman;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('report.index',[
            'periode'=>Transaction::select(DB::raw("DATE_FORMAT(tgl_transaksi, '%Y-%m') tanggal"),  DB::raw('YEAR(tgl_transaksi) year, DATE_FORMAT(tgl_transaksi,"%M") month'))
            ->groupby('year','month')
            ->get()
        ]);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function list_laba_rugi(Request $request)
    {
        $tanggal = explode(' - ',$request->tanggal);
        $tgl_mulai_temp = explode('/',$tanggal[0]);
        $tgl_akhir_temp = explode('/',$tanggal[1]);
        $tgl_mulai = $tgl_mulai_temp[2].'-'.$tgl_mulai_temp[1].'-'.$tgl_mulai_temp[0];
        $tgl_akhir = $tgl_akhir_temp[2].'-'.$tgl_akhir_temp[1].'-'.$tgl_akhir_temp[0];

        $total_pembelian    = Transaction::select(DB::raw('SUM(total_transaksi) AS TOTAL'))->whereRaw('transactions.tgl_transaksi BETWEEN "'.$tgl_mulai.'" AND "'.$tgl_akhir.'"')->where('tipe_id',1)->where('status',1)->first();
        $total_penjualan    = Transaction::select(DB::raw('SUM(total_transaksi) AS TOTAL'))->whereRaw('transactions.tgl_transaksi BETWEEN "'.$tgl_mulai.'" AND "'.$tgl_akhir.'"')->where('tipe_id',2)->where('status',1)->first();
        $total_pengiriman   = Pengiriman::select(DB::raw('(SUM(COALESCE(surat_jalan_barangs.tonase,0))*pengiriman_barangs.harga) TOTAL'))
        ->join('pengiriman_barangs', 'pengiriman_barangs.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalans', 'surat_jalans.pengiriman_id', '=', 'pengirimen.id')
        ->leftJoin('surat_jalan_barangs', 'surat_jalan_barangs.surat_jalan_id', '=', 'surat_jalans.id')
        ->whereRaw('pengirimen.tgl_transaksi BETWEEN "'.$tgl_mulai.'" AND "'.$tgl_akhir.'"')
        ->first();

        $pembelian = Transaction::select('products.name',DB::raw('SUM(total) AS TOTAL_PMB'))
        ->join('transaction_products', 'transactions.id', '=', 'transaction_products.transaction_id')
        ->join('products', 'products.id', '=', 'transaction_products.product_id')
        ->whereRaw('transactions.tgl_transaksi BETWEEN "'.$tgl_mulai.'" AND "'.$tgl_akhir.'"')
        // ->whereRaw('DATE_FORMAT(transactions.tgl_transaksi,"%Y-%m") = "'.$request->tanggal.'"')
        ->where('transaction_products.tipe_id',1)->where('transactions.status',1)->groupBy('transaction_products.product_id')->get();
        $detail_pembelian = '';

        foreach ($pembelian as $pmb) {
            $detail_pembelian .= '<li class="list-group-item">'.$pmb->name.'<div class="float-right">Rp '.number_format($pmb->TOTAL_PMB,2).'</div></li>';
        }
        
        $penjualan = Transaction::select('products.name',DB::raw('SUM(total) AS TOTAL_PNJ'))
        ->join('transaction_products', 'transactions.id', '=', 'transaction_products.transaction_id')
        ->join('products', 'products.id', '=', 'transaction_products.product_id')
        ->whereRaw('transactions.tgl_transaksi BETWEEN "'.$tgl_mulai.'" AND "'.$tgl_akhir.'"')
        // ->whereRaw('DATE_FORMAT(transactions.tgl_transaksi,"%Y-%m") = "'.$request->tanggal.'"')
        ->where('transaction_products.tipe_id',2)->where('transactions.status',1)->groupBy('transaction_products.product_id')->get();
        $detail_penjualan = '';
        foreach ($penjualan as $pnj) {
            $detail_penjualan .= '<li class="list-group-item">'.$pnj->name.'<div class="float-right">Rp '.number_format($pnj->TOTAL_PNJ,2).'</div></li>';
        }
        
        return [
            'pembelian'         => $detail_pembelian,
            'penjualan'         => $detail_penjualan,
            'total_pembelian'   => number_format($total_pembelian->TOTAL,2),
            'total_penjualan'   => number_format($total_penjualan->TOTAL,2),
            'laba'              => number_format($total_penjualan->TOTAL-($total_pengiriman->TOTAL+$total_pembelian->TOTAL),2),
            'pengiriman'        => number_format($total_pengiriman->TOTAL,2)
        ];
    }
    
    public function list_penjualan(Request $request)
    {
        $transaction = Transaction::where('tipe_id','2')->whereRaw('DATE_FORMAT(created_at,"%Y-%m") = "'.$request->tanggal.'"')->get();
        $data = array();
        foreach ($transaction as $s) {
            $row = array();
            $row [] = '<p class="text-center m-0">'.date('d/m/Y', strtotime($s->tgl_transaksi)).'</p>';
            $row [] = '<p class="text-center m-0">'.$s->no_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->contact->perusahaan.' ['.$s->contact->nama.']</p>';
            $row [] = '<div class="badge center badge-'.($s->status == 0 ? 'warning">Open' : 'success">Lunas').'</div>';
            $row [] = nl2br(e($s->memo));
            $row [] = '<p class="text-right m-0">'.number_format($s->total_transaksi,2).'</p>';
            $row [] = '<p class="text-right m-0">'.number_format($s->status == 0 ? $s->sisa_tagihan : 0,2).'</p>';
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
    }
    
    public function list_pembelian(Request $request)
    {
        $transaction = Transaction::where('tipe_id','1')->whereRaw('DATE_FORMAT(created_at,"%Y-%m") = "'.$request->tanggal.'"')->get();
        $data = array();
        foreach ($transaction as $s) {
            $row = array();
            $row [] = '<p class="text-center m-0">'.date('d/m/Y', strtotime($s->tgl_transaksi)).'</p>';
            $row [] = '<p class="text-center m-0">'.$s->no_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->contact->perusahaan.' ['.$s->contact->nama.']</p>';
            $row [] = '<div class="badge center badge-'.($s->status == 0 ? 'warning">Open' : 'success">Lunas').'</div>';
            $row [] = nl2br(e($s->memo));
            $row [] = '<p class="text-right m-0">'.number_format($s->total_transaksi,2).'</p>';
            $row [] = '<p class="text-right m-0">'.number_format($s->status == 0 ? $s->sisa_tagihan : 0,2).'</p>';
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }

    public function datatable_pembelian(){
         $transaction = Transaction::where('tipe_id','1')->get();
        $data = array();
        // $no = 0;
        foreach ($transaction as $s) {
            // $no++;
            $row = array();
            $row [] = $s->id;
            $row [] = '<p class="text-center m-0">'.$s->tgl_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->no_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->contact->perusahaan.' ['.$s->contact->nama.']</p>';
            $row [] = '<p class="text-center m-0">'.number_format($s->total_transaksi,2).'</p>';
            // $row [] = 
            // '<div class="text-center">
            // <button type="button" class="btn btn-sm btn-icon btn-warning secondary" onclick="getData('."'".($s->id)."'".')" title="Edit Lokasi"><i class="la la-edit"></i></button>
            // <button type="button" class="btn btn-sm btn-icon btn-danger secondary" onclick="deleteData('."'".($s->id)."'".')" title="Hapus Lokasi"><i class="la la-remove"></i></button>
            // </div>';
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
    }

     public function datatable_penjualan(){
        $transaction = Transaction::where('tipe_id','2')->get();
        $data = array();
        // $no = 0;
        foreach ($transaction as $s) {
            // $no++;
            $row = array();
            $row [] = $s->id;
            $row [] = '<p class="text-center m-0">'.$s->tgl_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->no_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->contact->nama.'</p>';
            $row [] = '<p class="text-center m-0">'.number_format($s->total_transaksi,2).'</p>';
            // $row [] = 
            // '<div class="text-center">
            // <button type="button" class="btn btn-sm btn-icon btn-warning secondary" onclick="getData('."'".($s->id)."'".')" title="Edit Lokasi"><i class="la la-edit"></i></button>
            // <button type="button" class="btn btn-sm btn-icon btn-danger secondary" onclick="deleteData('."'".($s->id)."'".')" title="Hapus Lokasi"><i class="la la-remove"></i></button>
            // </div>';
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
    }
}
