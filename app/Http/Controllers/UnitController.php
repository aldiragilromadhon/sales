<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('unit.index');
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $request->validate(['nama_unit' => 'required']);
            
            $location = Unit::updateOrCreate(
                ['id' => $request->id_unit], 
                ['name' => strtoupper($request->nama_unit)]
            );        
            
            if($location->wasRecentlyCreated){
                $message = 'tambah';
            }else{
                $message = 'ubah';
            }
            return response()->json(['code'=>200, 'message'=>$message], 200);
            
        }
        
    }
    
    public function list(Request $request)
    {
        if ($request->ajax()) {
            
            $location = Unit::all();
            $data = array();
            $no = 0;
            foreach ($location as $s) {
                $row = array();
                $row [] = $s->id;
                $row [] = ++$no;
                $row [] = $s->name;
                $data[] = $row;
            }
            return response()->json(array("data"=>$data));
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Unit  $location
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $location = Unit::find($id);
        return response()->json(['status'=>($location === null?false:true), 'data'=>$location], 200);
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Unit  $location
    * @return \Illuminate\Http\Response
    */
    public function edit(Unit $location)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Unit  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Unit $location)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Unit  $location
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return response()->json(['code'=>200, 'message'=> Unit::find($id)->delete()], 200);
    }
}
