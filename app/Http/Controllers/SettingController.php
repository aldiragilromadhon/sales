<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('setting.index',['setting'=>Setting::all()]);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        
        if($request->hasFile('setting_image_1')){
            $file = $request->file('setting_image_1');
            $value = time()."_".$file->getClientOriginalName();
            $file->move(public_path('images'),$value);
            $setting = Setting::find(1);
            $setting->value = $value;
            $setting->save();
        }
        if($request->hasFile('setting_image_3')){
            $file = $request->file('setting_image_3');
            $value = time()."_".$file->getClientOriginalName();
            $file->move(public_path('images'),$value);
            $setting = Setting::find(3);
            $setting->value = $value;
            $setting->save();
        }
                    
        for ($i=0; $i < count($request->setting_id_data); $i++) { 
            $setting = Setting::find( $request->setting_id_data[$i] );
            $setting->value = $request->setting_data[$i];
            $setting->save();
        }
        return redirect('setting');
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Setting  $setting
    * @return \Illuminate\Http\Response
    */
    public function show(Setting $setting)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Setting  $setting
    * @return \Illuminate\Http\Response
    */
    public function edit(Setting $setting)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Setting  $setting
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Setting $setting)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Setting  $setting
    * @return \Illuminate\Http\Response
    */
    public function destroy(Setting $setting)
    {
        //
    }
}
