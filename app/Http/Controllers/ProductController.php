<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('product.index');
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $params = [
            'product' => null,
            'categories'=> Category::all(),
            'units'=> Unit::all()
        ];
        return view('product.create', $params);
    }
    
    public function detail($id)
    {
        $params = [
            'product' => Product::find($id),
            'categories'=> Category::all(),
            'units'=> Unit::all()
        ];
        return view('product.create', $params);
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'productname'               => 'required',
            'productcode'               => 'nullable|max:200',
            'productcategory'           => 'nullable|max:250',
            'productunit'               => 'nullable|max:20',
            'productsellprice'          => 'nullable|numeric',
            'productselltax'            => 'nullable|max:20',
            'productbuyprice'           => 'nullable|numeric',
            'productbuytax'             => 'nullable|max:20',
            'productbufferquantity'     => 'nullable|numeric'
        ]);
        $product = new Product();
        $product->user_id       = Auth::id();
        $product->name          = $request->productname;
        $product->code          = $request->productcode;
        $product->description   = $request->productdescription;
        $product->category      = $request->productcategory;
        $product->unit          = $request->productunit;
        $product->sell_price    = (is_null($request->productsellprice)?0:$request->productsellprice);
        $product->sell_tax      = $request->productselltax;
        $product->buy_price     = (is_null($request->productbuyprice)?0:$request->productbuyprice);
        $product->buy_tax       = $request->productbuytax;
        $product->balance_quantity      = (is_null($request->productbalancequantity)?0:$request->productbalancequantity);
        $product->balance_average_price = (is_null($request->productbalanceaverageprice)?0:$request->productbalanceaverageprice);
        $product->balance_date          = date('Y-m-d', strtotime($request->productbalancedate));
        $product->buffer_quantity       = (is_null($request->productbufferquantity)?0:$request->productbufferquantity);
        $product->save();
        return redirect('product');
    }
    
    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            
            $product = Product::select(DB::raw('products.*, (products.balance_quantity-SUM(COALESCE(tjual.qty,0)))+SUM(COALESCE(tbeli.qty,0)) TOTAL'))->
            leftJoin(DB::raw('(SELECT product_id, SUM(quantity) qty FROM `transaction_products` WHERE `tipe_id` = 1 AND `deleted_at` IS NULL GROUP BY product_id) AS tbeli'),function($join){
                $join->on('products.id', '=', 'tbeli.product_id');
            })->
            leftJoin(DB::raw('(SELECT product_id, SUM(quantity) qty FROM `transaction_products` WHERE `tipe_id` = 2 AND `deleted_at` IS NULL GROUP BY product_id) AS tjual'),function($join){
                $join->on('products.id', '=', 'tjual.product_id');
            })->
            groupBy('products.id')->
            get();
            $data = array();
            // $no = 0;
            foreach ($product as $s) {
                // $no++;
                $row = array();
                $row [] = $s->id;
                $row [] = '<p class="text-center m-0">'.$s->code.'</p>';
                $row [] = '<p class="text-center m-0">'.$s->name.'</p>';
                $row [] = '<p class="text-center m-0">'.$s->TOTAL.'</p>';
                $row [] = '<p class="text-center m-0">'.$s->m_unit->name.'</p>';
                $row [] = '<p class="text-right m-0">Rp '.number_format($s->buy_price,2).'</p>';
                $row [] = '<p class="text-right m-0">Rp '.number_format($s->sell_price,2).'</p>';
                $data[] = $row;
            }
            return response()->json(array("data"=>$data));
            
            
            
        }
    }
    
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return response()->json(Product::find($id));
    }
    
    public function show_all()
    {
        $product = Product::all();
        return response()->json($product);
    }
    
    public function buy_price($id)
    {
        $product = Product::find($id);
        return $product->buy_price;
    }
    
    public function sell_price($id)
    {
        return response()->json(
            Product::
            select(DB::raw('
            products.sell_price, 
            products.buffer_quantity buffer, 
            (products.balance_quantity-SUM(COALESCE(tjual.qty,0)))+SUM(COALESCE(tbeli.qty,0)) balance'
            ))->
            leftJoin(DB::raw('(SELECT product_id, SUM(quantity) qty FROM `transaction_products` WHERE `tipe_id` = 1 AND `deleted_at` IS NULL GROUP BY product_id) AS tbeli'),function($join){
                $join->on('products.id', '=', 'tbeli.product_id');
            })->
            leftJoin(DB::raw('(SELECT product_id, SUM(quantity) qty FROM `transaction_products` WHERE `tipe_id` = 2 AND `deleted_at` IS NULL GROUP BY product_id) AS tjual'),function($join){
                $join->on('products.id', '=', 'tjual.product_id');
            })->
            where('products.id',$id)->
            groupBy('products.id')->
            first()
        );
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $request->validate([
            'productname'               => 'required',
            'productcode'               => 'nullable|max:200',
            'productcategory'           => 'nullable|max:250',
            'productunit'               => 'nullable|max:20',
            'productsellprice'          => 'nullable|numeric',
            'productselltax'            => 'nullable|max:20',
            'productbuyprice'           => 'nullable|numeric',
            'productbuytax'             => 'nullable|max:20',
            'productbufferquantity'     => 'nullable|numeric'
        ]);
        $product                = Product::find($id);
        $product->user_id       = Auth::id();
        $product->name          = $request->productname;
        $product->code          = $request->productcode;
        $product->description   = $request->productdescription;
        $product->category      = $request->productcategory;
        $product->unit          = $request->productunit;
        $product->sell_price    = (is_null($request->productsellprice)?0:$request->productsellprice);
        $product->sell_tax      = $request->productselltax;
        $product->buy_price     = (is_null($request->productbuyprice)?0:$request->productbuyprice);
        $product->buy_tax       = $request->productbuytax;
        $product->balance_quantity      = (is_null($request->productbalancequantity)?0:$request->productbalancequantity);
        $product->balance_average_price = (is_null($request->productbalanceaverageprice)?0:$request->productbalanceaverageprice);
        $product->balance_date          = date('Y-m-d', strtotime($request->productbalancedate));
        $product->buffer_quantity       = (is_null($request->productbufferquantity)?0:$request->productbufferquantity);
        $product->save();
        return redirect('product');
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return response()->json(['code'=>200, 'message'=> Product::find($id)->delete()], 200);
    }
}
