<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('category.index');
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        // dd($request->all());
        if ($request->ajax()) {
            $request->validate(['nama_kategori' => 'required']);
            
            $location = Category::updateOrCreate(
                ['id' => $request->id_kategori], 
                ['name' => strtoupper($request->nama_kategori)]
            );        
            
            if($location->wasRecentlyCreated){
                $message = 'tambah';
            }else{
                $message = 'ubah';
            }
            return response()->json(['code'=>200, 'message'=>$message], 200);
            
        }
        
    }
    
    public function list(Request $request)
    {
        if ($request->ajax()) {
            
            $location = Category::all();
            $data = array();
            $no = 0;
            foreach ($location as $s) {
                $row = array();
                $row [] = $s->id;
                $row [] = ++$no;
                $row [] = $s->name;
                $data[] = $row;
            }
            return response()->json(array("data"=>$data));
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Category  $location
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $location = Category::find($id);
        return response()->json(['status'=>($location === null?false:true), 'data'=>$location], 200);
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Category  $location
    * @return \Illuminate\Http\Response
    */
    public function edit(Category $location)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\Category  $location
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Category $location)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Category  $location
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return response()->json(['code'=>200, 'message'=> Category::find($id)->delete()], 200);
    }
}
