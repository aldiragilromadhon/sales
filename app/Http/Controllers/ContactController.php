<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;

class ContactController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {   
        return view('contact.index');
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $params = [
            'contact' => null
        ];
        return view('contact.create', $params);
    }

    public function detail($id)
    {
        $params = [
            'contact' => Contact::find($id)
        ];
        return view('contact.create', $params);
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'contactname'               => 'required',
            'contacttipe'               => 'required',
            'contactemail'              => 'nullable|email'
        ]);
        
        if ($request->contactid) {
            $contact = Contact::find($request->contactid);
        }else{
            $contact = new contact();
        }
        $contact->tipe_id       = json_encode($request->contacttipe);
        $contact->nama          = $request->contactname;
        $contact->perusahaan    = $request->contactcompany;
        $contact->phone         = $request->contacttelepon;
        $contact->email         = $request->contactemail;
        $contact->alamat        = $request->contactalamat;
        $contact->save();
        return redirect('contact');
    }
    
    public function datatable(Request $request)
    {
        
        if ($request->ajax()) {
            
            $location = Contact::all();
            $data = array();
            $no = 0;
            foreach ($location as $s) {
                $tipe = json_decode($s->tipe_id);
                $row = array();
                $row [] = $s->id;
                $row [] = ++$no;
                $row [] = $s->nama.($s->perusahaan?' [<b>'.$s->perusahaan.'</b>]':'');
                $row [] = (count($tipe)>1?$tipe[0].' - '.$tipe[1]:$tipe[0]);
                $row [] = nl2br(e($s->alamat));
                $row [] = $s->email;
                $row [] = $s->phone;
                $data[] = $row;
            }
            return response()->json(array("data"=>$data));
        }
    }
    
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return response()->json(['code'=>200, 'message'=> Contact::find($id)->delete()], 200);
    }
    
    public function get_address($id){
        $contact = Contact::where('id',$id)->first();
        return response()->json($contact);
    }
}
