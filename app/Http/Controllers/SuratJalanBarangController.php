<?php

namespace App\Http\Controllers;

use App\Models\Surat_jalan_barang;
use Illuminate\Http\Request;

class SuratJalanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\surat_jalan_barang  $surat_jalan_barang
     * @return \Illuminate\Http\Response
     */
    public function show(surat_jalan_barang $surat_jalan_barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\surat_jalan_barang  $surat_jalan_barang
     * @return \Illuminate\Http\Response
     */
    public function edit(surat_jalan_barang $surat_jalan_barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\surat_jalan_barang  $surat_jalan_barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, surat_jalan_barang $surat_jalan_barang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\surat_jalan_barang  $surat_jalan_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(surat_jalan_barang $surat_jalan_barang)
    {
        //
    }
}
