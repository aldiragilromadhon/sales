<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Pengiriman;
use App\Models\Product;
use App\Models\RentTruck;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\Transaction_product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class PenjualanController extends Controller
{
    public function index()
    {
        return view('penjualan.index',[
            'periode' => Transaction::select(DB::raw("DATE_FORMAT(tgl_transaksi, '%Y-%m') tanggal"),  DB::raw('YEAR(tgl_transaksi) year, DATE_FORMAT(tgl_transaksi,"%M") month'))
            ->orderBy('tanggal','desc')
            ->groupby('year','month')
            ->get(),
            'hutang' => Transaction::select(DB::raw('SUM(sisa_tagihan) AS TOTAL'))->where('tipe_id',2)->where('status',0)->first(),
            'jatuh_tempo' => Transaction::select(DB::raw('SUM(sisa_tagihan) AS TOTAL'))->where('tipe_id',2)->whereRaw('DATE(tgl_jatuh_tempo) < NOW()')->where('status',0)->first(),
            'lunas' => Transaction::select(DB::raw('SUM(total_transaksi) AS TOTAL'))->where('tipe_id',2)->where('status',1)->first()
        ]);
    }
    
    public function create()
    {
        // $parent = Transaction::select('no_parent')->whereNotNull('no_parent')->get()->toArray();
        $params = [
            'product'   => Product::all(),
            'truck'   => RentTruck::all(),
            'contact'    => Contact::where('tipe_id','LIKE','%Pelanggan%')->get(),
            'pembelian'    => DB::table(DB::raw('(
            SELECT
                a.*
            FROM
                (
                SELECT
                    no_transaksi,
                    no_referensi,
                    SUM( quantity ) qty 
                FROM
                    `transactions` a
                    JOIN transaction_products b ON a.id = b.transaction_id 
                WHERE
                    a.tipe_id = 1 
                    AND `a`.`deleted_at` IS NULL 
                GROUP BY
                    a.no_transaksi 
                ) a
                LEFT JOIN (
                SELECT
                    no_parent,
                    SUM( quantity ) qty 
                FROM
                    `transactions` a
                    JOIN transaction_products b ON a.id = b.transaction_id 
                WHERE
                    a.tipe_id = 2 
                    AND `a`.`deleted_at` IS NULL 
                GROUP BY
                    a.no_parent 
                ) b ON b.no_parent = a.no_transaksi 
            WHERE
                a.qty > COALESCE ( b.qty, 0 )
            ) trans'))->get()
        ];
        return view('penjualan.create', $params);
    }
    
    public function confirm($id)
    {
        $penjualan = Transaction::find($id);
        $penjualan->status = 1;
        $penjualan->save();
        Transaction_product::where('transaction_id', $id)->update(['status' => 1]);
        return redirect('penjualan')->with('success', 'Data penjualan berhasil di terima');
    }
    
    public function destroy($id){
        $penjualan = Transaction::find($id);
        $penjualan->delete();
        Transaction_product::where('transaction_id', $id)->delete();
        return redirect('penjualan')->with('success', 'Data penjualan berhasil di hapus');
    }
    
    public function detail($id)
    {
        $params = [
            'transaction'   => Transaction::find($id)
        ];
        return view('penjualan.detail', $params);
    }
    
    public function check_do($id)
    {
        $penjualan      = Transaction::find($id);
        $pengiriman     = Pengiriman::where('no_parent',$penjualan->no_transaksi)->first();
        if ($pengiriman) {
            return true;
        } else {
            return false;
        }
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'penjualancustomer' => 'required',
            'productpenjualan'  => 'required'
        ]);
        $nomor = Transaction::withTrashed()->whereYear('tgl_transaksi',date('Y', strtotime($request->penjualantanggal)))->orderBy('id', 'desc')->first();
        if(!$nomor){
            $Setting = Setting::get()->toArray();
            $nomor = $Setting[3]['value'];
        }else{
            $nomor_temp	= explode('/',$nomor->no_transaksi);
            $nomor = (integer)$nomor_temp[0] + 1;
        }
        $penjualan = new Transaction();
        $penjualan->tipe_id         = 2;
        $penjualan->contact_id      = $request->penjualancustomer;
        $penjualan->email           = $request->penjualanemail;
        $penjualan->alamat          = $request->penjualanalamatsupplier;
        $penjualan->alamat_kirim    = $request->penjualanalamatpengiriman;
        $penjualan->user_id         = Auth::id();
        $penjualan->no_parent       = $request->penjualanparent;
        $penjualan->no_transaksi    = sprintf("%03d", $nomor).'/BAS/'.date('d/m/Y', strtotime($request->penjualantanggal));
        $penjualan->no_referensi    = $request->penjualannomorreferensi;
        $penjualan->tgl_po          = date('Y-m-d', strtotime($request->penjualantanggalpo));
        $penjualan->tgl_transaksi   = date('Y-m-d', strtotime($request->penjualantanggal));
        $penjualan->tgl_jatuh_tempo = date('Y-m-d', strtotime($request->penjualanjatuhtempo));
        $penjualan->nilai_pemotong  = $request->penjualanpemotongan;
        $penjualan->uang_muka       = $request->penjualanuangmuka;
        $penjualan->sisa_tagihan    = $request->penjualansisatagihan;
        $penjualan->total_transaksi = $request->penjualansubtotal;
        $penjualan->pesan           = $request->penjualanpesan;
        $penjualan->memo            = $request->penjualanmemo;
        $penjualan->user            = $request->penjualanuser;
        $penjualan->status          = ($request->penjualansisatagihan == 0 ? 1 : 0);
        $penjualan->approve         = (Auth::id() == 1 ? 1 : 0);
        $penjualan->save();
        for ($i=0; $i < count($request->productpenjualan); $i++) { 
            if ($request->productpenjualan[$i]) {
                $product[] = [
                    'transaction_id'    => $penjualan->id,
                    'tipe_id'           => 2,
                    'product_id'        => $request->productpenjualan[$i],
                    'unit_id'           => '1',
                    'status'            => '1',
                    'diskon'            => round($request->productdiskon[$i],2),
                    'pajak'             => round($request->productpajak[$i],2),
                    'quantity'          => round(str_replace(",",".",$request->productquantity[$i]),2),
                    'price'             => round(str_replace(",",".",str_replace(['.','Rp '],"",$request->productharga[$i])),2),
                    'total'             => round(str_replace(",",".",str_replace(['.','Rp '],"",$request->producttotal[$i])),2),
                ];
            }
        }        
        Transaction_product::insert($product);
        return response()->json(['code'=>200, 'id'=>$penjualan->id, 'message'=>'Sukses'], 200);
        // return redirect()->route('penjualan')->with('success', 'Data berhasil ditambahkan');
    }
    
    public function datatable(Request $req)
    {
        if($req->tanggal){
            $tanggal = $req->tanggal;
        }else{
            $tanggal = date('Y-m');
        }
        $transaction = Transaction::where('tipe_id','2')->where(function ($query) use ($tanggal) {
            $query->whereRaw('DATE_FORMAT(tgl_transaksi,"%Y-%m") = "'.$tanggal.'"')->orWhere('status', '=', 0)->orWhere('approve', '=', 0);
        });
        // if(Auth::id() == 1){
            $transaction = $transaction->orderByDesc('id')->get();
        // }else{
            // $transaction = $transaction->orderByDesc('id')->where('user_id',Auth::id())->get();
        // }
        $data = array();
        // $no = 0;
        foreach ($transaction as $s) {
            // $no++;
            $row = array();
            $row [] = $s->id;
            $row [] = '<p class="text-center m-0">'.$s->tgl_transaksi.'</p>';
            $row [] = ($s->no_parent?'<div class="badge text-center badge-success">'.$s->no_parent.'</div>':'');
            $row [] = '<p class="text-center m-0">'.$s->no_transaksi.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->contact->nama.'</p>';
            $row [] = '<p class="text-center m-0">'.$s->tgl_jatuh_tempo.'</p>';
            $row [] = '<div class="badge badge-square badge-'.($s->approve == 0 ? 'danger"><i class="la la-clock-o font-small-3"></i> Pending' : ($s->status == 0 ? 'warning">Open' : 'success">Lunas')).'</div>';
            $row [] = '<p class="text-center m-0">'.number_format($s->sisa_tagihan,0,',','.').'</p>';
            $row [] = '<p class="text-center m-0">'.number_format($s->total_transaksi,0,',','.').'</p>';
            // $row [] = 
            // '<div class="text-center">
            // <button type="button" class="btn btn-sm btn-icon btn-warning secondary" onclick="getData('."'".($s->id)."'".')" title="Edit Lokasi"><i class="la la-edit"></i></button>
            // <button type="button" class="btn btn-sm btn-icon btn-danger secondary" onclick="deleteData('."'".($s->id)."'".')" title="Hapus Lokasi"><i class="la la-remove"></i></button>
            // </div>';
            $data[] = $row;
        }
        return response()->json(array("data"=>$data));
    }
    
    public function view_pdf($id)
    {
        $penjualan      = Transaction::find($id);
        $pengiriman     = Pengiriman::where('no_parent',$penjualan->no_transaksi)->first();
        $pdf            = PDF::setOptions(['enable_remote' => true,'chroot'  => public_path('/')])->loadview('penjualan.pdf',[
            'penjualan' => $penjualan,
            'pengiriman'=> $pengiriman,
            'terbilang' => $this->terbilang($penjualan->total_transaksi)
        ]);
        return          $pdf->stream('Invoice_'.str_replace(' ', '_', $penjualan->contact->perusahaan).'_'.date('d_F_Y').'.pdf');
    }
    
    public function view_pdf_do($id)
    {
        $penjualan    = Transaction::find($id);
        $pdf            = PDF::setOptions(['enable_remote' => true,'chroot'  => public_path('/')])->loadview('penjualan.pdf_do',['penjualan' => $penjualan]);
        return          $pdf->stream('Delivery-order-'.$id.'-'.time().'.pdf');
    }
    
    public static function terbilang($nominal)
    {
        $nominal = abs($nominal);
        $angka = array(	'', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 
        'tujuh', 'delapan',	'sembilan',	'sepuluh', 'sebelas');
        
        if ($nominal < 12) {
            return ' '.$angka[$nominal];
        }
        
        if ($nominal < 20) {
            return static::terbilang($nominal - 10). ' belas';
        }
        
        if ($nominal < 100) {
            return static::terbilang($nominal / 10). ' puluh'
            . static::terbilang($nominal % 10);
        }
        
        if ($nominal < 200) {
            return ' seratus'. static::terbilang($nominal - 100);
        }
        
        if ($nominal < 1000) {
            return static::terbilang($nominal / 100). ' ratus'
            . static::terbilang($nominal % 100);
        }
        
        if ($nominal < 2000) {
            return ' seribu'. static::terbilang($nominal - 1000);
        }
        
        if ($nominal < 1000000) {
            return static::terbilang($nominal / 1000). ' ribu' 
            . static::terbilang($nominal % 1000);
        }
        
        if ($nominal < 1000000000) {
            return static::terbilang($nominal / 1000000). ' juta' 
            . static::terbilang($nominal % 1000000);
        }
        
        if ($nominal < 1000000000000) {
            return static::terbilang($nominal / 1000000000). ' milyar'
            . static::terbilang(fmod($nominal, 1000000000));
        }
        
        if ($nominal < 1000000000000000) {
            return static::terbilang($nominal / 1000000000000). ' trilyun'
            . static::terbilang(fmod($nominal, 1000000000000));
        }		
    }
    
    
}
