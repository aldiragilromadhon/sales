<?php

namespace App\Http\Controllers;

use App\Models\Transaction_product;
use Illuminate\Http\Request;

class TransactionProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction_product  $transaction_product
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction_product $transaction_product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction_product  $transaction_product
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction_product $transaction_product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction_product  $transaction_product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction_product $transaction_product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction_product  $transaction_product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction_product $transaction_product)
    {
        //
    }
}
