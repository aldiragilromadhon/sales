<?php

function get_name_day($day) {
    $day_name = "";
    switch ($day) {
        case 1:
            $day_name = "Senin";
            break;
        case 2:
            $day_name = "Selasa";
            break;
        case 3:
            $day_name = "Rabu";
            break;
        case 4:
            $day_name = "Kamis";
            break;
        case 5:
            $day_name = "Jumat";
            break;
        case 6:
            $day_name = "Sabtu";
            break;
        case 7:
            $day_name = "Minggu";
            break;
    }
    return $day_name;
}