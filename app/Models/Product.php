<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    public function transaction_product()
    {
        return $this->hasMany(Transaction_product::class);
    }

    public function m_unit()
    {
        return $this->belongsTo(Unit::class,'unit');
    }

    public function m_category()
    {
        return $this->belongsTo(Category::class,'category');
    }

}
