<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class surat_jalan extends Model
{
    use HasFactory;
    protected $table = 'surat_jalans';
    protected $fillable = ['pengiriman_id', 'no_transaksi', 'tgl_transaksi', 'truck_id', 'tipe', 'kontainer'];
    
    public function pengiriman()
    {
        return $this->belongsTo(Pengiriman::class, 'pengiriman_id');
    }

    public function truck()
    {
        return $this->belongsTo(RentTruck::class, 'truck_id');
    }

    public function barang()
    {
        return $this->hasMany(Surat_jalan_barang::class);
    }

}
