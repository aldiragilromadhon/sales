<?php

namespace App\Models;

use App\Http\Controllers\LocationPriceController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $fillable = ['name'];

    public function locationprice()
    {
        return $this->hasMany(LocationPrice::class);
    }

}
