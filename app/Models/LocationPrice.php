<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocationPrice extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $fillable = ['location_from','location_to','parameter','weight','price'];


    public function asal()
    {
        return $this->belongsTo(Location::class,'location_from');
    }

    public function tujuan()
    {
        return $this->belongsTo(Location::class,'location_to');
    }

}
