<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengiriman extends Model
{
    use HasFactory, SoftDeletes;

    public function barang()
    {
        return $this->hasMany(Pengiriman_barang::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    public function location_from()
    {
        return $this->belongsTo(Location::class, 'location_from_id');
    }

    public function location_to()
    {
        return $this->belongsTo(Location::class, 'location_to_id');
    }

}
