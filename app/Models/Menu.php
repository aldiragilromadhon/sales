<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function hak_akses()
    {
        return $this->hasMany(Hak_akses::class,'menu_id');
    }

    public function parent()
    {
        return $this->belongsTo(Item::class,'parent_id');
    }
}
