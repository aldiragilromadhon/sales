<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class surat_jalan_barang extends Model
{
    use HasFactory;
    protected $table = 'surat_jalan_barangs';
    protected $fillable = ['surat_jalan_id', 'pengiriman_barang_id', 'tonase', 'karung'];

    public function surat_jalan()
    {
        return $this->belongsTo(Surat_jalan::class, 'surat_jalan_id');
    }

    public function pengiriman_barang()
    {
        return $this->belongsTo(Pengiriman_barang::class, 'pengiriman_barang_id');
    }

}
