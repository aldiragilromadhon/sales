<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/password/new/{token}', [App\Http\Controllers\NewPassword::class, 'validation']);
Route::post('/password/new/save', [App\Http\Controllers\NewPassword::class, 'store'])->name('password.new.save');

Route::get('/show-transaksi/{id}', [App\Http\Controllers\TransactionController::class, 'show'])->middleware('auth');

Route::get('/pembelian', [App\Http\Controllers\PembelianController::class, 'index'])->name('pembelian')->middleware('auth');
Route::get('/pembelian/new', [App\Http\Controllers\PembelianController::class, 'create'])->middleware('auth');
Route::get('/pembelian/detail/{id}', [App\Http\Controllers\PembelianController::class, 'detail'])->middleware('auth');
Route::get('/pembelian-confirm/{id}', [App\Http\Controllers\PembelianController::class, 'confirm'])->middleware('auth');
Route::get('/pembelian-delete/{id}', [App\Http\Controllers\PembelianController::class, 'destroy'])->middleware('auth');
Route::post('/store-pembelian', [App\Http\Controllers\PembelianController::class, 'store'])->middleware('auth');
Route::post('/datatable-pembelian', [App\Http\Controllers\PembelianController::class, 'datatable'])->middleware('auth');
Route::get('/pembelian-po/{id}', [App\Http\Controllers\PembelianController::class, 'view_pdf']);

Route::get('/penjualan', [App\Http\Controllers\PenjualanController::class, 'index'])->name('penjualan')->middleware('auth');
Route::get('/penjualan/new', [App\Http\Controllers\PenjualanController::class, 'create'])->middleware('auth');
Route::get('/penjualan/detail/{id}', [App\Http\Controllers\PenjualanController::class, 'detail'])->middleware('auth');
Route::get('/penjualan-confirm/{id}', [App\Http\Controllers\PenjualanController::class, 'confirm'])->middleware('auth');
Route::get('/penjualan-delete/{id}', [App\Http\Controllers\PenjualanController::class, 'destroy'])->middleware('auth');
Route::get('/penjualan-check/{id}', [App\Http\Controllers\PenjualanController::class, 'check_do'])->middleware('auth');
Route::post('/store-penjualan', [App\Http\Controllers\PenjualanController::class, 'store'])->middleware('auth');
Route::post('/datatable-penjualan', [App\Http\Controllers\PenjualanController::class, 'datatable'])->middleware('auth');
Route::get('/penjualan-inv/{id}', [App\Http\Controllers\PenjualanController::class, 'view_pdf']);
Route::get('/penjualan-do/{id}', [App\Http\Controllers\PenjualanController::class, 'view_pdf_do']);

Route::get('/pengiriman', [App\Http\Controllers\PengirimanController::class, 'index'])->name('pembelian')->middleware('auth');
Route::get('/pengiriman/new', [App\Http\Controllers\PengirimanController::class, 'create'])->middleware('auth');
Route::get('/pengiriman/detail/{id}', [App\Http\Controllers\PengirimanController::class, 'detail'])->middleware('auth');
Route::post('/store-pengiriman', [App\Http\Controllers\PengirimanController::class, 'store'])->middleware('auth');
Route::post('/store-pengiriman-pembayaran', [App\Http\Controllers\PengirimanController::class, 'pembayaran'])->middleware('auth');
Route::post('/datatable-pengiriman', [App\Http\Controllers\PengirimanController::class, 'list'])->middleware('auth');
Route::post('/pengiriman-parent', [App\Http\Controllers\PengirimanController::class, 'get_parent'])->middleware('auth');
Route::get('/pengiriman-delete/{id}', [App\Http\Controllers\PengirimanController::class, 'destroy'])->middleware('auth');
Route::get('/pengiriman-confirm/{id}', [App\Http\Controllers\PengirimanController::class, 'confirm'])->middleware('auth');
Route::get('/pengiriman-barang/{id}', [App\Http\Controllers\PengirimanController::class, 'list_barang'])->middleware('auth');
Route::get('/pengiriman-barang-edit/{id}', [App\Http\Controllers\PengirimanController::class, 'list_barang_edit'])->middleware('auth');
Route::get('/pengiriman-check/{id}', [App\Http\Controllers\PengirimanController::class, 'check_inv'])->middleware('auth');
Route::get('/pengiriman-check-pay/{id}', [App\Http\Controllers\PengirimanController::class, 'check_pay'])->middleware('auth');
Route::get('/pengiriman-po/{id}', [App\Http\Controllers\PengirimanController::class, 'view_pdf']);
Route::get('/pengiriman-do/{id}', [App\Http\Controllers\PengirimanController::class, 'view_pdf_do']);
Route::get('/pengiriman-sj/{id}', [App\Http\Controllers\PengirimanController::class, 'view_pdf_sj']);
Route::get('/pengiriman-payment/{id}', [App\Http\Controllers\PengirimanController::class, 'view_pdf_payment']);
Route::get('/pengiriman-rekap/{id}', [App\Http\Controllers\PengirimanController::class, 'view_pdf_rekap']);

Route::get('/angkutan', [App\Http\Controllers\AngkutanController::class, 'index'])->middleware('auth');
Route::get('/angkutan/new', [App\Http\Controllers\AngkutanController::class, 'create'])->middleware('auth');
Route::get('/angkutan/detail/{id}', [App\Http\Controllers\AngkutanController::class, 'detail'])->middleware('auth');
Route::post('/store-angkutan', [App\Http\Controllers\AngkutanController::class, 'store'])->middleware('auth');
Route::post('/store-angkutan-pembayaran', [App\Http\Controllers\AngkutanController::class, 'pembayaran'])->middleware('auth');
Route::post('/datatable-angkutan', [App\Http\Controllers\AngkutanController::class, 'list'])->middleware('auth');
Route::post('/angkutan-parent', [App\Http\Controllers\AngkutanController::class, 'get_parent'])->middleware('auth');
Route::get('/angkutan-delete/{id}', [App\Http\Controllers\AngkutanController::class, 'destroy'])->middleware('auth');
Route::get('/angkutan-confirm/{id}', [App\Http\Controllers\AngkutanController::class, 'confirm'])->middleware('auth');
Route::get('/angkutan-barang/{id}', [App\Http\Controllers\AngkutanController::class, 'list_barang'])->middleware('auth');
Route::get('/angkutan-barang-edit/{id}', [App\Http\Controllers\AngkutanController::class, 'list_barang_edit'])->middleware('auth');
Route::get('/angkutan-check/{id}', [App\Http\Controllers\AngkutanController::class, 'check_inv'])->middleware('auth');
Route::get('/angkutan-check-pay/{id}', [App\Http\Controllers\AngkutanController::class, 'check_pay'])->middleware('auth');
Route::get('/angkutan-po/{id}', [App\Http\Controllers\AngkutanController::class, 'view_pdf']);
Route::get('/angkutan-do/{id}', [App\Http\Controllers\AngkutanController::class, 'view_pdf_do']);
Route::get('/angkutan-sj/{id}', [App\Http\Controllers\AngkutanController::class, 'view_pdf_sj']);
Route::get('/angkutan-payment/{id}', [App\Http\Controllers\AngkutanController::class, 'view_pdf_payment']);
Route::get('/angkutan-rekap/{id}', [App\Http\Controllers\AngkutanController::class, 'view_pdf_rekap']);

Route::post('/store-surat-jalan', [App\Http\Controllers\SuratJalanController::class, 'store'])->middleware('auth');
Route::get('/get-surat-jalan/{id}', [App\Http\Controllers\SuratJalanController::class, 'show'])->middleware('auth');
Route::get('/delete-surat-jalan/{id}', [App\Http\Controllers\SuratJalanController::class, 'destroy'])->middleware('auth');
Route::get('/datatable-surat-jalan/{id}', [App\Http\Controllers\SuratJalanController::class, 'list'])->middleware('auth');

Route::get('/product', [App\Http\Controllers\ProductController::class, 'index'])->middleware('auth');
Route::get('/product/new', [App\Http\Controllers\ProductController::class, 'create'])->middleware('auth');
Route::get('/product/detail/{id}', [App\Http\Controllers\ProductController::class, 'detail'])->middleware('auth');
Route::post('/store-product', [App\Http\Controllers\ProductController::class, 'store'])->middleware('auth');
Route::post('/update-product/{id}', [App\Http\Controllers\ProductController::class, 'update'])->middleware('auth');
Route::post('/datatable-product', [App\Http\Controllers\ProductController::class, 'datatable'])->middleware('auth');
Route::get('/delete-product/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->middleware('auth');
Route::get('/show-product/{id}', [App\Http\Controllers\ProductController::class, 'show'])->middleware('auth');
Route::get('/show-all-product', [App\Http\Controllers\ProductController::class, 'show_all'])->middleware('auth');
Route::get('/buy-price-product/{id}', [App\Http\Controllers\ProductController::class, 'buy_price'])->middleware('auth');
Route::get('/sell-price-product/{id}', [App\Http\Controllers\ProductController::class, 'sell_price'])->middleware('auth');

Route::get('/contact', [App\Http\Controllers\ContactController::class, 'index'])->middleware('auth');
Route::get('/contact/new', [App\Http\Controllers\ContactController::class, 'create'])->middleware('auth');
Route::get('/contact/detail/{id}', [App\Http\Controllers\ContactController::class, 'detail'])->middleware('auth');
Route::post('/store-contact', [App\Http\Controllers\ContactController::class, 'store'])->middleware('auth');
Route::post('/update-contact/{id}', [App\Http\Controllers\ContactController::class, 'update'])->middleware('auth');
Route::get('/delete-contact/{id}', [App\Http\Controllers\ContactController::class, 'destroy'])->middleware('auth');
Route::get('/datatable-contact', [App\Http\Controllers\ContactController::class, 'datatable'])->middleware('auth');
Route::get('/get-address-contact/{id}', [App\Http\Controllers\ContactController::class, 'get_address'])->middleware('auth');

Route::get('/renttruck', [App\Http\Controllers\RentTruckController::class, 'index'])->middleware('auth');
Route::get('/renttruck/new', [App\Http\Controllers\RentTruckController::class, 'create'])->middleware('auth');
Route::post('/store-renttruck', [App\Http\Controllers\RentTruckController::class, 'store'])->middleware('auth');
Route::get('/delete-renttruck/{id}', [App\Http\Controllers\RentTruckController::class, 'destroy'])->middleware('auth');
Route::get('/get-renttruck/{id}', [App\Http\Controllers\RentTruckController::class, 'show'])->middleware('auth');
Route::get('/datatable-renttruck', [App\Http\Controllers\RentTruckController::class, 'datatable'])->middleware('auth');

Route::get('/unit', [App\Http\Controllers\UnitController::class, 'index'])->middleware('auth');
Route::post('/store-unit', [App\Http\Controllers\UnitController::class, 'store'])->middleware('auth');
Route::get('/get-unit/{id}', [App\Http\Controllers\UnitController::class, 'show'])->middleware('auth');
Route::get('/delete-unit/{id}', [App\Http\Controllers\UnitController::class, 'destroy'])->middleware('auth');
Route::get('/datatable-unit', [App\Http\Controllers\UnitController::class, 'list'])->middleware('auth');

Route::get('/category', [App\Http\Controllers\CategoryController::class, 'index'])->middleware('auth');
Route::post('/store-category', [App\Http\Controllers\CategoryController::class, 'store'])->middleware('auth');
Route::get('/get-category/{id}', [App\Http\Controllers\CategoryController::class, 'show'])->middleware('auth');
Route::get('/delete-category/{id}', [App\Http\Controllers\CategoryController::class, 'destroy'])->middleware('auth');
Route::get('/datatable-category', [App\Http\Controllers\CategoryController::class, 'list'])->middleware('auth');

Route::get('/location', [App\Http\Controllers\LocationController::class, 'index'])->middleware('auth');
Route::post('/store-location', [App\Http\Controllers\LocationController::class, 'store'])->middleware('auth');
Route::get('/get-location/{id}', [App\Http\Controllers\LocationController::class, 'show'])->middleware('auth');
Route::get('/delete-location/{id}', [App\Http\Controllers\LocationController::class, 'destroy'])->middleware('auth');
Route::get('/datatable-location', [App\Http\Controllers\LocationController::class, 'list'])->middleware('auth');

Route::get('/location-price', [App\Http\Controllers\LocationPriceController::class, 'index'])->middleware('auth');
Route::post('/store-location-price', [App\Http\Controllers\LocationPriceController::class, 'store'])->middleware('auth');
Route::post('/store-location-price-detail', [App\Http\Controllers\LocationPriceController::class, 'store_detail'])->middleware('auth');
Route::get('/get-location-price/{id1}/{id2}', [App\Http\Controllers\LocationPriceController::class, 'show'])->middleware('auth');
Route::get('/delete-location-price/{id1}/{id2}', [App\Http\Controllers\LocationPriceController::class, 'destroy'])->middleware('auth');
Route::get('/datatable-location-price', [App\Http\Controllers\LocationPriceController::class, 'list'])->middleware('auth');
Route::get('/datatable-location-price-detail/{id1}/{id2}', [App\Http\Controllers\LocationPriceController::class, 'list_detail'])->middleware('auth');

Route::get('/report', [App\Http\Controllers\ReportController::class, 'index'])->middleware('auth');
Route::post('/list-laba-rugi', [App\Http\Controllers\ReportController::class, 'list_laba_rugi'])->middleware('auth');
Route::post('/list-laporan-pembelian', [App\Http\Controllers\ReportController::class, 'list_pembelian'])->middleware('auth');
Route::post('/list-laporan-penjualan', [App\Http\Controllers\ReportController::class, 'list_penjualan'])->middleware('auth');
Route::post('/datatable-report-pembelian', [App\Http\Controllers\ReportController::class, 'datatable_pembelian'])->middleware('auth');
Route::post('/datatable-report-penjualan', [App\Http\Controllers\ReportController::class, 'datatable_penjualan'])->middleware('auth');

Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->middleware('auth');
Route::post('/store-user', [App\Http\Controllers\UserController::class, 'store'])->name('store.user')->middleware('auth');
Route::post('/update-user', [App\Http\Controllers\UserController::class, 'update'])->name('update.user')->middleware('auth');
Route::post('/datatable-user', [App\Http\Controllers\UserController::class, 'datatable'])->name('datatable-user')->middleware('auth');
Route::get('/get-user/{id}', [App\Http\Controllers\UserController::class, 'show'])->middleware('auth');
Route::get('/delete-user/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->middleware('auth');
Route::get('/user-profile', [App\Http\Controllers\UserController::class, 'profile'])->name('user.profile')->middleware('auth');
Route::post('/update-profile', [App\Http\Controllers\UserController::class, 'updateProfile'])->middleware('auth');
Route::post('/update-profile-password', [App\Http\Controllers\UserController::class, 'updatePassword'])->middleware('auth');

Route::get('/setting', [App\Http\Controllers\SettingController::class, 'index'])->middleware('auth');
Route::post('/store-setting', [App\Http\Controllers\SettingController::class, 'store'])->middleware('auth');
