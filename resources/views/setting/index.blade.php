
@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">SETTING APLIKASI</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" method="post" action="{{ url('store-setting') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row justify-content-md-center">
                                    <div class="col-md-6">
                                        <div class="form-body">
                                            @foreach ($setting as $s)
                                            @if ($s->id == 1 || $s->id == 3)
                                            <div class="form-group mb-0">
                                                <label for="setting_image_{{ $s->id }}">{{ $s->name }}</label>
                                                <input type="file" id="setting_image_{{ $s->id }}" name="setting_image_{{ $s->id }}" class="form-control">
                                            </div>
                                            <div class="row mb-1">
                                                <div class="col-md-4">
                                                    <img class="img-thumbnail img-fluid" src="{{ asset('images').'/'.$s->value }}" itemprop="thumbnail" alt="File foto" id="preview-file-{{ $s->id }}"/>
                                                </div>
                                            </div>
                                            @else
                                            <div class="form-group">
                                                <label for="setting_data_{{ $s->id }}">{{ $s->name }}</label>
                                                <input type="text" id="setting_id_data_{{ $s->id }}" name="setting_id_data[]" value="{{ $s->id }}" hidden>
                                                <input type="text" id="setting_data_{{ $s->id }}" name="setting_data[]" class="form-control" value="{{ $s->value }}">
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions text-center">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Save
                                    </button>
                                </div>
                            </form>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    
    $(document).ready(function() {
        $("#setting_image_1").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview-file-1').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        $("#setting_image_3").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview-file-3').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    
</script>
@endsection
