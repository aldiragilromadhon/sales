<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Aplikasi sales dengan fitur pembelian, penjualan dan pengiriman produk.">
    <meta name="keywords" content="aplikasi sales, sales apps, distributor, pembelian, penjualan, pengiriman">
    <meta name="author" content="PIXINVENT">
    <title>{{ config('app.name', 'Sales Apps') }}</title>
    <link rel="apple-touch-icon" href="{{asset('images/'.$settings['0']['value'])}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/'.$settings['0']['value'])}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/vendors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/selects/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/selects/selectize.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/daterange/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/sweetalert.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/switch.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/extended/form-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/selectize/selectize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/pickers/daterange/daterange.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/loaders/loaders.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/colors/palette-loader.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/colors/palette-callout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/fonts/simple-line-icons/style.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/fonts/line-awesome/css/line-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/sweetalert.css')}}">
    @yield('style')
    
    <style type="text/css">
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            opacity: 0.5;
            background-color: white;
        }
        .img-responsive1 {
            width: 50px;
            height: 50px;
            position:absolute;
            left:50%;
            top:50%;
            margin-top:-25px; /* This needs to be half of the height */
            margin-left:-25px;
        }
    </style>
</head>
<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    <div class="se-pre-con">
        <img src=' {{ asset("app-assets/images/loading.gif") }}' class="img-responsive1" />
    </div>
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-info navbar-shadow" tabindex="-1">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img class="brand-logo" alt="{{ config('app.name', 'Sales Apps') }}" src="{{asset('images/'.$settings['0']['value'])}}">
                            <h2 class="brand-text">{{ config('app.name', 'Sales Apps') }}</h2>
                        </a>
                    </li>
                    <li class="nav-item d-md-none">
                        <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#" id="hidden-menu"><i class="ft-menu"></i></a></li>
                        <li class="nav-item d-none d-md-block"><a href="{{ url('pembelian/new') }}" class="btn btn-success box-shadow-2 mt-1 mr-1"><i class="la la-shopping-cart"></i> Beli</a></li>
                        <li class="nav-item d-none d-md-block"><a href="{{ url('penjualan/new') }}" class="btn btn-success box-shadow-2 mt-1 mr-1"><i class="la la-tags"></i> Jual</a></li>
                        <li class="nav-item d-none d-md-block"><a href="{{ url('pengiriman/new') }}" class="btn btn-primary box-shadow-2 mt-1 mr-1"><i class="la la-truck"></i> Kirim</a></li>
                    </ul>
                    
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link pt-2" href="#" data-toggle="dropdown">
                                <span class="menu-title">Hello, <span class="user-name text-bold-700">{{ Auth::user()->name }}</span></span>
                                <i class="ficon ft-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{ url('user.profile') }}"><i class="ft-user"></i> Edit Profile</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" onclick="logout()"><i class="ft-power"></i> Logout</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-label"href="#" onclick="logout()" title="Keluar"><i class="ficon ft-log-out"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true">
        <div class="main-menu-content">
            <div class="hidden-xs profile-perusahaan">
                <div class="text-center">
                    <div class="card-body pl-0 pt-1 pb-1 pr-0 m-0">
                        @if ($settings['0']['value'])
                        <img src="{{asset('images/'.$settings['0']['value'])}}" class="height-100" alt="Card image">
                        @else
                        <img src="{{asset('app-assets/images/portrait/medium/avatar-m-5.png')}}" class="rounded-circle height-100" alt="Card image">
                        @endif
                    </div>
                    <div class="card-body pt-0 pb-0">
                        <h6 class="">{{ $settings['1']['value'] }}</h6>
                    </div>
                </div>
            </div>
            <ul class="navigation navigation-main mt-1 mb-2" id="main-menu-navigation" data-menu="menu-navigation">
                <li><hr class="m-0 mb-1"></li>
                @php $modul = 0; @endphp
                @foreach ($menu as $m)
                @if($m->modul_id != $modul)
                <li><hr class="m-0 mb-1 mt-1"></li>
                @php $modul = $m->modul_id; @endphp
                @endif
                <li class="{{ Request::is($m->target) || Request::is($m->target .'/*') ? 'active' : '' }} nav-item"><a class="menu-item" href="{{ url($m->target) }}"><i class="{{ $m->icon }}"></i><span class="menu-title">{{ $m->menu }}</span></a></li>
                @endforeach
                
                {{-- <li class=" nav-item"><a href="#"><i class="la la-database"></i><span class="menu-title">Stock Opname</span></a></li>
                
                <li><hr class="m-0 mb-1"></li>
                
                <li class=" nav-item"><a href="#"><i class="la la-gear"></i><span class="menu-title">Supplier</span></a></li>
                <li class=" nav-item"><a href="#"><i class="la la-gear"></i><span class="menu-title">Customer</span></a></li>
                <li class=" nav-item"><a href="#"><i class="la la-gear"></i><span class="menu-title">Barang</span></a></li>
                
                <li><hr class="m-0 mb-1"></li>
                
                <li class=" nav-item"><a href="#"><i class="la la-gear"></i><span class="menu-title">User</span></a></li>
                <li class=" nav-item"><a href="#"><i class="la la-gear"></i><span class="menu-title">Akses</span></a></li> --}}
                
            </ul>
        </div>
    </div>
    <div class="app-content content">
        @yield('content')
    </div>
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">Copyright &copy; {{date('Y')}} <a class="text-bold-800 grey darken-2" href="#" target="_blank"></a>, All rights reserved. </span>
        </p>
    </footer>
    <script src="{{asset('app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/icheck/icheck.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/select/selectize.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/extended/formatter/formatter.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/jszip.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/pdfmake.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/vfs_fonts.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/buttons.html5.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/buttons.print.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/tables/buttons.colVis.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/pickadate/picker.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/pickadate/picker.date.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/daterange/daterangepicker.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/extensions/sweetalert.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/ui/headroom.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/js/core/app.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/js/scripts/modal/components-modal.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/js/scripts/popover/popover.js')}}" type="text/javascript"></script>
    {{-- <script src="{{asset('app-assets/js/scripts/forms/select/form-selectize.js')}}" type="text/javascript"></script> --}}
    <script type="text/javascript">
        $( ".se-pre-con" ).hide();
        
        $(document).ajaxStop(function() {
            setTimeout(function(){
                $( ".se-pre-con" ).hide();
            },500);
        });
        $(document).ajaxStart(function() {
            $(".se-pre-con").show();
        });
        $(document).ajaxError(function() {
            setTimeout(function(){
                $( ".se-pre-con" ).hide();
            },500);
        });
        $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
            setTimeout(function(){
                $( ".se-pre-con" ).hide();
            },500);
        };
        
        $('.select2').select2({
            dropdownAutoWidth : true,
            width: '100%'
        });
        function logout(){
            swal({
                title: "Are you sure?",
                text: "Anda akan keluar dari sistem!",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "Batal!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Keluar",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(isConfirm => {
                if (isConfirm) {
                    window.location = '{{ url("logout")}}'; 
                } else {
                    swal("Batal!", "Anda batal keluar sistem!.", "info");
                } 
            });
        }
        $(document).ready(function() {
            $('.nav-menu-main').bind('click', function() {
                if($(this).hasClass('is-active')) {
                    $('.profile-perusahaan').hide();
                }else {
                    $('.profile-perusahaan').show();
                }
            });
            
            
        }); 
        function show_modal(id){
            $('#'+id).modal('show');
        } 
        function nl2br (str, is_xhtml) {
            if (typeof str === 'undefined' || str === null) {
                return '';
            }
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }
    </script>
    @yield('script')
</body>
</html>