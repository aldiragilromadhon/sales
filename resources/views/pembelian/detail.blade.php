@extends('layouts.app')
@include('pembelian.transaction')
@section('content')
<div class="content-wrapper pt-1">
    <div class="content-header row pr-2">
        <div class="content-header-left col-md-6 col-12 mt-1">
            <h3 class="content-header-title">DETAIL PEMBELIAN</h3>
            <p class="text-bold-700 font-medium-2">{{ $transaction->no_transaksi }}</p>
        </div>
        <div class="content-header-right col-md-6 col-12 mt-1">
            <div class="float-md-right">
                <a href="{{ url('pembelian') }}" type="button" class="btn btn-info round box-shadow-2 px-2 mb-1"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="lists">
            @yield('transaction')
        </section>
    </div>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function(){
    });
</script>
@endsection