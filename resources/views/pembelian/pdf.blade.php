<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Purchase Order</title>
    {{-- <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css"> --}}
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
</head>
<style>
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        color: black;
    }
    body{
        font-size: 12px;
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        background-color: white;
        color: black;
    }
    table{
        width: 100%;
    }
    .table-bordered, .border{
        border: 1px solid black;
    }
    .table th, .table td {
        padding: 0.25rem;
    }
    .border .border-dark{
        color: black;
        border: 1px solid black;
    }
    
    footer {
        position: fixed; 
        bottom: -60px; 
        left: 20px; 
        right: 0px;
        height: 50px; 
        
        color: black;
        text-align: left;
    }
    
</style>
<body>
    <div class="ml-1 mr-1 p-1 border border-dark" style="color: black;border: 1px solid black;">
        <table>
            <tr>
                <td class="text-center" width="30%"><img src="{{public_path('images/'.$settings['2']['value'])}}" class="text-center" alt="company logo" style="width: 80%"></td>
                <td width="60%" class="text-left">
                    <h4 class="m-0 p-0">{{ $settings['1']['value'] }}</h4>
                    <p class="m-0 p-0">email : cvbintangalkacasejahtera@gmail.com</p>
                    <p class="m-0 p-0">Jl Dr Cipto Perumahan BTN Blok L/11</p>
                    <p class="m-0 p-0">Kab Sumenep - Provinsi Jawa timur</p>
                </td>
                <td class="text-center" width="10%"></td>
            </tr>
        </table>
        <hr/>
        <table>
            <tr style="text-align: center">
                <td class="text-center">
                    <h4 class="m-0"><u>PURCHASE ORDER</u></h4>
                    <h5 class="m-0 p-0">{{ $pembelian->no_transaksi }}</h5>
                </td>
            </tr>
        </table>
        <table class="mt-1">
            <tr>
                <td style="text-align: left" width="45%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="33%">Kepada Yth,</td>
                        </tr>
                        <tr>
                            <td width="33%"><b>{{ $pembelian->contact->nama }}</b></td>
                        </tr>
                        <tr>
                            <td width="33%">{!! nl2br(e($pembelian->alamat)) !!}</td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: left" width="30%">
                </td>
                <td class="right" style="text-align: right; vertical-align: top;" width="25%">
                    Tanggal : {{ date('d F Y',strtotime($pembelian->tgl_transaksi)) }}
                </td>
            </tr>
        </table>
        @php
        $no = 1;
        @endphp
        <p class="mb-1 mt-1">Melalui surat ini, kami mengajukan pesanan barang sebagai berikut :</p>
        <table class="table table-bordered mb-0">
            <thead>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='1%''>No.</th>
                    <th class="border text-center" width='40%'>Nama Barang</th>
                    <th class="border text-center" width='10%'>Quantity</th>
                    <th class="border text-center" width='14%'>Harga</th>
                    <th class="border text-center" width='15%'>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pembelian->transaction_product as $item)
                <tr class="border">
                    <td class="border" style="text-align: center;">{{$no++}}</td>
                    <td class="border">{{$item->product->name}}</td>
                    <td class="border" style="text-align: right;">{{ $item->quantity}}</td>
                    <td class="border" style="text-align: right;">{{ number_format($item->price,0,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($item->total,0,',','.') }}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td class='pr-2' style="text-align: right; border-style: none;" colspan="4"><b>TOTAL TRANSAKSI</b></td>
                    <td style="text-align: right;" class="border">{{ number_format($pembelian->sisa_tagihan+$pembelian->uang_muka,0,',','.') }}</td>
                </tr>
            </tfoot>
        </table>
        <p class="mt-1 mb-0">Demikian surat pesanan barang ini kami ajukan, atas perhatian dan kerjasamanya yang baik, kami sampaikan sekian dan terima kasih.</p>
        <table class="text-center mt-3" >
            <tr>
                <td width="30%"></td>
                <td width="5"></td>
                <td width="30%"></td>
                <td width="5"></td>
                <td width="30%"><b>Hormat Kami,</b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td height="50px"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>( {{ $pembelian->user }})</td>
            </tr>
        </table>
    </div>
    <p style="font-size: 10px; margin-left: 15px; "><i><b>Dokumen ini sah, diterbitkan oleh {{ $settings['1']['value'] }} secara elektronik melalui sistem dan tidak membutuhkan cap <br/>dan tandatangan basah.</b></i></p>             
    {{-- <footer> --}}
        {{-- <p class="m-0 p-0" style="font-size: 8px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p> --}}
        <p style="font-size: 8px; margin-left: 15px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p>
    {{-- </footer> --}}
</body>
</html>