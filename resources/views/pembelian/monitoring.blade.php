@extends('layouts.app')
@section('content')
<div class="content-wrapper pt-1">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">LAPORAN SURAT JALAN</h3>
        </div>
    </div>
    <div class="content-body">
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-blue white pb-1 pt-1" style="padding-top: 0.7rem !important;padding-bottom: 0.5rem !important;">
                        <h3 class="font-medium-3 text-bold-700 white mb-0">Filter Data</h3>
                        <a class="heading-elements-toggle" style="top: 12px;"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements" style="top: 3px;">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse" class="btn btn-danger box-shadow-1 white"><i class="ft-minus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">TAHUN</label>
                                        <div class="col-md-9">
                                            <select name="tahun_pekerjaan" id="tahun_pekerjaan" class="form-control select2" onchange="pilih_pekerjaan()">
                                                @foreach($tahun as $t)
                                                <option value="{{ $t->tahun }}">{{ $t->tahun }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">PEKERJAAN</label>
                                        <div class="col-md-9">
                                            <select name="pekerjaan_id" id="pekerjaan_id" class="form-control select2">
                                                <option value="">- Semua Pekerjaan -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">STATUS</label>
                                        <div class="col-md-9">
                                            <select name="status_id" id="status_id" class="form-control select2">
                                                <option value="">- Semua Status -</option>
                                                @foreach($status as $status)
                                                <option value="{{ $status->id }}">{{ $status->keterangan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <div class="text-right">
                                                <button type="button" class="btn btn-success" onclick="reload_list_data()">Cari <i class="ft-refresh-cw position-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    
                                    {{-- <div class="bs-callout-info callout-bordered callout-transparent p-1 mt-3" id="kolom-detail-pekerjaan" style="">
                                        <h6 class="black text-bold-600 font-medium-2 mb-1"><u>DETAIL PEKERJAAN</u></h6>
                                        <p id="detail-nama-pekerjaan"></p>
                                    </div> --}}
                                    
                                    {{-- <div class="form-group row">
                                        <label class="col-md-3 label-control">LOKASI</label>
                                        <div class="col-md-9">
                                            <select name="lokasi_id" id="lokasi_id" class="form-control select2" onchange="pilih_lokasi()" required>
                                                <option value="">- Semua Lokasi -</option>
                                                @foreach($lokasi as $lokasi)
                                                <option value="{{ $lokasi->id }}">{{ $lokasi->lokasi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control">PIHAK KETIGA</label>
                                        <div class="col-md-9">
                                            <select name="third_party_id" id="third_party_id" class="form-control select2" onchange="pilih_third_party()" required>
                                                <option value="">- Semua Pihak Ketiga -</option>
                                                @foreach($third_party as $tp)
                                                <option value="{{ $tp->id }}">[{{ $tp->tipe->keterangan }}] {{ $tp->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table display nowrap table-striped table-bordered table-sm" id="table-surat-jalan" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>ID</th>
                                        <th>NO. SURAT JALAN</th>
                                        <th>STATUS</th>
                                        <th>PEKERJAAN</th>
                                        <th>LOKASI</th>
                                        <th>TANGGAL</th>
                                        <th>PEMBUAT</th>
                                        <th>PENERIMA</th>
                                        <th class="text-center">~</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal-message-surat-jalan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning white">
                <h4 class="modal-title white">HISTORY SURAT JALAN</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal-detail-surat-jalan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">DETAIL SURAT JALAN</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    var table_data;
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
            pilih_pekerjaan();
        }, 500);
        $('#tanggal_surat_jalan').pickadate({
            format: 'dd-mm-yyyy'
        });
        // $('#tanggal_surat_jalan').daterangepicker({format: 'dd-mm-yyyy'});
    });
    
    function load_list_data(){
        table_data = $('#table-surat-jalan').DataTable({ 
            "responsive": true,
            "autoWidth": true,
            "processing": true,
            // "scrollX": true,
            "ajax": {
                "url": "{{ url('monitoring-surat-jalan-list') }}",
                "type": "POST",
                "data": function (e) {
                    e.tahun = $('#tahun_pekerjaan').val(),
                    e.pekerjaan_id = $('#pekerjaan_id').val(),
                    e.status = $('#status_id').val()
                },
                "headers": {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                "error": function (jqXHR, textStatus, errorThrown) {
                    var err = JSON.parse(jqXHR.responseText);
                    swal("ERROR!", err.message, "error");
                    // setTimeout(function(){ location.reload(); }, 1000);
                }
            },
            "columnDefs": [{ 
                "targets": [ -1 ],
                "orderable": false
            }],
        });
    }
    
    function reload_list_data(){
        table_data.ajax.reload(null, false);
    }
    
    function pilih_pekerjaan(){
        var option = '<option value="">- Semua Pekerjaan -</option>';
        $.get("{{ url('show-year-pekerjaan') }}/"+$('#tahun_pekerjaan').val(), function(result){ 
            $.each(result, function (key, value) {
                option += '<option value="'+value.id+'">'+value.nama_pekerjaan+'</option>';
            });
            $('#pekerjaan_id').html(option);
        });
    }
    
    function showData(id){
        $('#modal-detail-surat-jalan').modal('show');
        $.get("{{ url('/monitoring/detail') }}/"+id, function(result){ 
            $('#modal-detail-surat-jalan .modal-body').html(result); 
        });
    }    
    
    function messageData(id){
        $('#modal-message-surat-jalan').modal('show');
        $('#judul-modal-persetujuan').text('KIRIM SURAT JALAN');
        $('#header-background').removeClass().addClass('modal-header bg-primary white');
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('surat-jalan/get-message') }}/"+id, 
            success: function(result){
                $('#modal-message-surat-jalan .modal-body').html(result.data);
            }
        });
        
    }
    
</script>
@endsection
