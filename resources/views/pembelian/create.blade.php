@extends('layouts.app')
@section('style')
<style type="text/css">
    .remove-td td {
        padding: 0.5rem 0.5rem;
    }    
</style>
@endsection
@section('content')
<div class="content-wrapper pt-2">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title mt-1 m-0">
                Buat Penagihan Pembelian
            </h3>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('pembelian') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        @if (session('success'))
        <div class="alert alert-success mb-2" role="alert">
            {{ session('success') }}
        </div>
        @endif
        
        @if (session('error'))
        <div class="alert alert-warning mb-2" role="alert">
            {{ session('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div id="alert-form-pembelian"></div>
                <div class="card">
                    <div class="card-content">
                        <form class="form form-horizontal" method="POST" validate id="form-pembelian">
                            @csrf
                            <div class="card-body p-1" style="background-color: #B1DCF7">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <h5>Supplier <span class="red">*</span></h5>
                                            {{-- <div class="controls"> --}}
                                                <select name="pembeliansupplier" id="pembeliansupplier" class="form-control js-example-events select2" required>
                                                    <option value="">Pilih Kontak</option>
                                                    @foreach($contact as $data)
                                                    <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                                    @endforeach
                                                </select>
                                                {{-- </div> --}}
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <h5>No Referensi Supplier</h5>
                                            <input type="text" class="form-control" id="pembeliannomorreferensi" name="pembeliannomorreferensi" placeholder="">
                                        </div>
                                        <div class="col-lg-5 text-right center pt-2 pr-2">
                                            <p class="font-medium-3 text-bold-600" id="totalpembelianatas">Total Rp. 0</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body p-1">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label>Mengetahui</label>
                                                    <input type="text" class="form-control" id="pembelianuser" name="pembelianuser" required style="background-color: white;">
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat Supplier</label>
                                                    <textarea class="form-control" id="pembelianalamatsupplier" name="pembelianalamatsupplier" rows="3"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat Pengiriman</label>
                                                    <textarea class="form-control" id="pembelianalamatpengiriman" name="pembelianalamatpengiriman" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label>Tgl Transaksi</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control" id="pembeliantanggal" name="pembeliantanggal" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tgl Jatuh Tempo</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                        </div>
                                                        <input type="text" class="form-control" id="pembelianjatuhtempo" name="pembelianjatuhtempo" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Syarat Pembayaran</label>
                                                    <div class="controls">
                                                        <select class="form-control select2" id="pembeliantipepembayaran" name="pembeliantipepembayaran" style="width: 100%">
                                                            <option value="0" selected>Cash On Delivery</option>
                                                            <option value="15">Net 15</option>
                                                            <option value="30">Net 30</option>
                                                            <option value="60">Net 60</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4"  id="detail-contact">
                                        </div>
                                        
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table class="table table-hover remove-td " id="table-product-pembelian">
                                                    <thead>
                                                        <tr>
                                                            <th width="30%">Produk</th>
                                                            {{-- <th width="20%">Deskripsi</th> --}}
                                                            <th width="15%">Kuantitas</th>
                                                            <th width="15%">Harga Satuan</th>
                                                            <th width="24%">Jumlah</th>
                                                            <th width="1%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <button type="button" class="btn btn-info btn-min-width box-shadow-2 mt-1" id="button-add-row-product-pembelian"><i class="la la-plus-square"></i> Tambah Data</button>
                                            
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-lg-4">
                                            <fieldset class="form-group form-group-style">
                                                <label for="textbox2">Pesan</label>
                                                <textarea class="form-control" id="pembelianpesan" name="pembelianpesan" rows="3"></textarea>
                                            </fieldset>
                                            <fieldset class="form-group form-group-style">
                                                <label for="textbox2">Memo</label>
                                                <textarea class="form-control" id="pembelianmemo" name="pembelianmemo" rows="3"></textarea>
                                            </fieldset>
                                        </div>
                                        <div class="col-lg-3">
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="table-responsive">
                                                <table class="table table-borderless mb-0 p-0 m-0">
                                                    <tbody>
                                                        <tr>
                                                            <td>Sub Total</td>
                                                            <td class="type-info text-right">
                                                                <p id="totalpembeliansubtotal" class="m-0">Rp. 0</p>
                                                                <input type="text" id="hiddentotalpembeliansubtotal" name="pembeliansubtotal" value="0" hidden>
                                                            </td>
                                                        </tr>
                                                        <tr hidden>
                                                            <td>Diskon per baris</td>
                                                            <td class="type-info text-right">
                                                                <p id="diskonpembelianperbaris" class="m-0">Rp. 0</p>
                                                                <input type="text" id="hiddendiskonpembelianperbaris" value="0" hidden>
                                                            </td>
                                                        </tr>
                                                        <tr hidden>
                                                            <td>Pajak per baris</td>
                                                            <td class="type-info text-right">
                                                                <p id="pajakpembelianperbaris" class="m-0">Rp. 0</p>
                                                                <input type="text" id="hiddenpajakpembelianperbaris" value="0" hidden>
                                                            </td>
                                                        </tr>
                                                        <tr hidden>
                                                            <td><p style="margin-bottom: 5px">Pemotongan / Diskon</p>
                                                                <input type="text" class="form-control input-sm" id="pembelianpemotongan" name="pembelianpemotongan" placeholder="" value="0">
                                                            </td>
                                                            <td class="type-info text-right pt-2" id="showpembelianpemotongan">Rp. 0</td>
                                                        </tr>
                                                        <tr hidden>
                                                            <td>Total</td>
                                                            <td class="type-info text-right"><p id="totalpembelianbawah" class="m-0">Rp. 0</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p style="margin-bottom: 5px">Uang Muka</p>
                                                                <input type="text" class="form-control input-sm" id="pembelianuangmuka" name="pembelianuangmuka" placeholder="" value="0">
                                                            </td>
                                                            <td class="type-info text-right pt-2" id="showpembelianuangmuka">Rp. 0</td>
                                                        </tr>
                                                        <tr>
                                                            <td><h2 class="m-0">Sisa Tagihan</h2>
                                                                <input type="text" id="pembeliansisatagihan" name="pembeliansisatagihan" value="0" hidden>
                                                            </td>
                                                            <td class="type-info text-right"><h2 id="sisatagihanpembelian" class="m-0">Rp. 0</h2></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions right">
                                        <button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</button>
                                        <button type="submit" class="btn btn-primary" >Buat Pembelian</button>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('script')
    <script type="text/javascript">
        var rowIdx = 0, sumdiskon = 0, sumpajak = 0, sumtotal = 0;
        $(document).ready(function () {
            if( /Android|iPhone|iPad|iPod|IEMobile/i.test(navigator.userAgent) ) {
                $('.select2').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                });
            }else{
                $('.select2').select2({
                    dropdownAutoWidth : true,
                    width: '100%'
                });
                setTimeout(function(){ $('#hidden-menu').click() }, 1000);
            }
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
            addRowsTablePembelian();
            $('#pembeliantanggal, #pembelianjatuhtempo').pickadate({
                format: 'dd-mm-yyyy',
                today: '',
                close: 'Close',
                clear: ''
            });
        });
        
        $('#pembeliansupplier').on('change', function() {
            
            if ($(this).val()) {
                $.ajax("{{ url('get-address-contact') }}/"+$(this).val(),{
                    success: function (data, status, xhr) {
                        if (data) {
                            $('#pembelianalamatsupplier').val((data.alamat));
                            $('#pembelianalamatpengiriman').val((data.alamat));
                            $('#detail-contact').html('<div class="bs-callout-info callout-bordered callout-transparent p-1 mt-2">'+
                                '<h6 class="black text-bold-600 font-medium-2">'+data.nama+'</h6>'+
                                '<h6 class="black font-small-3 mb-1">'+data.perusahaan+'</h6>'+
                                '<p>'+nl2br(data.alamat)+'</p>'+'<p>'+(data.email?data.email:'')+'</p>'+'<p>'+(data.phone?data.phone:'')+'</p></div>'
                                );
                            }
                        }
                    });
                }
            });
            
            $('#button-add-row-product-pembelian').on('click', function () {
                addRowsTablePembelian();
            });
            
            $('#table-product-pembelian tbody').on('click', '.remove', function () {
                var row = $(this).closest('tr');
                row.remove();
                sumdiskon = 0;
                $("#table-product-pembelian tbody input[name='producttotaldiskon[]']").map(function(){ return $(this).val();}).get().forEach(setDiskonPembelian);
                sumpajak = 0;
                $("#table-product-pembelian tbody input[name='producttotalpajak[]']").map(function(){ return $(this).val();}).get().forEach(setPajakPembelian);
                sumtotal = 0;
                $("#table-product-pembelian tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPembelian);
                setGrandTotal();
            });
            
            function addRowsTablePembelian() {
                if(!$('.check-text-isset').filter(function(){ return !this.value.trim(); }).length){
                    $('#table-product-pembelian tbody').append(`
                    <tr>
                        <td style="min-width: 300px;">
                            <select class="form-control select2 select2-table check-text-isset" id="productpembelian" name="productpembelian[]">
                                <option value=""><i>- Pilih Produk -</i></option>
                                @foreach($product as $p)
                                <option value="{{$p->id}}">{{$p->name}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td><input type="text" class="form-control class-money-format class-quantity" id="productquantity" name="productquantity[]" value="1" style="min-width: 170px;"></td>
                        <td>
                            <div class="input-group" style="min-width: 170px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input type="text" class="form-control text-right class-money-format class-quantity" id="productharga" name="productharga[]" value="0">
                            </div>
                        </td>
                        <td>
                            <div class="input-group" style="min-width: 170px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input type="text" class="form-control text-right" id="producttotal" name="producttotal[]" value="0">
                            </div>
                        </td>
                        <td><button type="button" class="btn btn-icon btn-pure danger remove"><i class="la la-close"></i></button></td>
                    </tr>`);
                    
                    $('.select2-table').select2()
                    .on("change", function(e) {
                        if ($(this).val() != '') {
                            $(this).closest('tr').find('#productharga').load("{{ url('buy-price-product') }}/"+$(this).val(),function(response, status, xhr){
                                $(this).val(response);
                                var harga = response == 0 ? 1 : response;
                                var quantity = $(this).closest('tr').find('#productquantity').val() == 0 ? [1,0] : $(this).closest('tr').find('#productquantity').val().toString().replace(/[^,\d]/g, "").split(",");
                                var total_temp = (harga * parseFloat(quantity[0]+'.'+quantity[1]));
                                var total = total_temp.toFixed(0).toString().split(".");
                                
                                var total_diskon = $(this).closest('tr').find('#producttotaldiskon').val();
                                
                                $(this).closest('tr').find('#producttotal').val(formatRupiah(total[0]+','+total[1], ''));
                                $(this).closest('tr').find('#productharga').val(formatRupiah(harga, ''));
                                sumtotal = 0;
                                $("#table-product-pembelian tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPembelian);
                                setGrandTotal();
                            });
                        }                
                    });
                    
                    $('.class-money-format').on("keyup", function(e) {
                        if ($(this).val() != '') {
                            var harga = $(this).closest('tr').find('#productharga').val() == 0 ? 1 : $(this).closest('tr').find('#productharga').val().toString().replace(/[^,\d]/g, "");
                            var quantity = $(this).closest('tr').find('#productquantity').val() == 0 ? 1 : $(this).closest('tr').find('#productquantity').val().toString().replace(/[^,\d]/g, "").split(",");
                            var total_temp = harga * parseFloat(quantity[0]+'.'+quantity[1]);
                            var total = total_temp.toFixed(2).toString().split(".");
                            
                            $(this).closest('tr').find('#producttotal').val(formatRupiah(total[0]+','+total[1], ''));
                            $(this).closest('tr').find('#productharga').val(formatRupiah(harga, ''));
                            sumtotal = 0;
                            $("#table-product-pembelian tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPembelian);
                            setGrandTotal();
                        }
                    });
                    
                    $('.class-quantity').focus(function(e) {
                        if ($(this).val() == 1) {
                            $(this).val('');
                        }
                    });
                    $('.class-quantity').focusout(function(e){
                        if ($(this).val() == '') {
                            $(this).val('1');
                            var harga = $(this).closest('tr').find('#productharga').val() == 0 ? 1 : $(this).closest('tr').find('#productharga').val().toString().replace(/[^,\d]/g, "");
                            var quantity = $(this).closest('tr').find('#productquantity').val() == 0 ? 1 : $(this).closest('tr').find('#productquantity').val().toString().replace(/[^,\d]/g, "").split(",");
                            var total_temp = harga * parseFloat(quantity[0]+'.'+quantity[1]);
                            var total = total_temp.toFixed(0).toString().split(".");
                            
                            $(this).closest('tr').find('#producttotal').val(formatRupiah(total[0]+','+total[1], ''));
                            $(this).closest('tr').find('#productharga').val(formatRupiah(harga, ''));
                            sumtotal = 0;
                            $("#table-product-pembelian tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPembelian);
                            setGrandTotal();
                        }
                    });
                    
                    ;
                }    
            }
            
            $('#pembelianpemotongan, #pembelianuangmuka').on("keyup", function(e) {
                setGrandTotal();
                $('#showpembelianpemotongan').html(formatRupiah($('#pembelianpemotongan').val(), 'Rp '));
                $('#showpembelianuangmuka').html(formatRupiah($('#pembelianuangmuka').val(), 'Rp '));
            });
            
            function setTotalPembelian(total) {
                temp_total = total.toString().replace(/[^,\d]/g, "").split(",");
                sumtotal += parseFloat(temp_total[0]+'.'+temp_total[1]);
                var temp_sum = sumtotal.toFixed(0).toString().split(".");
                $('#totalpembelianatas, #totalpembeliansubtotal').html(formatRupiah(temp_sum[0]+','+temp_sum[1], 'Rp '));
                $('#hiddentotalpembeliansubtotal').val(sumtotal);
            }
            
            function setGrandTotal(){
                var pemotong = $('#pembelianpemotongan').val() == '' ? 0 : $('#pembelianpemotongan').val();
                var grand_total_temp = parseFloat(parseFloat($('#hiddentotalpembeliansubtotal').val()-$('#hiddendiskonpembelianperbaris').val())+parseFloat($('#hiddenpajakpembelianperbaris').val())-parseFloat(pemotong));
                var grand_total = grand_total_temp.toFixed(0).toString().split(".");
                $('#totalpembelianbawah').html(formatRupiah(grand_total[0]+','+grand_total[1], 'Rp '));
                var uangmuka = $('#pembelianuangmuka').val() == '' ? 0 : $('#pembelianuangmuka').val();
                var sisa_uangmuka_temp = parseFloat(grand_total_temp-uangmuka);
                var sisa_uangmuka = sisa_uangmuka_temp.toFixed(0).toString().split(".");
                $('#sisatagihanpembelian').html(formatRupiah(sisa_uangmuka[0]+','+sisa_uangmuka[1], 'Rp '));
                $('#pembeliansisatagihan').val(sisa_uangmuka_temp);
            }
            
            $( "#pembelianuangmuka, #pembelianpemotongan" ).focus(function(e) {
                if ($(this).val() == 0) {
                    $(this).val('');
                }
            });
            $( "#pembelianuangmuka, #pembelianpemotongan" ).focusout(function(e){
                if ($(this).val() == '') {
                    $(this).val('0');
                }
            });
            
            function formatRupiah(angka, prefix) {
                var number_string = angka.toString().replace(/[^,\d]/g, ""),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);
                if (ribuan) {
                    separator = sisa ? "." : "";
                    rupiah += separator + ribuan.join(".");
                }
                rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
                return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
            }
            
            $('#pembeliantipepembayaran, #pembeliantanggal').change( function(e) { 
                var temp_date = $("#pembeliantanggal").val().split('-');
                console.log(temp_date[2]+'-'+temp_date[1]+'-'+temp_date[0]);
                var date = new Date(temp_date[2]+'-'+temp_date[1]+'-'+temp_date[0]);
                date.setDate(date.getDate() + parseInt($('#pembeliantipepembayaran').val()));
                $("#pembelianjatuhtempo").val(date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
            });
            
            
            $("#form-pembelian").submit(function(e){
                e.preventDefault();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    url: "{{ url('store-pembelian') }}",
                    type: "POST",
                    data: new FormData($('#form-pembelian')[0]),
                    processData: false,
                    contentType: false,
                    dataType: "JSON",
                    success: function(result) {
                        swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                        window.open("{{ url('pembelian-po') }}/"+result.id);
                        window.location = "{{ url('pembelian') }}";
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                        $.each(err.errors, function (key, value) {
                            $('#alert-form-pembelian').html('<div class="alert alert-danger mb-2" role="alert">'+key+' : '+value+'</div>');
                        });
                    }
                });
                
                
            });
            
        </script>
        @endsection
        