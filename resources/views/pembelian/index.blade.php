@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <small>Transaksi</small>
            <h2 class="content-header-title">Pembelian</h2>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('pembelian/new') }}" type="button" class="btn btn-info round box-shadow-2 px-2 btn-glow"><i class="ft-plus icon-left"></i> Buat Pembelian Baru</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        @if (session('success'))
        <div class="alert alert-success mb-2" role="alert">
            {{ session('success') }}
        </div>
        @endif
        
        @if (session('error'))
        <div class="alert alert-warning mb-2" role="alert">
            {{ session('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="card box-shadow-0 border-warning">
                    <div class="card-header card-head-inverse bg-warning p-1">
                        <h4 class="card-title text-white">Pembelian Belum Dibayar (IDR)</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body p-1">
                            <small>Total</small>
                            <p class="card-text">Rp {{ number_format($hutang->TOTAL,0,',','.') }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="card box-shadow-0 border-primary">
                    <div class="card-header card-head-inverse bg-primary p-1">
                        <h4 class="card-title text-white">Pelunasan 30 Hari Terakhir (IDR)</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body p-1">
                            <small>Total</small>
                            <p class="card-text">Rp {{ number_format($lunas->TOTAL,0,',','.') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="row pb-1 pl-1 pr-1">
                                <div class="col-12 col-md-4">
                                    <div class="input-group mb-1">
                                        <select name="periode_laporan" id="periode_laporan" class="form-control">
                                            @foreach($periode as $data)
                                            @if ($data->tanggal == now()->format('Y-m'))
                                            <option value="{{ $data->tanggal }}" selected>{{ $data->month }} {{ $data->year }}</option>
                                            @else
                                            <option value="{{ $data->tanggal }}">{{ $data->month }} {{ $data->year }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <div class="input-group-append">
                                            <button class="btn btn-success" type="button" onclick="reload_list_data()">Cari!</button>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-12 col-md-8 text-right">
                                    <button class="btn btn-info" id="buttonDetail"><i class="ft-eye"></i> Detail</button>
                                    <button class="btn btn-success" id="buttonPrint"><i class="ft-printer"></i> PO</button>
                                </div>
                            </div>                                    
                            <table class="table table-xs table-striped table-bordered" id="table-pembelian" style="width: 100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>ID</th>
                                        <th>Tanggal</th>
                                        <th>Nomor</th>
                                        <th>Supplier</th>
                                        <th>Status</th>
                                        <th>Sisa Pembayaran (IDR)</th>
                                        <th>Total Pembelian (IDR)</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var table_data,tanggal;
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();        
        }, 500);
        setTimeout(function(){ $('.alert-success, .alert-warning').remove() }, 5000);
    });
    
    $('#buttonEdit').click( function () {
        var rows = table.rows('.selected').data().length;
        if(rows > 0){
            var edit =   table.row('.selected').data()[0];  
            var status =   table.row('.selected').data()[10];
            if(status == 'OPEN')
            {					
                $('#mymodal').modal('show');
                get_data_to_update(edit);
            }else{
                swal("Info!", "Delivery Order yang anda pilih sedang berjalan", "success");
            }   
        }
    });
    
    function load_list_data(){
        table_data = $('#table-pembelian').DataTable({ 
            "responsive": true,
            "autoWidth": true,
            "processing": true,
            "ajax": {
                "url": "{{ url('datatable-pembelian') }}",
                "type": "POST",
                "data": function ( d ) {
                    d.tanggal = $('#periode_laporan').val();
                },
                "headers": {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                "error": function (jqXHR, textStatus, errorThrown) {
                    var err = JSON.parse(jqXHR.responseText);
                }
            },
            "columnDefs": [{ 
                "targets": [ 0 ],
                "visible": false
            }],
        });
        
        $('#table-pembelian tbody').on( 'dblclick', 'tr', function () {
            table_data.$('tr.detail').removeClass('detail');
            $('#table-pembelian tbody').removeClass('detail');
            $(this).addClass('detail');
            get_detail();
        });
        
        $('#table-pembelian tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_data.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
        
    }
    
    function reload_list_data(){
        table_data.ajax.reload(null, false);
    }
    
    function get_detail(){
        if(table_data.rows('.detail').data().length > 0){
            location.assign("{{ url('pembelian/detail') }}/"+table_data.row('.detail').data()[0]);
        }
    }
    
    $('#buttonPrint').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            window.open("{{ url('pembelian-po') }}/"+table_data.row('.selected').data()[0]);
        }
    });

    $('#buttonDetail').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            location.assign("{{ url('pembelian/detail') }}/"+table_data.row('.selected').data()[0]);
        }
    });
    
</script>
@endsection
