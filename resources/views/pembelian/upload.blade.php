@extends('layouts.app')
@include('suratjalan.surat_jalan')
@section('content')
<div class="content-wrapper pt-1">
    <div class="content-header row pr-2">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">DETAIL SURAT JALAN</h3>
            <small>Divisi Operasi & Teknik</small>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('surat-jalan') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="lists">
            @yield('surat_jalan')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4 class="card-title" id="basic-layout-square-controls">Upload File Attachment</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form" id="form-upload-file-surat-jalan" enctype="multipart/form-data" validate>
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-10">
                                                <fieldset class="form-group">
                                                    <div class="custom-file">
                                                        <input type="text" id="id_surat_jalan" name="id_surat_jalan" hidden="" value="{{ $surat_jalan->id }}" />
                                                        <input type="text" id="status_approval" name="status_approval" hidden="" value="3"/>
                                                        <input type="file" class="custom-file-input" id="file_surat_jalan" name="file_surat_jalan[]" multiple required/>
                                                        <label class="custom-file-label">Choose file</label>
                                                    </div>
                                                    <p class="mb-0"><strong>Format</strong> : jpg, jpeg, png, pdf</p>
                                                </fieldset>                                        
                                            </div>
                                            <div class="col-2">
                                                <button type="reset" class="btn btn-danger clear"><i class="ft-delete"></i></button>
                                            </div>
                                        </div> 
                                        <ul id="filenames">
                                        </ul>
                                    </div>
                                    <div class="form-actions right pb-0">
                                        <button type="button" class="btn btn-success" onclick="proses_upload()"><i class="la la-check-square-o"></i> Proccess</button>
                                        <button type="submit" id="button-proses-upload" hidden></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade text-left" id="modal-persetujuan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info white">
                <h4 class="modal-title white" >KETERANGAN UPLOAD FILE</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-persetujuan" validate>
                    <fieldset class="form-group">
                        <input type="text" id="id_surat_jalan" name="id_surat_jalan" hidden="" value="{{ $surat_jalan->id }}"/>
                        <input type="text" id="status_approval" name="status_approval" hidden="" value="3"/>
                        <textarea class="form-control" id="keterangan_approval" name="keterangan_approval" rows="3" placeholder="Tulis keterangan">-</textarea>
                        <button type="submit" id="button-submit-modal" hidden></button>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">BATAL</button>
                <button type="button" class="btn btn-outline-success pull-right" id="button-proses-modal">PROSES</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('.input-images-1').imageUploader();
    });
    $('#file_surat_jalan').change(function(){
        $('#filenames').html('');
        for(var i = 0 ; i < this.files.length ; i++){
            var fileName = this.files[i].name;
            $('#filenames').append('<li>' + fileName + '</li>');
        }
    });
    $('.clear').click(function(){
        $("#form-upload-file-surat-jalan")[0].reset();
        $('#filenames').html('');
    });
    
    $("#form-upload-file-surat-jalan").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('surat-jalan/proses-upload') }}",
            type: "POST",
            data: new FormData($('#form-upload-file-surat-jalan')[0]),
            cache: false,
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(data) {
                $("#form-upload-file-surat-jalan").modal('hide');
                $('#button-submit-modal').click();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    });
    
    
    $("#form-persetujuan").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('surat-jalan/submit') }}",
            type: "POST",
            data: new FormData($('#form-persetujuan')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(data) {
                if (data) {
                    swal("Sukses!", "Data berhasil di proses!.", "success");
                    window.location = "{{ url('surat-jalan') }}";
                }else{
                    swal("Gagal!", "Data gagal di proses!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    });
    
    $('#button-proses-modal').click(function(){
        $('#button-proses-upload').click();
    })
    
    function proses_upload(){
        $('#modal-persetujuan').modal('show');
    }
    
</script>
@endsection