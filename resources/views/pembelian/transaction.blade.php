@section('transaction')
<div class="row">
    <div class="col-md-12">
        <div class="card mb-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div id="basic-list">
                            <ul class="list-group list">
                                <li class="list-group-item">
                                    <h3 class="name font-medium-2 text-bold-600" style="margin-bottom: 0px;">SUPPLIER :</h3>
                                    <p class="mb-0">{{ $transaction->contact->nama }}<br><b>{{ $transaction->contact->perusahaan }}</b><br><br>{!! nl2br(e($transaction->alamat)) !!}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="basic-list">
                            <ul class="list-group list">
                                {{-- <li class="list-group-item">
                                    <h3 class="name font-medium-2 text-bold-600" style="margin-bottom: 0px;">NO PENJUALAN :</h3>
                                    <p class="mb-0"></p>
                                </li> --}}
                                <li class="list-group-item">
                                    <h3 class="name font-medium-2 text-bold-600" style="margin-bottom: 0px;">TANGGAL TRANSAKSI :</h3>
                                    <p class="mb-0">{{ date('d F Y',strtotime($transaction->tgl_transaksi)) }}</p>
                                </li>
                                <li class="list-group-item">
                                    <h3 class="name font-medium-2 text-bold-600" style="margin-bottom: 0px;">TANGGAL TERIMA :</h3>
                                    <p class="mb-0">{{ ($transaction->status==1?date('d F Y',strtotime($transaction->tgl_jatuh_tempo)):'') }}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="basic-list">
                            <ul class="list-group list">
                                <li class="list-group-item">
                                    <h3 class="name font-medium-2 text-bold-600" style="margin-bottom: 0px;">ALAMAT PENGIRIMAN :</h3>
                                    <p class="mb-0">{!! nl2br(e($transaction->alamat_kirim)) !!}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card mb-1">
            <div class="card-body">
                <form class="form-bordered">
                    <h4 class="form-section"><i class="ft-box"></i> LIST BARANG</h4>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered text-center table-sm">
                        <thead>
                            <tr>
                                <th class="sort text-center">No</th>
                                <th class="sort text-center">Nama Barang</th>
                                <th class="sort text-center">Quantity</th>
                                <th class="sort text-center">Satuan</th>
                                <th class="sort text-center">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            @php
                            $no = 0;
                            $total = 0;
                            @endphp
                            @if ($transaction->transaction_product)
                            @foreach ($transaction->transaction_product as $tp)
                            @php
                            $total = $total + $tp->total;
                            @endphp
                            <tr>
                                <td>{{ ++$no }}</td>
                                <td>{{ $tp->product->name }}</td>
                                <td>{{ number_format($tp->quantity,3,',','.') }}</td>
                                <td class="text-right">{{ number_format($tp->price,0,',','.') }}</td>
                                <td class="text-right">{{ number_format($tp->total,0,',','.') }}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="row justify-content-between">
                    <div class="col-md-5">
                        <div id="basic-list">
                            <ul class="list-group list">
                                <li class="list-group-item">
                                    <h3 class="name font-medium-2 text-bold-600" style="margin-bottom: 0px;">PESAN :</h3>
                                    <p class="mb-0">{{ nl2br(e($transaction->pesan)) }}</p>
                                </li>
                                <li class="list-group-item">
                                    <h3 class="name font-medium-2 text-bold-600" style="margin-bottom: 0px;">MEMO :</h3>
                                    <p class="mb-0">{{ nl2br(e($transaction->memo)) }}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="table-responsive">
                            <table class="table table-borderless mb-0 p-0 m-0">
                                <tbody>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td>:</td>
                                        <td class="type-info text-right">
                                            <p id="totalpenjualansubtotal" class="m-0">Rp {{ number_format($total,0,',','.') }}</p>
                                        </td>
                                    </tr>
                                    <tr hidden>
                                        <td>Pemotongan / Diskon</td>
                                        <td>:</td>
                                        <td class="type-info text-right">{{ number_format($transaction->nilai_pemotong,0,',','.') }}</td>
                                    </tr>
                                    <tr hidden>
                                        <td>Total</td>
                                        <td>:</td>
                                        <td class="type-info text-right"><p class="m-0">Rp {{ number_format($total-$transaction->nilai_pemotong,0,',','.') }}</p></td>
                                    </tr>
                                    <tr>
                                        <td><h3>Uang Muka</h3></td>
                                        <td>:</td>
                                        <td class="type-info text-right"><h3>Rp {{ number_format($transaction->uang_muka,0,',','.') }}</h3></td>
                                    </tr>
                                    <tr>
                                        <td><h2>Sisa Tagihan</h2></td>
                                        <td>:</td>
                                        <td class="type-info text-right"><h2 class="m-0">Rp {{ number_format($transaction->sisa_tagihan,0,',','.') }}</h2></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
</div>
@if ($transaction->status==0)
<button class="btn btn-danger round box-shadow-2 px-2" onclick="pembelian_delete()">HAPUS PEMBELIAN <i class="ft-trash-2"></i></button>
<a href="#" type="button" class="btn btn-success round box-shadow-2 px-2 pull-right " onclick="pembelian_confirm()">TERIMA BARANG <i class="ft-check-square"></i></a>
@endif
<script type="text/javascript">
    function pembelian_confirm() {
        swal({
            title: "Are you sure?",
            text: "Barang akan dicatat kedalam stok!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Terima!",
                    value: true,
                    visible: true,
                    className: "btn-success",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                location.replace("{{ url('pembelian-confirm') }}/{{ $transaction->id }}");
            } else {
                swal("Batal!", "Transaksi belum diterima", "info");
            } 
        });
    }
    
    function pembelian_delete() {
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "btn-danger",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                location.replace("{{ url('pembelian-delete') }}/{{ $transaction->id }}");
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
</script>
@endsection
@section('product')
@endsection