@extends('layouts.app')
@section('content')
<div class="content-wrapper pt-0">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12">
                        <h3 class="content-header-title">MASTER KATEGORI</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="float-md-right">
                            <button type="button" id="button-tambah-category" name="button-tambah-category" class="btn btn-info round box-shadow-2 px-2" data-toggle="modal" data-target="#modal-category"><i class="ft-plus icon-left"></i> Tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-warning ml-1 mb-1" id="buttonEdit"><i class="ft-edit"></i> Edit</button>
                            <button class="btn btn-danger ml-1 mb-1" id="buttonDelete"><i class="ft-close"></i> Delete</button>
                            <table class="table table-striped selection-deletion-row display nowrap table-xs" id="table-category" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>No.</th>
                                        <th>Nama</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-category" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-category" enctype="multipart/form-data" validate>
                <div class="modal-header bg-success white">
                    <h4 class="modal-title white" id="title-form-category">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert-form"></div>
                            <div class="form-group">
                                <h5>Nama Kategori <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="id_kategori" id="id_kategori" hidden>
                                    <input type="text" name="nama_kategori" id="nama_kategori" class="form-control" required data-validation-required-message="This field is required" placeholder="GARAM">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    
    var table_category;
    var default_image = 'app-assets/images/gallery/1.jpg'; 
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    function load_list_data(){
        table_category = $('#table-category').DataTable({
            processing: true,
            serverSide: true,
            ordering: false, 
            ajax: {
                url: "{{ url('datatable-category') }}",
                headers: {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    setTimeout(function(){ category.reload(); }, 1000);
                }
            },
            columnDefs: [{ 
                targets: [ 0 ],
                visible: false
            }],
        });

        // $('#table-category tbody').on( 'dblclick', 'tr', function () {
        //     $('#table-category tbody').removeClass('detail');
        //     $(this).addClass('detail');
        //     if(table_category.rows('.detail').data().length > 0){
        //         getData(table_category.row('.detail').data()[0]);
        //     }
            
        // });
        
        $('#table-category tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_category.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });        
    }
    
    $('#buttonEdit').click( function () {
        if(table_category.rows('.selected').data().length > 0){
            getData(table_category.row('.selected').data()[0]);
        }
    });

    $('#buttonDelete').click( function () {
        if(table_category.rows('.selected').data().length > 0){
            deleteData(table_category.row('.selected').data()[0]);
        }
    });
    
    function reload_list_data(){
        table_category.ajax.reload(null, false);
    }
    
    $('#button-tambah-category').click(function () {
        $("#form-category")[0].reset();
        $('#preview-file-user').attr('src', default_image);
    });
    
    $("#form-category").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-category') }}",
            type: "POST",
            data: new FormData($('#form-category')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-category").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
            }
        });
        
        
    });
    
    function getData(id){
        $('#title-form-category').html('Edit Data');
        $.ajax({
            url : "{{ url('get-category') }}/"+id,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-category').modal('show');
                    $('#id_kategori').val(result.data.id);
                    $('#nama_kategori').val(result.data.name);
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-category') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    
</script>
@endsection
