@extends('layouts.auth')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section class="flexbox-container">
            <div class="col-12 d-flex align-items-centeer justify-content-center">
                <div class="col-md-4 box-shadow-5 p-0">
                    
                    @error('password')
                    <div class="alert bg-warning alert-dismissible mb-2" role="alert">
                        <strong>Warning!</strong> {{ $message }}
                    </div>
                    @enderror
                    @error('email')
                    <div class="alert bg-warning alert-dismissible mb-2" role="alert">
                        <strong>Warning!</strong> {{ $message }}
                    </div>
                    @enderror
                    
                    
                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0">
                            <div class="card-title text-center">
                                <img src="{{asset('images/'.$settings['0']['value'])}}" alt="branding logo" width="40%"> <p class="font-medium-4 pt-2">{{$settings['1']['value']}}</p>
                              </div>
                              <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2 mb-0">
                                <span>{{ __('Login') }}</span>
                              </h6>
            
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form method="POST" class="form-horizontal" action="{{ route('login') }}" validate>
                                    @csrf
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Your Email"
                                        required>
                                        <div class="form-control-position">
                                            <i class="ft-user"></i>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password"
                                        required>
                                        <div class="form-control-position">
                                            <i class="ft ft-lock"></i>
                                        </div>
                                    </fieldset>
                                    <div class="form-group row">
                                        <div class="col-md-6 col-12 text-center text-sm-left">
                                        </div>
                                        <div class="col-md-6 col-12 float-sm-left text-center text-sm-right"><a href="{{ route('password.request') }}" class="card-link">Forgot Password?</a></div>
                                    </div>
                                    <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-log-in"></i> {{ __('LOGIN') }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection


@section('content1')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="text-center">
                <img src="{{asset('images/logo/logo-full.png')}}" width="200">
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="card" >
                <div class="card-header text-center"><h3><b>{{ __('Login') }}</b></h3></div>
                
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                
                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
