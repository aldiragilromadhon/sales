@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                @foreach ($menu as $m)
                                <div class="col-md-3 col-6">
                                    <div class="form-group text-center">
                                        <a href="{{ url($m->target) }}" class="btn btn-block btn-float btn-float-lg btn-outline-cyan"><i class="{{ $m->icon }}"></i><span>{{ $m->menu }}</span></a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h6 class="text-muted">Total Pembelian Bulan Ini </h6>
                                            <h3>Rp {{ number_format($transaksi->BELI,2,',','.') }}</h3>
                                        </div>
                                        <div class="align-self-center">
                                            <i class="icon-bag success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="card pull-up">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="media-body text-left">
                                    <h6 class="text-muted">Total Penjualan Bulan Ini </h6>
                                    <h3>Rp {{ number_format($transaksi->JUAL,2,',','.') }}</h3>
                                </div>
                                <div class="align-self-center">
                                    <i class="icon-basket success font-large-2 float-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="recent-sales" class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Product Sale</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content mt-1">
                        <div class="table-responsive">
                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">Product</th>
                                        <th class="border-top-0">Categories</th>
                                        <th class="border-top-0">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $p)
                                    <tr>
                                        <td class="text-truncate">{{ $p->name }}</td>
                                        <td><button type="button" class="btn btn-sm btn-outline-danger round">{{ $p->m_category->name }}</button></td>
                                        <td class="text-truncate">{{ number_format($p->TOTAL,2,',','.').' '.$p->m_unit->name }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="../public/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
<script src="../public/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
<script src="../public/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
<script src="../public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
<script src="../public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js" type="text/javascript"></script>
<script src="../public/app-assets/data/jvector/visitor-data.js" type="text/javascript"></script>
<script src="../public/app-assets/js/scripts/pages/dashboard-sales.js" type="text/javascript"></script>
@endsection
@section('content_old')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    
                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
