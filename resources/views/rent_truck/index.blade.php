@extends('layouts.app')
@section('content')
<div class="content-wrapper pt-0">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12">
                        <h3 class="content-header-title">MASTER DATA TRUK</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="float-md-right">
                            <button type="button" id="button-tambah-rent-truck" name="button-tambah-rent-truck" class="btn btn-info round box-shadow-2 px-2" data-toggle="modal" data-target="#modal-rent-truck"><i class="ft-plus icon-left"></i> Tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-warning ml-1 mb-1" id="buttonEdit"><i class="ft-edit"></i> Edit</button>
                            <button class="btn btn-danger ml-1 mb-1" id="buttonDelete"><i class="ft-close"></i> Delete</button>
                            <table class="table table-striped zero-configuration font-small-3" id="table-rent-truck" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Foto</th>
                                        <th>Nama / Nomor</th>
                                        <th>No HP</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-rent-truck" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-rent-truck" enctype="multipart/form-data" validate>
                <div class="modal-header bg-success white">
                    <h4 class="modal-title white" id="title-form-rent-truck">Data Kendaraan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert-form"></div>
                            <div class="form-group">
                                <h5>Sopir <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="id_rent_truck" id="id_rent_truck" hidden>
                                    <input type="text" name="perusahaan" id="perusahaan" class="form-control" required data-validation-required-message="This field is required" placeholder="">
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <h5>Merek <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="merek" id="merek" class="form-control">
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <h5>Tahun<span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="tahun" id="tahun" class="form-control" value="{{ date('Y') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Nomor Polisi <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="no_polisi" id="no_polisi" class="form-control" required data-validation-required-message="This field is required" placeholder="" max="11">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Nomor HP <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="no_hp" id="no_hp" class="form-control" required data-validation-required-message="This field is required" placeholder="" max="14">
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <h5>Tanggal Sewa <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="tgl_sewa" id="tgl_sewa" class="form-control" required data-validation-required-message="This field is required" placeholder="" value="{{ date('d-m-Y') }}">
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <h5>Tanggal Kembali <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="tgl_berakhir" id="tgl_berakhir" class="form-control" required data-validation-required-message="This field is required" placeholder="" value="{{ date('d-m-Y') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Foto Kendaraan</h5>
                                <div class="controls">
                                    <input type="file" id="foto" name="foto" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img-thumbnail img-fluid" src="#" itemprop="thumbnail" alt="File foto" id="preview-file-user"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    
    var table_rent_truck;
    var default_image = "{{ asset('app-assets/images/gallery/1.jpg') }}";

    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
        $("#foto").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview-file-user').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        
    });
    
    function load_list_data(){
        table_rent_truck = $('#table-rent-truck').DataTable({
            processing: true,
            serverSide: true,
            ordering: false, 
            ajax: {
                url: "{{ url('datatable-renttruck') }}",
                headers: {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // setTimeout(function(){ location.reload(); }, 1000);
                }
            },
            columns: [
            {
                data: 'action', 
                name: 'action', 
                orderable: false, 
                searchable: false
            },
            {data: 'foto', name: 'foto', render:function(data, type, full, meta){
                if(data == null){
                    return '<img class="img-thumbnail img-fluid" src="images/'+data+'" itemprop="thumbnail"/>'
                }else{
                    return ''
                }
            }},
            // {data: 'foto', name: 'foto'},
            {data: 'no_polisi', name: 'no_polisi', render: function ( data, type, row ) {
                return row.perusahaan + ' [' + row.no_polisi + ']';
            }},
            {data: 'no_hp', name: 'no_hp'}
            ],
            columnDefs: [{ "orderable": false, "targets": 0 }],
        });
        
        $('#table-rent-truck tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_rent_truck.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
    }
    
    $('#buttonEdit').click( function () {
        if(table_rent_truck.rows('.selected').data().length > 0){
            var data = table_rent_truck.rows('.selected').data()[0];
            getData(data.id);
        }
    });
    
    $('#buttonDelete').click( function () {
        if(table_rent_truck.rows('.selected').data().length > 0){
            var data = table_rent_truck.rows('.selected').data()[0];
            deleteData(data.id);
        }
    });
    
    function reload_list_data(){
        table_rent_truck.ajax.reload(null, false);
    }
    
    $('#button-tambah-rent-truck').click(function () {
        $('#alert-form').html('');
        $("#form-rent-truck")[0].reset();
        $('#preview-file-user').attr('src', default_image);
    });
    
    $("#form-rent-truck").submit(function(e){
        $('#alert-form').html('');
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-renttruck') }}",
            type: "POST",
            data: new FormData($('#form-rent-truck')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-rent-truck").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
                $.each(err.errors, function (key, value) {
                    $('#alert-form').html('<div class="alert alert-danger mb-2" role="alert">'+key+' : '+value+'</div>');
                });
            }
        });
    });
    
    function getData(id){
        $('#title-form-contact').html('Edit Data');
        $.ajax({
            url : "{{ url('get-renttruck') }}/"+id,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-rent-truck').modal('show');
                    $('#id_rent_truck').val(result.data.id);
                    $('#perusahaan').val(result.data.perusahaan);
                    $('#merek').val(result.data.merek);
                    $('#no_polisi').val(result.data.no_polisi);
                    $('#tahun').val(result.data.tahun);
                    if(result.data.foto != '-'){
                        $('#preview-file-user').attr('src', 'images/'+result.data.foto);
                    }else{
                        $('#preview-file-user').attr('src', default_image);
                    }
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-renttruck') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    
</script>
@endsection
