@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12">
                        <h3 class="content-header-title">MASTER LOKASI</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="float-md-right">
                            <button type="button" id="button-tambah-location" name="button-tambah-location" class="btn btn-info round box-shadow-2 px-2" data-toggle="modal" data-target="#modal-location"><i class="ft-plus icon-left"></i> Tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-warning ml-1 mb-1" id="buttonEdit"><i class="ft-edit"></i> Edit</button>
                            <button class="btn btn-danger ml-1 mb-1" id="buttonDelete"><i class="ft-close"></i> Delete</button>
                            <table class="table table-striped table-bordered selection-deletion-row display nowrap table-xs" id="table-location" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>No.</th>
                                        <th>Nama</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-location" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-location" enctype="multipart/form-data" validate>
                <div class="modal-header bg-success white">
                    <h4 class="modal-title white" id="title-form-location">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert-form"></div>
                            <div class="form-group">
                                <h5>Nama Lokasi <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="id_lokasi" id="id_lokasi" hidden>
                                    <input type="text" name="nama_lokasi" id="nama_lokasi" class="form-control" required data-validation-required-message="This field is required" placeholder="Surabaya">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    
    var table_location;
    var default_image = 'app-assets/images/gallery/1.jpg'; 
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    function load_list_data(){
        table_location = $('#table-location').DataTable({
            processing: true,
            serverSide: true,
            ordering: false, 
            ajax: {
                url: "{{ url('datatable-location') }}",
                headers: {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    setTimeout(function(){ location.reload(); }, 1000);
                }
            },
            columnDefs: [{ 
                targets: [ 0 ],
                visible: false
            }],
        });

        // $('#table-location tbody').on( 'dblclick', 'tr', function () {
        //     $('#table-location tbody').removeClass('detail');
        //     $(this).addClass('detail');
        //     if(table_location.rows('.detail').data().length > 0){
        //         getData(table_location.row('.detail').data()[0]);
        //     }
            
        // });
        
        $('#table-location tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_location.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });        
    }
    
    $('#buttonEdit').click( function () {
        if(table_location.rows('.selected').data().length > 0){
            getData(table_location.row('.selected').data()[0]);
        }
    });

    $('#buttonDelete').click( function () {
        if(table_location.rows('.selected').data().length > 0){
            deleteData(table_location.row('.selected').data()[0]);
        }
    });
    
    function reload_list_data(){
        table_location.ajax.reload(null, false);
    }
    
    $('#button-tambah-location').click(function () {
        $("#form-location")[0].reset();
        $('#preview-file-user').attr('src', default_image);
    });
    
    $("#form-location").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-location') }}",
            type: "POST",
            data: new FormData($('#form-location')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-location").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
            }
        });
        
        
    });
    
    function getData(id){
        $('#title-form-location').html('Edit Data');
        $.ajax({
            url : "{{ url('get-location') }}/"+id,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-location').modal('show');
                    $('#id_lokasi').val(result.data.id);
                    $('#nama_lokasi').val(result.data.name);
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-location') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    
</script>
@endsection
