@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12">
                        <h3 class="content-header-title">MASTER MAPPING LOKASI</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="float-md-right">
                            <button type="button" id="button-tambah-location" name="button-tambah-location" class="btn btn-info round box-shadow-2 px-2" data-toggle="modal" data-target="#modal-location-price"><i class="ft-plus icon-left"></i> Tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-warning ml-1 mb-1" id="buttonEdit"><i class="ft-edit"></i> Detail</button>
                            <button class="btn btn-danger ml-1 mb-1" id="buttonDelete"><i class="ft-close"></i> Delete</button>
                            <table class="table table-striped table-bordered selection-deletion-row display nowrap table-xs" id="table-location-price" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID1</th>
                                        <th>ID2</th>
                                        <th>No.</th>
                                        <th>Asal</th>
                                        <th>Tujuan</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-location-price" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-location-price" enctype="multipart/form-data" validate>
                <div class="modal-header bg-success white">
                    <h4 class="modal-title white" id="title-form-location-price">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert-form"></div>
                            <div class="form-group">
                                <h5>Asal <span class="red">*</span></h5>
                                <div class="controls">
                                    <select name="location_from_id" id="location_from_id" class="form-control select2" style="width: 100%;" required>
                                        <option value="">- Pilih Wilayah -</option>
                                        @foreach($location as $l)
                                        <option value="{{ $l->id }}">{{ $l->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Tujuan <span class="red">*</span></h5>
                                <div class="controls">
                                    <select name="location_to_id" id="location_to_id" class="form-control select2" style="width: 100%;" required>
                                        <option value="">- Pilih Wilayah -</option>
                                        @foreach($location as $l)
                                        <option value="{{ $l->id }}">{{ $l->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Berat <span class="red">*</span></h5>
                                <div class="controls">
                                    <div class="row">
                                        <div class="col-5">
                                            <input type="text" name="parameter_location" id="parameter_location" class="form-control" required data-validation-required-message="This field is required" placeholder=">" value=">">
                                        </div>
                                        <div class="col-7">
                                            <input type="text" name="weight_location" id="weight_location" class="form-control" required data-validation-required-message="This field is required" placeholder="0" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Harga <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="price_location" id="price_location" class="form-control" required data-validation-required-message="This field is required" placeholder="0" value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal-location-price-detail" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success white">
                <h4 class="modal-title white">Detail Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-location-price-detail" enctype="multipart/form-data" validate>
                    <div class="row mb-1">
                        <div class="col-sm-6">
                            <fieldset class="form-group form-group-style mb-0">
                                <label for="productbalancequantity">ASAL</label>
                                <input type="text" name="location_from_id_detail" id="location_from_id_detail" hidden>
                                <input type="text" class="form-control" id="location_from_text" value="SURABAYA" disabled>
                            </fieldset>
                        </div>
                        <div class="col-sm-6">
                            <fieldset class="form-group form-group-style mb-0">
                                <label for="productbalanceaverageprice">TUJUAN</label>
                                <input type="text" name="location_to_id_detail" id="location_to_id_detail" hidden>
                                <input type="text" class="form-control" id="location_to_text" value="SURABAYA" disabled>
                            </fieldset>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <h5>Berat <span class="red">*</span></h5>
                                <div class="controls">
                                    <div class="row">
                                        <div class="col-3">
                                            <input type="text" name="parameter_location_detail" id="parameter_location_detail" class="form-control" required data-validation-required-message="This field is required" placeholder=">" value=">">
                                        </div>
                                        <div class="col-9">
                                            <input type="text" name="weight_location_detail" id="weight_location_detail" class="form-control" required data-validation-required-message="This field is required" placeholder="0" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Harga <span class="red">*</span></h5>
                                <div class="controls">
                                    <div class="row">
                                        <div class="col-9">
                                            <input type="text" name="price_location_detail" id="price_location_detail" class="form-control" required data-validation-required-message="This field is required" placeholder="0" value="0">
                                        </div>
                                        <div class="col-3">
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <hr/>
                </form>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-striped table-bordered selection-deletion-row display nowrap table-xs" id="table-location-price-detail" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Parameter</th>
                                    <th>Berat</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody id="tempel-detail-location">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
        
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    
    var table_location_price;
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    function load_list_data(){
        table_location_price = $('#table-location-price').DataTable({
            processing: true,
            serverSide: true,
            ordering: false, 
            ajax: {
                url: "{{ url('datatable-location-price') }}",
                headers: {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // setTimeout(function(){ location.reload(); }, 1000);
                }
            },
            columnDefs: [{ 
                targets: [ 0 ],
                visible: false
            },{ 
                targets: [ 1 ],
                visible: false
            }],
        });
        
        
        $('#table-location-price tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_location_price.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });        
    }
    
    $('#buttonEdit').click( function () {
        if(table_location_price.rows('.selected').data().length > 0){
            getData(table_location_price.row('.selected').data()[0],table_location_price.row('.selected').data()[1]);
        }
    });
    
    $('#buttonDelete').click( function () {
        if(table_location_price.rows('.selected').data().length > 0){
            deleteData(table_location_price.row('.selected').data()[0],table_location_price.row('.selected').data()[1]);
        }
    });
    
    function reload_list_data(){
        table_location_price.ajax.reload(null, false);
    }
    
    $('#button-tambah-location').click(function () {
        $("#form-location-price")[0].reset();
        $('#location_from_id').val('').trigger('change');
        $('#location_to_id').val('').trigger('change');
        $('#preview-file-user').attr('src', default_image);
    });
    
    $("#form-location-price").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-location-price') }}",
            type: "POST",
            data: new FormData($('#form-location-price')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-location-price").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
            }
        });
    });
    
    $("#form-location-price-detail").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-location-price-detail') }}",
            type: "POST",
            data: new FormData($('#form-location-price-detail')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-location-price").modal('hide');
                reload_list_data();
                $.ajax("{{ url('datatable-location-price-detail') }}/"+table_location_price.row('.selected').data()[0]+'/'+table_location_price.row('.selected').data()[1],{
                    success: function (data, status, xhr) {
                        $('#tempel-detail-location').html(data);
                    }
                });
                
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
            }
        });
    });
    
    function getData(id1,id2){
        $('#title-form-location-price').html('Edit Data');
        $.ajax({
            url : "{{ url('get-location-price') }}/"+id1+'/'+id2,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-location-price-detail').modal('show');
                    $('#location_from_id_detail').val(result.data.location_from);
                    $('#location_to_id_detail').val(result.data.location_to);
                    $('#location_from_text').val(result.data.asal);
                    $('#location_to_text').val(result.data.tujuan);
                    $.ajax("{{ url('datatable-location-price-detail') }}/"+id1+'/'+id2,{
                        success: function (data, status, xhr) {
                            $('#tempel-detail-location').html(data);
                        }
                    });
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id1,id2){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-location-price') }}/"+id1+'/'+id2,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
</script>
@endsection
