<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bukti Pembayaran</title>
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
</head>
<style>
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        color: black;
    }
    body{
        font-size: 12px;
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        background-color: white;
        color: black;
    }
    table{
        width: 100%;
    }
    .table-bordered, .border{
        border: 1px solid black;
    }
    .table th, .table td {
        padding: 0.25rem;
    }
    .border .border-dark{
        color: black;
        border: 1px solid black;
    }
    footer {
        position: fixed; 
        bottom: -60px; 
        left: 20px; 
        right: 0px;
        height: 50px; 
        
        color: black;
        text-align: left;
    }
</style>
<body>
    <div class="ml-1 mr-1 p-1 border border-dark" style="color: black;border: 1px solid black;">
        <table>
            <tr>
                <td class="text-center" width="30%"><img src="{{public_path('images/'.$settings['2']['value'])}}" class="text-center" alt="company logo" style="width: 80%"></td>
                <td width="40%" class="text-left">
                    <h4 class="m-0 p-0">{{ $settings['1']['value'] }}</h4>
                    <p class="m-0 p-0">email : cvbintangalkacasejahtera@gmail.com</p>
                    <p class="m-0 p-0">Jl Dr Cipto Perumahan BTN Blok L/11</p>
                    <p class="m-0 p-0">Kab Sumenep - Provinsi Jawa timur</p>
                </td>
                <td class="text-center" width="20%" style="vertical-align: top;">
                    <table>
                        <tr>
                            <td class="right" style="text-align: center; font-weight: bold; vertical-align: top;" width="100%">
                                <table class="m-0 p-0">
                                    <tr>
                                        <td width="1%">Tgl</td>
                                        <td width="1%">:</td>
                                        <td>{{ date('d-M-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">Alamat</td>
                                        <td style="vertical-align: top;" width="1%">:</td>
                                        <td>{!! nl2br(e($pengiriman->contact->alamat)) !!}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        <hr/>
        <table>
            <tr style="text-align: center">
                <td class="text-center">
                    <h4 class="m-0"><u>BUKTI PEMBAYARAN TAGIHAN</u></h4>
                    <h5 class="m-0 p-0">{{ $pengiriman->no_transaksi }}</h5>
                </td>
            </tr>
        </table>
        <table class="mt-1">
            <tr>
                <td style="text-align: left; " width="100%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="19%"><b>Kepada</b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $pengiriman->contact->nama }} / {{ $pengiriman->contact->perusahaan }}</td>
                        </tr>
                        <tr>
                            <td><b>Rute</b></td>
                            <td width="1%">:</td>
                            <td>{{ $pengiriman->rute }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        @php
        $no = 1;
        @endphp
        <table class="table mt-1 mb-2">
            <thead>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='1%' rowspan="2">No</th>
                    <th class="border text-center" width='34%' rowspan="2">Keterangan</th>
                    <th class="border text-center" width='28%' colspan="2">Volume</th>
                    <th class="border text-center" width='17%' rowspan="2">Harga</th>
                    <th class="border text-center" width='20%' rowspan="2">Total</th>
                </tr>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='14%'>Tonase</th>
                    <th class="border text-center" width='14%'>Karung</th>
                </tr>
            </thead>
            <tbody>
                @php
                $no = 0;
                $total = 0;
                @endphp
                @foreach ($barang as $b)
                <tr class="border">
                    <td class="border" style="text-align: center;">{{ ++$no }}</td>
                    <td class="border" style="text-align: left;">{{ $b->produk }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->tonase,3,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->karung,0,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->harga,0,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->total,0,',','.') }}</td>
                </tr>
                @php
                $total += $b->total;
                @endphp
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5" class="border" style="text-align: right; padding-right: 50px;">TOTAL</td>
                    <td class="border" style="text-align: right;">{{ number_format($total,0,',','.') }}</td>
                </tr>
                <tr>
                    <td colspan="6" style="padding-right: 0px;" class="pt-1">
                        Terbilang : <b>{{ $terbilang }} rupiah</b><br/><br/>
                        Telah di lakukan Pembayaran sebagai berikut:<br/>
                        No. Invoice atau Bill <b>{{ $pengiriman->no_bill }}</b> Tanggal <b>{{ date('d/m/Y',strtotime($pengiriman->tgl_bill)) }}</b><br/>
                        No. Rekening <b>{{ $pengiriman->no_rekening_bill }} @if (substr(strtoupper($pengiriman->nama_bank),0,4) != 'BANK') Bank @endif {{ $pengiriman->nama_bank }}</b><br/>
                        Atas Nama <b>{{ $pengiriman->atas_nama }}</b> dari <b>{{ $settings['1']['value'] }}</b> sesuai dengan nomor tagihan invoice tersebut.<br/>
                    </td>
                </tr>
            </tfoot>
        </table>
        <table class="text-center mt-1" >
            <tr>
                <td width="30%"><b>Yang Menerima</b></td>
                <td width="5"></td>
                <td width="30%"></td>
                <td width="5"></td>
                <td width="30%"><b>Yang Mengetahui</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td height="50px"></td>
                </tr>
                <tr>
                    <td>(.......................................)</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>( {{ $pengiriman->user }} )</td>
                </tr>
            </table>
        </div>
        
    </div>
    <p style="font-size: 10px; margin-left: 15px; "><i><b>Dokumen ini sah, diterbitkan oleh {{ $settings['1']['value'] }} secara elektronik melalui sistem dan tidak membutuhkan cap dan tandatangan basah.</b></i></p>             
    <p style="font-size: 8px; margin-left: 15px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p>
    {{-- <footer> --}}
        {{-- <p class="m-0 p-0" style="font-size: 8px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p> --}}
        {{-- </footer> --}}
    </body>
    </html>