<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
</head>
<style>
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        color: black;
    }
    body{
        font-size: 12px;
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        background-color: white;
        color: black;
    }
    table{
        width: 100%;
    }
    .table-bordered, .border{
        border: 1px solid black;
    }
    .table th, .table td {
        padding: 0.25rem;
    }
    .border .border-dark{
        color: black;
        border: 1px solid black;
    }
    footer {
        position: fixed; 
        bottom: -60px; 
        left: 20px; 
        right: 0px;
        height: 50px; 
        
        color: black;
        text-align: left;
    }
</style>
<body>
    <div class="ml-1 mr-1 p-1 border border-dark" style="color: black;border: 1px solid black;">
        <table>
            <tr>
                <td class="text-center" width="30%"><img src="{{ public_path('images/'.$settings['2']['value']) }}" class="text-center" alt="company logo" style="width: 80%"></td>
                {{-- <td class="text-center" width="30%"><img src="images/{{ $settings['2']['value'] }}" class="text-center" alt="company logo" style="width: 80%"></td> --}}
                <td width="60%" class="text-left">
                    <h4 class="m-0 p-0">{{ $settings['1']['value'] }}</h4>
                    <p class="m-0 p-0">email : cvbintangalkacasejahtera@gmail.com</p>
                    <p class="m-0 p-0">Jl Dr Cipto Perumahan BTN Blok L/11</p>
                    <p class="m-0 p-0">Kab Sumenep - Provinsi Jawa timur</p>
                </td>
                <td class="text-center" width="10%"></td>
            </tr>
        </table>
        <hr/>
        <table>
            <tr style="text-align: center">
                <td class="text-center">
                    <h4 class="m-0"><u>INVOICE</u></h4>
                    <h5 class="m-0 p-0">{{ $pengiriman->no_transaksi }}</h5>
                </td>
            </tr>
        </table>
        <table class="mt-1">
            <tr>
                <td style="text-align: left" width="45%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="33%">Kepada Yth</td>
                        </tr>
                        <tr>
                            <td width="33%"><b>Bpk. {{ $pengiriman->contact->nama }}</b></td>
                        </tr>
                        <tr>
                            <td width="33%">{!! nl2br(e($pengiriman->detail_location_from)) !!}</td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: left" width="30%">
                </td>
                <td class="right" style="text-align: right; vertical-align: top; font-weight: bold" width="25%">
                    Tanggal : {{ date('d F Y',strtotime($pengiriman->tgl_transaksi)) }}
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="text-align: left;" width="65%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="19%"><b>No. PO</b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $pengiriman->no_referensi }}</td>
                        </tr>
                        @if ($pengiriman)
                        <tr>
                            <td width="19%"><b>No. DO</b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $pengiriman->no_transaksi }}</td>
                        </tr>
                        @endif
                    </table>
                </td>
                <td class="right" style="text-align: right; vertical-align: top;" width="35%">
                    <table class="m-0 p-0">
                        <tr>
                            <td><b>Tanggal PO</b></td>
                            <td width="1%">:</td>
                            <td style="text-align: right">{{ date('d F Y',strtotime($pengiriman->tgl_po)) }}</td>
                        </tr>
                        @if ($pengiriman)
                        <tr>
                            <td><b>Tanggal DO</b></td>
                            <td width="1%">:</td>
                            <td style="text-align: right">{{ date('d F Y',strtotime($pengiriman->tgl_transaksi)) }}</td>
                        </tr>
                        @endif
                    </table>
                </td>
            </tr>
        </table>        
        @php
        $no = 1;
        @endphp
        <table class="table mb-0 mt-2">
            <thead>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='1%' rowspan="2">No</th>
                    <th class="border text-center" width='34%' rowspan="2">Keterangan</th>
                    <th class="border text-center" width='28%' colspan="2">Volume</th>
                    <th class="border text-center" width='17%' rowspan="2">Harga</th>
                    <th class="border text-center" width='20%' rowspan="2">Total</th>
                </tr>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='14%'>Tonase</th>
                    <th class="border text-center" width='14%'>Karung</th>
                </tr>
            </thead>
            <tbody>
                @php
                $no = 0;
                $total = 0;
                @endphp
                @foreach ($barang as $b)
                <tr class="border">
                    <td class="border" style="text-align: center;">{{ ++$no }}</td>
                    <td class="border" style="text-align: left;">{{ $b->produk }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->tonase, floor($b->tonase +0)?3:0 ,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->karung, floor($b->karung +0)?0:0 ,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->harga,0 ,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($b->total,0 ,',','.') }}</td>
                </tr>
                @php
                $total += $b->total;
                @endphp
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" rowspan="4" style="padding-right: 20px;">
                        No Rekening : <b>{{ $settings['4']['value'] }}</b><br/>
                        Nama Bank   : <b>{{ $settings['5']['value'] }}</b><br/>
                        Atas Nama   : <b>{{ $settings['6']['value'] }}</b><br/>
                        Senilai     : <b>{{ $terbilang }} rupiah</b>
                        <br/>
                    </td>
                    
                    <td style="text-align: left; border-style: none">Total</td>
                    <td class="border" style="text-align: right;">{{ number_format($total,0,',','.') }}</td>
                </tr>
                
                @if ($pengiriman->potongan > 0)
                <tr>
                    <td style="text-align: left; border-style: none">Potongan</td>
                    <td class="border" style="text-align: right;">{{ number_format($pengiriman->potongan*$total/100,0,',','.') }}</td>
                </tr>
                @endif
                @if ($pengiriman->pajak > 0)
                <tr>
                    <td style="text-align: left; border-style: none">{{ ($pengiriman->tipe_pajak?$pengiriman->tipe_pajak:'Pajak') }} ({{ $pengiriman->pajak +0 }}%)</td>
                    <td class="border" style="text-align: right;">{{ number_format($pengiriman->pajak*$total/100,0,',','.') }}</td>
                </tr>
                @endif
                <tr>
                    <td style="text-align: left; border-style: none"><b>Total Transaksi</b></td>
                    <td class="border" style="text-align: right;"><b>{{ number_format(($total-($pengiriman->potongan*$total/100)-($pengiriman->pajak*$total/100)),0,',','.') }}</b></td>
                </tr>
            </tfoot>
        </table>
        
        <table class="text-center mt-3" >
            <tr>
                <td width="30%"><b>Yang Menerima</b></td>
                <td width="5"></td>
                <td width="30%"></td>
                <td width="5"></td>
                <td width="30%"><b>Yang Mengetahui</b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td height="50px"></td>
            </tr>
            <tr>
                <td>(.......................................)</td>
                <td></td>
                <td></td>
                <td></td>
                <td> {{ $pengiriman->user }} </td>
            </tr>
        </table>
        
    </div>
    <p style="font-size: 10px; margin-left: 15px; "><i><b>Dokumen ini sah, diterbitkan oleh {{ $settings['1']['value'] }} secara elektronik melalui sistem dan tidak membutuhkan cap dan tandatangan basah.</b></i></p>             
    <p style="font-size: 8px; margin-left: 15px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p>
    {{-- <footer>
        <p class="m-0 p-0" style="font-size: 8px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p>
    </footer> --}}
</body>
</html>