@extends('layouts.app')
@include('pengiriman.pengiriman')
@section('content')
<div class="content-wrapper pt-1">
    <div class="content-header row pr-2">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">DETAIL PENGIRIMAN</h3>
            <b>{{ $pengiriman->no_parent?$pengiriman->no_parent:$pengiriman->no_transaksi }}</b>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-left">
                <a href="{{ url('pengiriman') }}" type="button" class="btn btn-info round box-shadow-2 px-2 mt-1"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
            @if ($pengiriman->status == '0')
            <div class="float-md-right">
                <button type="button" class="btn btn-danger round box-shadow-2 px-2 mt-1" onclick="pengiriman_delete()">Hapus <i class="ft-trash-2 icon-left"></i></button>
            </div>
            @endif
        </div>
    </div>
    <div class="content-body">
        <section id="lists">
            @yield('pengiriman')
        </section>
    </div>
    @if ($pengiriman->status == '0')
    <div class="content-footer row pr-2">
        <div class="col-12 text-right">
            <button type="button" id="button-selesai" class="btn btn-success round box-shadow-2 px-2 m-0" onclick="pengiriman_selesai()">Selesai <i class="ft-check icon-left"></i></button>
            <div class="loader-wrapper" id="loader-selesai">
                <div class="loader-container">
                    <div class="ball-pulse loader-success">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<script>
    function pengiriman_delete() {
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "btn-danger",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                location.replace("{{ url('pengiriman-delete') }}/{{ $pengiriman->id }}");
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    function pengiriman_selesai() {
        swal({
            title: "Are you sure?",
            text: "Pengiriman telah selesai!",
            icon: "info",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Belum!",
                    value: null,
                    visible: true,
                    className: "btn-default",
                    closeModal: false,
                },
                confirm: {
                    text: "Selesai",
                    value: true,
                    visible: true,
                    className: "btn-success",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $('#button-selesai').hide();
                $('#loader-selesai').show();
                $('#button-pembayaran-pengiriman').click();
                location.replace("{{ url('pengiriman-confirm') }}/{{ $pengiriman->id }}");
            } else {
                $('#button-selesai').show();
                $('#loader-selesai').hide();
                swal("Batal!", "Pengiriman belum diselesaikan!", "warning");
            } 
        });
    }
        
</script>
@endsection