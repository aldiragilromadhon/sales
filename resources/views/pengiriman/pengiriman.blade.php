@section('pengiriman')
<div class="card mb-1">
    <div class="card-body">
        <div class="row">
            <div class="col-md-4">
                <div id="basic-list">
                    <ul class="list-group list">
                        <li class="list-group-item">
                            <h3 class="name font-medium-1 text-bold-600" style="margin-bottom: 0px;">NO/TGL PURCHASE ORDER :</h3>
                            <p class="mb-0">{{ $pengiriman->no_referensi }} | {{ date('d F Y',strtotime($pengiriman->tgl_po)) }}</p>
                        </li>
                        <li class="list-group-item">
                            <h3 class="name font-medium-1 text-bold-600" style="margin-bottom: 0px;">PERUSAHAAN :</h3>
                            <p class="mb-0">{{ $pengiriman->contact->perusahaan }} | {{ $pengiriman->contact->nama }}<br/>{!! nl2br(e($pengiriman->detail_location_from)) !!}</p>
                        </li>
                        <li class="list-group-item">
                            <h3 class="name font-medium-1 text-bold-600" style="margin-bottom: 0px;">ALAMAT PENERIMA :</h3>
                            <p class="mb-0">{!! nl2br(e($pengiriman->detail_location_to)) !!}</p>
                        </li>
                        <li class="list-group-item">
                            <h3 class="name font-medium-1 text-bold-600" style="margin-bottom: 0px;">TANGGAL :</h3>
                            <p class="mb-0">{{ date('d F Y',strtotime($pengiriman->tgl_transaksi)) }}</p>
                        </li>
                        <li class="list-group-item">
                            <h3 class="name font-medium-1 text-bold-600" style="margin-bottom: 0px;">INFO BANK :</h3>
                            <p class="mb-0">{{ $pengiriman->no_rekening_bill }} | {{ $pengiriman->atas_nama }}</p>
                            <p class="mb-0">
                                @if (substr(strtoupper($pengiriman->nama_bank),0,4) != 'BANK')
                                Bank
                                @endif
                                {{ $pengiriman->nama_bank }}
                            </p>
                        </li>
                        <li class="list-group-item">
                            <form class="form form-horizontal" method="POST" validate id="form-pembayaran-pengiriman">
                                @csrf
                                <h3 class="name font-medium-1 text-bold-600" style="margin-bottom: 0px;">PEMBAYARAN :</h3>
                                <input type="text" id="idpengirimannobill" name="idpengirimannobill" value="{{ $pengiriman->id }}" hidden>
                                <input type="text" class="form-control" id="pengirimannobill" name="pengirimannobill" required="" style="background-color: white;" placeholder="No. Invoice atau Bill" value="{{ $pengiriman->no_bill }}">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="pengirimantanggalbill" name="pengirimantanggalbill" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-success" id="button-pembayaran-pengiriman">Save</button>
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <form class="form-bordered">
                    <h5 class="form-section mb-0"><i class="ft-box"></i> LIST BARANG</h5>
                </form>
                <div class="table-responsive">
                    <table class="table table-xs">
                        <thead>
                            <tr>
                                <th class="sort text-center">No</th>
                                <th class="sort text-center">Keterangan</th>
                                <th class="sort text-center">Tonase</th>
                                <th class="sort text-center">Karung</th>
                                @if ($pengiriman->no_parent)
                                <th class="sort text-center">Harga</th>
                                <th class="sort text-center">Total</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody class="list">
                            @php
                            $no = 0;
                            @endphp
                            @if ($pengiriman->barang)
                            @foreach ($pengiriman->barang as $b)
                            <tr>
                                <td class="text-center">{{ ++$no }}</td>
                                <td class="text-left">{{ $b->produk }}</td>
                                <td class="text-right">{{ number_format($b->tonase,3,',','.') }}</td>
                                <td class="text-right">{{ number_format($b->karung,0,',','.') }}</td>
                                @if ($pengiriman->no_parent)
                                <td class="text-right">{{ number_format($b->harga,0,',','.') }}</td>
                                <td class="text-right">{{ number_format($b->total,0,',','.') }}</td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        @if ($pengiriman->no_parent)
                        <tfoot>
                            <tr>
                                <th colspan="5" class="text-right">Total</th>
                                <th class="text-right">{{ number_format(round($pengiriman->subtotal)) }}</th>
                            </tr>
                            <tr>
                                <th colspan="5" class="text-right">Potongan / Diskon</th>
                                <th class="text-right">{{ number_format(round($pengiriman->potongan*$pengiriman->subtotal/100),0,',','.') }}</th>
                            </tr>
                            <tr>
                                <th colspan="5" class="text-right">{{ ($pengiriman->tipe_pajak?$pengiriman->tipe_pajak:'Pajak') }} {{ $pengiriman->pajak +0 }}%</th>
                                <th class="text-right">{{ number_format(round($pengiriman->pajak*$pengiriman->subtotal/100),0,',','.') }}</th>
                            </tr>
                            <tr>
                                <th colspan="5" class="text-right"><b>Total Transaksi</b></th>
                                <th class="text-right">{{ number_format(round($pengiriman->total_transaksi),0,',','.') }}</th>
                            </tr>
                        </tfoot>
                        @endif
                    </table>
                </div>
                @if (!$pengiriman->no_parent)
                <form class="form-bordered">
                    <h5 class="form-section mb-0"><i class="ft-file-text"></i> LIST SURAT JALAN</h5>
                </form>
                <div class="table-responsive">
                    <table class="table table-xs">
                        <thead>
                            <tr>
                                <th class="sort text-center">Keterangan</th>
                                <th class="sort text-center">Tonase</th>
                                <th class="sort text-center">Karung</th>
                                <th class="sort text-center">Harga</th>
                                <th class="sort text-center">Total</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            @php
                            $total = 0;
                            @endphp
                            @if ($suratjalan->count()>0)
                            @foreach ($suratjalan as $sj)
                            <tr>
                                <td class="text-left">{{ $sj->produk }}</td>
                                <td class="text-right">{{ number_format($sj->tonase,3,',','.') }}</td>
                                <td class="text-right">{{ number_format($sj->karung,0, ',','.') }}</td>
                                <td class="text-right">{{ number_format($sj->harga,0,',','.') }}</td>
                                <td class="text-right">{{ number_format($sj->total,0,',','.') }}</td>
                            </tr>
                            @php
                            $total += $sj->total;
                            @endphp
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="text-right">Total</th>
                                <th class="text-right">{{ number_format($total,0,',','.') }}</th>
                            </tr>
                            <tr>
                                <th colspan="4" class="text-right">Potongan / Diskon</th>
                                <th class="text-right">{{ number_format($pengiriman->potongan*$total/100,0,',','.') }}</th>
                            </tr>
                            <tr>
                                <th colspan="4" class="text-right">{{ ($pengiriman->tipe_pajak?$pengiriman->tipe_pajak:'Pajak') }} ({{ $pengiriman->pajak +0 }}%)</th>
                                <th class="text-right">{{ number_format($pengiriman->pajak*$total/100,0,',','.') }}</th>
                            </tr>
                            <tr>
                                <th colspan="4" class="text-right"><b>Total Transaksi</b></th>
                                <th class="text-right">{{ number_format(($total-($pengiriman->potongan*$total/100)-($pengiriman->pajak*$total/100)),0,',','.') }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @endif
                
            </div>
        </div>
    </div>
</div>
<div class="card mb-1">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                @if ($pengiriman->status == '0')
                <button class="btn btn-danger box-shadow-2 pull-right btn-min-width btn-sm ml-2" id="buttonPrintDelete"><i class="ft-trash"></i> Delete</button>
                @endif
                <button class="btn btn-success box-shadow-2 pull-right btn-min-width btn-sm ml-2" id="buttonPrintRekap"><i class="ft-printer"></i> Rekap</button>
                <button class="btn btn-success box-shadow-2 pull-right btn-min-width btn-sm ml-2" id="buttonPrintSJ"><i class="ft-printer"></i> Surat Jalan</button>
                @if ($pengiriman->status == '0')
                <button type="button" class="btn btn-primary box-shadow-2 pull-right btn-min-width btn-sm" id="button-add-row-product-pengiriman" data-toggle="modal" data-target="#modal-surat-jalan-pengiriman"><i class="ft-plus"></i> Tambah</button>
                @endif
                <form class="form-bordered">
                    <h4 class="form-section"><i class="ft-file-text"></i> SURAT JALAN</h4>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered table-xs selection-deletion-row" id="table-surat-jalan" width="100%">
                        <thead>
                            <tr>
                                <th class="sort text-center">ID</th>
                                <th class="sort text-center">No DO</th>
                                <th class="sort text-center">Tanggal</th>
                                <th class="sort text-center">Truck/Kontainer</th>
                                <th class="sort text-center">Tonase</th>
                                <th class="sort text-center">Karung</th>
                            </tr>
                        </thead>
                        <tbody class="list" id="tempel-list-surat-jalan">
                        </tbody>
                        <tfoot id="tempel-total-surat-jalan">
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-surat-jalan-pengiriman" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-surat-jalan-pengiriman" enctype="multipart/form-data" validate>
                <div class="modal-header bg-info">
                    <h4 class="modal-title white" id="title-modal-surat-jalan">Buat Surat Jalan</h4>
                </div>
                <div class="modal-body">
                    <div id="alert-form"></div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card box-shadow-2" style="border: 1px solid #e2e2e2;">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="no_do_surat_jalan">No Surat Jalan</label>
                                            <input type="text" class="form-control" id="id_surat_jalan" name="id_surat_jalan" value="" hidden>
                                            <input type="text" class="form-control" id="no_pengiriman_surat_jalan" name="no_pengiriman_surat_jalan" value="{{ $pengiriman->id }}" hidden>
                                            <input type="text" class="form-control" id="no_do_surat_jalan" name="no_do_surat_jalan" >
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal_surat_jalan"><strong class="text-uppercase">Tanggal</strong></label>
                                            <input type="text" class="form-control" id="tanggal_surat_jalan" name="tanggal_surat_jalan" value="{{ date('d-m-Y') }}" style="background-color: white;" >
                                        </div>
                                        <div class="form-group">
                                            <label for="tipe_surat_jalan"><strong class="text-uppercase">Tipe</strong></label>
                                            <select class="form-control" name="tipe_surat_jalan" id="tipe_surat_jalan">
                                                <option selected value="1">Truck</option>
                                                <option value="2">Kontainer</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="form-kontainer">
                                            <label for="kontainer_surat_jalan">Kontainer</label>
                                            <input type="text" class="form-control" id="kontainer_surat_jalan" name="kontainer_surat_jalan">
                                        </div>
                                        <div class="form-group" id="form-truck">
                                            <label for="plat_surat_jalan">Plat Nomor</label>
                                            <select class="form-control select2 select2-table check-text-isset" id="plat_surat_jalan" name="plat_surat_jalan" style="width: 100%;">
                                                <option value=""><i>- Pilih Truck -</i></option>
                                                @foreach($truck as $t)
                                                <option value="{{$t->id}}">{{$t->perusahaan}} | {{$t->no_polisi}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 table-responsive">
                            <table class="table table-striped table-bordered table-xs">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Keterangan</th>
                                        <th>Tonase</th>
                                        <th>Karung</th>
                                    </tr>
                                </thead>
                                <tbody id="tempel-detail-surat-jalan">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                </div>
            </form>
        </div>
        
    </div>
</div>

@endsection
@section('script')
<script>
    var table_data;
    
    $(document).ready(function(){
        $('#loader-selesai').hide();
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        $('#tanggal_surat_jalan, #pengirimantanggalbill').pickadate({
            format: 'dd-mm-yyyy'
        }).attr("tabIndex", "-1");
        if( /Android|iPhone|iPad|iPod|IEMobile/i.test(navigator.userAgent) ) {
            $('.select2').select2({
                dropdownAutoWidth : true,
                width: '100%'
            });
        }else{
            $('.select2').select2({
                dropdownAutoWidth : true,
            });
            setTimeout(function(){ $('#hidden-menu').click() }, 1000);
        }
        table_surat_jalan();
    });
    
    $('#tipe_surat_jalan').change( function () {
        if ($('#tipe_surat_jalan').val() == '1') {
            $('#form-kontainer').hide();
            $('#form-truck').show();
        }else{
            $('#form-kontainer').show();
            $('#form-truck').hide();
        }
        
    });
    
    function table_surat_jalan(){
        if ( $.fn.DataTable.isDataTable('#table-surat-jalan') ) {
            $('#table-surat-jalan').DataTable().destroy();
        }
        
        $.get("{{ url('datatable-surat-jalan') }}/"+"{{ $pengiriman->id }}", function(response, status, xhr){
            $('#tempel-list-surat-jalan').html(response.list);
            $('#tempel-total-surat-jalan').html(response.total);
            
            table_data = $('#table-surat-jalan').DataTable({
                "ordering": false,
                "paging": false,
                "info": false,
                "columnDefs": [{ 
                    "targets": [ 0 ],
                    "visible": false
                }]
            });
        });
    }
    
    $('#table-surat-jalan tbody').on( 'dblclick', 'tr', function () {
        $('#table-surat-jalan tbody').removeClass('detail');
        $(this).addClass('detail');
        get_detail();
    });
    
    $('.selection-deletion-row tbody').on('click', 'tr', function() {
        $('.selection-deletion-row tbody tr').removeClass('selected');
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            $(this).removeClass('selected');
            $(this).addClass('selected');
        }
    });
    
    function get_detail(){
        $('#title-modal-surat-jalan').html('Edit Surat Jalan');
        $.get("{{ url('pengiriman-barang-edit') }}/"+"{{ $pengiriman->id }}", function(response, status, xhr){
            $('#modal-surat-jalan-pengiriman').modal('show');
            $('#id_surat_jalan').val(response.id.id);
            $('#no_do_surat_jalan').val(response.id.no_transaksi);
            $('#tanggal_surat_jalan').val(response.id.tanggal);
            $('#plat_surat_jalan').val(response.id.truck_id).change();
            $('#tempel-detail-surat-jalan').html(response.data);
            $('#alert-form').html('');
        });
        $('#plat_surat_jalan').val('').change();
    }
    
    $('#buttonPrintSJ').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            window.open("{{ url('pengiriman-sj') }}/"+table_data.row('.selected').data()[0]);
        }else{
            swal('Pilih data surat jalan terlebih dahulu');
        }
    });
    
    $('#buttonPrintRekap').click( function () {
        window.open("{{ url('pengiriman-rekap') }}/"+"{{ $pengiriman->id }}");
    });
    
    $('#buttonPrintDelete').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            deleteData(table_data.row('.selected').data()[0]);
        }else{
            swal('Pilih data surat jalan terlebih dahulu');
        }
    });
    
    $('#button-add-row-product-pengiriman').click(function(){
        $('#title-modal-surat-jalan').html('Buat Surat Jalan');
        $('.selection-deletion-row tbody tr').removeClass('selected');
        $.get("{{ url('pengiriman-barang') }}/"+"{{ $pengiriman->id }}", function(response, status, xhr){
            $('#no_do_surat_jalan').val(response.id);
            $('#tipe_surat_jalan').val('1');
            $('#plat_surat_jalan').val('').change();
            $('#kontainer_surat_jalan').val('');
            $('#form-kontainer').hide();
            $('#form-truck').show();
            $('#tempel-detail-surat-jalan').html(response.data);
            $('#alert-form').html('');
        });
    });
    
    $("#form-surat-jalan-pengiriman").submit(function(e){
        $('#alert-form').html('');
        e.preventDefault();
        if ( ($('#plat_surat_jalan').val() && $('#tipe_surat_jalan').val() == '1') || ($('#kontainer_surat_jalan').val() && $('#tipe_surat_jalan').val() == '2') ) {
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                url: "{{ url('store-surat-jalan') }}",
                type: "POST",
                data: new FormData($('#form-surat-jalan-pengiriman')[0]),
                processData: false,
                contentType: false,
                dataType: "JSON",
                success: function(result) {
                    swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                    $("#modal-surat-jalan-pengiriman").modal('hide');
                    table_surat_jalan();
                },
                error: function (jqXHR, textStatus, errorThrown ){
                    var err = JSON.parse(jqXHR.responseText);
                    swal("INFO!", err.message, "warning");
                    $.each(err.errors, function (key, value) {
                        $('#alert-form').append('<div class="alert alert-danger mb-2" role="alert">'+value+'</div>');
                    });
                    
                }
            });
        }else{
            swal("INFO!", 'Truck / Kontainer tidak boleh kosong', "warning");
        }
    });
    
    function getData(id){
        $('#title-form-location').html('Edit Data');
        $.ajax({
            url : "{{ url('get-location') }}/"+id,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-location').modal('show');
                    $('#id_lokasi').val(result.data.id);
                    $('#nama_lokasi').val(result.data.name);
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-surat-jalan') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        table_surat_jalan();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    $("#form-pembayaran-pengiriman").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-pengiriman-pembayaran') }}",
            type: "POST",
            processData: false,
            contentType: false,
            data: new FormData($('#form-pembayaran-pengiriman')[0]),
            success: function(result) {
                if(result.code == 200){
                    swal("Sukses!", "Data telah tersimpan!", "success");
                }else{
                    swal("Gagal!", "Data gagal disimpan!", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
        
    });
    
    
</script>
@endsection