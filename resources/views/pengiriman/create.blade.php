@extends('layouts.app')
@section('style')
<style type="text/css">
    .remove-td td {
        padding: 0.5rem 0.5rem;
    }
</style>
@endsection
@section('content')
<div class="content-wrapper pt-2">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mt-1 m-0">
                Buat Penagihan Pengiriman
            </h3>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-2">
            <div class="float-md-right">
                <a href="{{ url('pengiriman') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body ">
        @if (session('success'))
        <div class="alert alert-success mb-2" role="alert">
            {{ session('success') }}
        </div>
        @endif
        
        @if (session('error'))
        <div class="alert alert-warning mb-2" role="alert">
            {{ session('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div id="alert-form-pengiriman"></div>
                <div class="card">
                    <div class="card-content">
                        <form class="form form-horizontal" method="POST" validate id="form-pengiriman">
                            @csrf
                            <div class="card-body p-1" style="background-color: #B1DCF7">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group mb-0">
                                            <h5>No Penjualan <span class="red">*</span></h5>
                                            <div class="controls">
                                                <select class="form-control select2" id="pengirimanparent" name="pengirimanparent" required style="width: 100%">
                                                    <option value="">Pilih Penjualan</option>
                                                    @foreach($penjualan as $penj)
                                                    <option value="{{ $penj->no_transaksi }}">{{ $penj->no_transaksi }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <h5 hidden>No Referensi / No PO</h5>
                                        <input type="text" class="form-control" id="pengirimannomorreferensi" name="pengirimannomorreferensi" placeholder="" hidden>
                                    </div>
                                    <div class="col-lg-6 text-right center pt-2 pr-2">
                                        <p class="font-medium-3 text-bold-600" id="showpengirimanatas">Total Rp. 0</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-1">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label><b>Perusahaan Ekspedisi <span class="red">*</span></b></label>
                                                <select name="pengirimancustomer" id="pengirimancustomer" class="form-control js-example-events select2" required style="width: 100%;">
                                                    <option value="">Pilih Kontak</option>
                                                    @foreach($contact as $data)
                                                    <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Tgl Purchase Order</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="pengirimantanggalpo" name="pengirimantanggalpo" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Tgl Pengiriman</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="pengirimantanggal" name="pengirimantanggal" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Mengetahui</label>
                                                <input type="text" class="form-control" id="pengirimanuser" name="pengirimanuser" required style="background-color: white;">
                                            </div>
                                                <div class="form-group">
                                                <label>Dikirim dari <span class="red">*</span></label>
                                                <textarea class="form-control" id="pengirimanalamatpengiriman" name="pengirimanalamatpengiriman" required rows="3"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Diterima di <span class="red">*</span></label>
                                                <textarea class="form-control" id="pengirimanalamatpenerimaan" name="pengirimanalamatpenerimaan" required rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Rute Tujuan <span class="red">*</span></label>
                                                <input type="text" class="form-control" id="pengirimanrute" name="pengirimanrute" required style="background-color: white;" placeholder="Sumenep - Surabaya">
                                            </div>
                                                <div class="form-group">
                                                <label>No Rek Ekspedisi</label>
                                                <input type="text" class="form-control" id="pengirimanrekening" name="pengirimanrekening" required style="background-color: white;" placeholder="No Rekening">
                                            </div>
                                            <div class="form-group">
                                                <label>A/n Rekening</label>
                                                <input type="text" class="form-control" id="pengirimanatasnama" name="pengirimanatasnama" required style="background-color: white;" placeholder="Atas Nama Rekening">
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Bank & Cabang</label>
                                                <input type="text" class="form-control" id="pengirimanbank" name="pengirimanbank" required style="background-color: white;" placeholder="Mandiri Cabang Surabaya">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3"  id="detail-contact">
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover remove-td " id="table-product-pengiriman">
                                                <thead>
                                                    <tr>
                                                        <th width="30%">Produk</th>
                                                        <th width="15%">Tonase</th>
                                                        <th width="15%">Karung</th>
                                                        <th width="18%">Harga</th>
                                                        <th width="21%">Total</th>
                                                        <th width="1%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="button" class="btn btn-info btn-min-width box-shadow-2 mt-1" id="button-add-row-product-pengiriman"><i class="la la-plus-square"></i> Tambah Data</button>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="table-responsive">
                                            <table class="table table-borderless mb-0 p-0 m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td class="type-info text-right">
                                                            <input type="text" id="pengirimansubtotal" name="pengirimansubtotal" value="0" hidden>
                                                            <p id="showpengirimansubtotal" class="m-0">Rp. 0</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><p style="margin-bottom: 5px">Pemotongan / Diskon</p>
                                                            <div class="row">
                                                                <div class="col-7">
                                                                    <div class="input-group input-group-sm" style="min-width: 10px;">
                                                                        <input type="text" class="form-control input-sm" id="pengirimanpemotongan" name="pengirimanpemotongan" placeholder="" value="0">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">%</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="type-info text-right pt-2" id="showpengirimanpemotongan">Rp. 0</td>
                                                    </tr>
                                                    <tr>
                                                        <td><p style="margin-bottom: 5px">Pajak</p>
                                                            <div class="row">
                                                                <div class="col-7">
                                                                    <select class="form-control input-sm" id="pengirimantipepajak" name="pengirimantipepajak">
                                                                        <option value="">- Pilih -</option>
                                                                        <option value="PPN">PPN</option>
                                                                        <option value="PPh 21">PPh 21</option>
                                                                        <option value="PPh 23">PPh 23</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-7">
                                                                    <div class="input-group input-group-sm">
                                                                        <input type="text" class="form-control input-sm" id="pengirimanpajak" name="pengirimanpajak" placeholder="" value="0" disabled>
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">%</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="type-info text-right pt-2" id="showpengirimanpajak">Rp. 0</td>
                                                    </tr>
                                                    <tr>
                                                        <td><h2 class="m-0">Total tagihan</h2>
                                                            <input type="text" id="pengirimantotal" name="pengirimantotal" value="0" hidden>
                                                        </td>
                                                        <td class="type-info text-right"><h2 id="totaltagihanpengiriman" class="m-0">Rp. 0</h2></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <button type="submit" class="btn btn-success" >Buat Pengiriman</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    var rowIdx = 0, sumdiskon = 0, sumpajak = 0, sumtotal = 0;
    $(document).ready(function () {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        addRowsTablePengiriman();
        $('#pengirimantanggalpo, #pengirimantanggal, #pengirimanjatuhtempo').pickadate({
            format: 'dd-mm-yyyy',
            today: '',
            close: 'Close',
            clear: ''
        });
        if( /Android|iPhone|iPad|iPod|IEMobile/i.test(navigator.userAgent) ) {
            $('.select2').select2({
                dropdownAutoWidth : true,
                width: '100%'
            });
        }else{
            $('.select2').select2({
                dropdownAutoWidth : true,
            });
            setTimeout(function(){ $('#hidden-menu').click() }, 1000);
        }
    });
    
    $('#button-add-row-product-pengiriman').on('click', function () {
        addRowsTablePengiriman();
    });
    
    $('#table-product-pengiriman tbody').on('click', '.remove', function () {
        var row = $(this).closest('tr');
        row.remove();
        sumtotal = 0;
        $("#table-product-pengiriman tbody input[name='pengirimanproduktotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPengiriman);
        setGrandTotal();
    });
    
    $( "#pengirimanpajak, #pengirimanpemotongan" ).focus(function(e) {
        if ($(this).val() == 0) {
            $(this).val('');
        }
    });
    $( "#pengirimanpajak, #pengirimanpemotongan" ).focusout(function(e){
        if ($(this).val() == '') {
            $(this).val('0');
        }
    });
    
    $('#pengirimantipepajak').change(function(){
        if ($(this).val() == '') {
            $('#pengirimanpajak').val(0);
            $('#pengirimanpajak').prop('disabled', true);
        }else{
            $('#pengirimanpajak').prop('disabled', false);
        }
    });
    
    $('#pengirimanparent').on('change', function() {
        if ($(this).val()) {
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                url : "{{ url('pengiriman-parent') }}",
                data: {
                    'nomor': $(this).val()
                },
                type: "POST",
                success: function(result) {
                    $('#pengirimannomorreferensi').val(result.no_referensi);
                    $('#pengirimantanggalpo').val(result.tanggal);
                    // $('#pengirimancustomer').val(result.contact_id).change();
                    $('#pengirimanalamatpenerimaan').val((result.alamat_kirim));
                },
                error: function (jqXHR, textStatus, errorThrown ){
                    var err = JSON.parse(jqXHR.responseText);
                    swal("ERROR!", err.message, "error");
                }
            });
        }
    });
    
    $('#pengirimancustomer').on('change', function() {
        if ($(this).val()) {
            $.ajax("{{ url('get-address-contact') }}/"+$(this).val(),{
                success: function (data, status, xhr) {
                    // $('#pengirimanalamatpengiriman').val((data.alamat));
                    // $('#pengirimanalamatpenerimaan').val((data.alamat));
                    $('#detail-contact').html('<div class="bs-callout-info callout-bordered callout-transparent p-1 mt-2">'+
                            '<h6 class="black text-bold-600 font-medium-2">'+data.nama+'</h6>'+
                            '<h6 class="black font-small-3 mb-1">'+data.perusahaan+'</h6>'+
                            '<p>'+nl2br(data.alamat)+'</p>'+'<p>'+(data.email?data.email:'')+'</p>'+'<p>'+(data.phone?data.phone:'')+'</p></div>'
                        );
                }
            });
        }
    });
    
    function addRowsTablePengiriman() {
        $('#table-product-pengiriman tbody').append(`
        <tr>
            <td>
                <input type="text" class="form-control" id="pengirimanproduk" name="pengirimanproduk[]" value="" style="min-width: 170px;">
            </td>
            <td>
                <input type="text" class="form-control class-money-format class-quantity" id="pengirimantonase" name="pengirimantonase[]" value="0" style="min-width: 170px;">
            </td>
            <td>
                <input type="text" class="form-control class-quantity" id="pengirimankarung" name="pengirimankarung[]" value="0" style="min-width: 170px;">
            </td>
            <td>
                <div class="input-group" style="min-width: 170px;">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    <input type="text" class="form-control text-right class-money-format class-quantity" id="pengirimanprodukprice" name="pengirimanprodukprice[]" value="0">
                </div>
            </td>
            <td>
                <div class="input-group" style="min-width: 170px;">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Rp</span>
                    </div>
                    <input type="text" class="form-control text-right class-money-format-total class-quantity" id="pengirimanproduktotal" name="pengirimanproduktotal[]" value="0">
                </div>
            </td>
            <td><button type="button" class="btn btn-icon btn-pure danger remove"><i class="la la-close"></i></button></td>
        </tr>`);
        
        $('.class-money-format').on("keyup", function(e) {
            $(this).val(formatRupiah($(this).val()));
            var tonase = $(this).closest('tr').find('#pengirimantonase').val() == 0 ? [1,0] : $(this).closest('tr').find('#pengirimantonase').val().toString().replace(/[^,\d]/g, "").split(",");
            var price = $(this).closest('tr').find('#pengirimanprodukprice').val() == 0 ? [1,0] : $(this).closest('tr').find('#pengirimanprodukprice').val().toString().replace(/[^,\d]/g, "").split(",");
            var total = (parseFloat(price[0]+'.'+price[1]) *  parseFloat(tonase[0]+'.'+tonase[1])).toFixed(0).toString().split(".");
            $(this).closest('tr').find('#pengirimanproduktotal').val(formatRupiah(total[0]+','+total[1],''));
            sumtotal = 0;
            $("#table-product-pengiriman tbody input[name='pengirimanproduktotal[]']").map(function(){ return $(this).val(); }).get().forEach(setTotalPengiriman);
            setGrandTotal();
        });
        
        $('.class-money-format-total').on("keyup", function(e) {
            $(this).val(formatRupiah($(this).val()));
            var tonase = $(this).closest('tr').find('#pengirimantonase').val() == 0 ? [1,0] : $(this).closest('tr').find('#pengirimantonase').val().toString().replace(/[^,\d]/g, "").split(",");
            var price = $(this).closest('tr').find('#pengirimanproduktotal').val() == 0 ? [1,0] : $(this).closest('tr').find('#pengirimanproduktotal').val().toString().replace(/[^,\d]/g, "").split(",");
            var total = (parseFloat(price[0]+'.'+price[1]) /  parseFloat(tonase[0]+'.'+tonase[1])).toFixed(0).toString().split(".");
            $(this).closest('tr').find('#pengirimanprodukprice').val(formatRupiah(total[0]+','+total[1],''));
            sumtotal = 0;
            $("#table-product-pengiriman tbody input[name='pengirimanproduktotal[]']").map(function(){ return $(this).val(); }).get().forEach(setTotalPengiriman);
            setGrandTotal();
        });
        
        $('.class-quantity').focus(function(e) {
            if ($(this).val() == 0) {
                $(this).val('');
            }
        });
        $('.class-quantity').focusout(function(e){
            if ($(this).val() == '') {
                $(this).val('0');
            }
        });
    }
    
    $('#pengirimanpemotongan, #pengirimanpajak').on("keyup", function(e) {
        setGrandTotal();
    });
    
    function setTotalPengiriman(total) {
        temp_total = total.toString().replace(/[^,\d]/g, "").split(",");
        sumtotal += parseFloat(temp_total[0]+'.'+temp_total[1]);
        var temp_sum = sumtotal.toFixed(0).toString().split(".");
        $('#showpengirimanatas, #showpengirimansubtotal').html(formatRupiah(temp_sum[0]+','+temp_sum[1], 'Rp '));
        $('#pengirimansubtotal').val(sumtotal);
    }
    
    function setGrandTotal(){
        setPajakPemotong();
        var pemotong = $('#pengirimanpemotongan').val() == '' ? 0 : ($('#pengirimanpemotongan').val()*$('#pengirimansubtotal').val())/100;
        var pajak = $('#pengirimanpajak').val() == '' ? 0 : ($('#pengirimanpajak').val()*$('#pengirimansubtotal').val())/100;
        var grand_total_temp = $('#pengirimansubtotal').val() > parseFloat(pemotong)+parseFloat(pajak) ? parseFloat($('#pengirimansubtotal').val()-parseFloat(pemotong)-parseFloat(pajak)) : 0;
        var grand_total = grand_total_temp.toFixed(0).toString().split(".");
        $('#totaltagihanpengiriman').html(formatRupiah(grand_total[0]+','+grand_total[1], 'Rp '));
        $('#pengirimantotal').val(grand_total_temp);
        
    }
    
    function setPajakPemotong(){
        var tmp_pemotong = ($('#pengirimansubtotal').val()*$('#pengirimanpemotongan').val()/100).toFixed(0).toString().split(".");
        var tmp_pajak = ($('#pengirimansubtotal').val()*$('#pengirimanpajak').val()/100).toFixed(0).toString().split(".");
        $('#showpengirimanpemotongan').html(formatRupiah(tmp_pemotong[0]+','+tmp_pemotong[1], 'Rp '));
        $('#showpengirimanpajak').html(formatRupiah(tmp_pajak[0]+','+tmp_pajak[1], 'Rp '));
    }
    
    function formatRupiah(angka, prefix) {
        var number_string = angka.toString().replace(/[^,\d]/g, ""),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }
        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
    }    
    
    $("#form-pengiriman").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-pengiriman') }}",
            type: "POST",
            data: new FormData($('#form-pengiriman')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                window.open("{{ url('pengiriman-do') }}/"+result.id);
                window.location = "{{ url('pengiriman') }}";
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
                $.each(err.errors, function (key, value) {
                    $('#alert-form-pengiriman').html('<div class="alert alert-danger mb-2" role="alert">'+key+' : '+value+'</div>');
                });
            }
        });
    });
    
</script>
@endsection
