@extends('layouts.app')
@section('content')
<div class="content-wrapper pt-0">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12">
                        <h3 class="content-header-title">MASTER PRODUK</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="float-md-right">
                            <a href="{{ url('product/new') }}" class="btn btn-info round box-shadow-2 px-2"><i class="ft-plus icon-left"></i> Tambah</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-danger ml-1 mb-1" id="buttonDelete"><i class="ft-close"></i> Delete</button>
                            <table class="table table-striped table-bordered selection-deletion-row display nowrap table-sm font-small-3" id="table-product" style="width: 100%">
                                <thead>
                                    <tr style="text-align: center">
                                        <th>ID</th>
                                        <th>Kode</th>
                                        <th>Produk</th>
                                        <th>Qty</th>
                                        <th>Unit</th>
                                        <th>Harga Beli</th>
                                        <th>Harga Jual</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-product">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-product" validate>
                <div class="modal-header">
                    <h4 class="modal-title" id="title-form-product">Tambah Data</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h5>Nama product <span class="required">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="id_product" id="id_product" hidden>
                                    <input type="text" name="product" id="product" class="form-control" required data-validation-required-message="This field is required" placeholder="Ton">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    
    var table_product;
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    function load_list_data(){
        table_product = $('#table-product').DataTable({ 
            "responsive": true,
            "autoWidth": true,
            "processing": true,
            "ajax": {
                "url": "{{ url('datatable-product') }}",
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                "error": function (jqXHR, textStatus, errorThrown) {
                    var err = JSON.parse(jqXHR.responseText);
                }
            },
            "columnDefs": [{ 
                "targets": [ 0 ],
                "visible": false
            }],
        });
        
        $('#table-product tbody').on( 'dblclick', 'tr', function () {
            $('#table-product tbody').removeClass('detail');
            $(this).addClass('detail');
            if(table_product.rows('.detail').data().length > 0){
                location.replace("{{ url('product/detail') }}/"+table_product.row('.detail').data()[0]);
            }
            
        });
        
        $('#table-product tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_product.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
    }
    
    function reload_list_data(){
        table_product.ajax.reload(null, false);
    }
    
    $('#button-tambah-product').click(function () {
        $('#form-product').trigger("reset");
        $("#form-product")[0].reset();
        $('#title-form-product').html('Tambah Data');
    });
    
    $("#form-product").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-product') }}",
            type: "POST",
            data: new FormData($('#form-product')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-product").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
            }
        });
        
        
    });
    
    function getData(id){
        $('#title-form-product').html('Edit Data');
        $.ajax({
            url : "{{ url('get-product') }}/"+id,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-product').modal('show');
                    $('#id_product').val(result.data.id);
                    $('#product').val(result.data.product);
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-product') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    $('#buttonDelete').click( function () {
        if(table_product.rows('.selected').data().length > 0){
            deleteData(table_product.row('.selected').data()[0]);
        }
    });

</script>
@endsection
