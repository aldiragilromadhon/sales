@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <small>Produk</small>
            <h3 class="content-header-title">Buat Produk / Barang</h3>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('product') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-blue white pb-1 pt-1">
                        @if ($product)
                        <h3 class="font-medium-3 text-bold-700 white">Formulir Perubahan Data</h3>
                        @else
                        <h3 class="font-medium-3 text-bold-700 white">Formulir Pengisian Data</h3>
                        @endif
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">
                                    @if ($product)
                                    <form id="form-product" class="form" method="POST" action="{{ url('update-product') }}/{{ $product->id }}" validate>
                                        @else
                                        <form id="form-product" class="form" method="POST" action="{{ url('store-product') }}" validate>
                                            @endif
                                            @csrf
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Nama <span class="red">*</span></label>
                                                        <div class="col-lg-10">
                                                            <div class="controls">
                                                                @if ($product)
                                                                <input class="form-control" type="text" id="productname" name="productname" value="{{ old('productname',$product->name) }}" required/>
                                                                @else
                                                                <input class="form-control" type="text" id="productname" name="productname" value="{{ old('productname') }}" required/>
                                                                @endif
                                                                @error('productname') <p class="red"> {{ $message }}</p> @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Kode / SKU <span class="red">*</span></label>
                                                        <div class="col-lg-10">
                                                            <div class="controls">
                                                                @if ($product)
                                                                <input class="form-control" type="text" id="productcode" name="productcode" value="{{ old('productcode',$product->code) }}" required/>
                                                                @else
                                                                <input class="form-control" type="text" id="productcode" name="productcode" value="{{ old('productcode') }}" required/>
                                                                @endif
                                                                @error('productcode') <p class="red"> {{ $message }}</p> @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Kategori</label>
                                                        <div class="col-lg-10">
                                                            <div class="controls">
                                                                <select class="form-control select2" id="productcategory" name="productcategory" required>
                                                                    <option value="">Pilih Kategori</option>
                                                                    @foreach ($categories as $category)
                                                                    <option value="{{ $category->id }}" @if ($product) @if ($product->category_id == $category->id) selected @endif @endif>{{ $category->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                {{-- @if ($product) --}}
                                                                {{-- <input class="form-control" type="text" id="productcategory" name="productcategory" value="{{ old('productcategory',$product->category) }}"/> --}}
                                                                {{-- @else --}}
                                                                {{-- <input class="form-control" type="text" id="productcategory" name="productcategory" value="{{ old('productcategory') }}"/> --}}
                                                                {{-- @endif --}}
                                                                @error('productcategory') <p class="red"> {{ $message }}</p> @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Unit / Satuan</label>
                                                        <div class="col-lg-10">
                                                            <div class="controls">
                                                                <select class="form-control select2" id="productunit" name="productunit" required>
                                                                    <option value="">Pilih Satuan</option>
                                                                    @foreach ($units as $unit)
                                                                    <option value="{{ $unit->id }}" @if ($product) @if ($product->unit_id == $unit->id) selected @endif @endif>{{ $unit->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                {{-- @if ($product)
                                                                    <input class="form-control" type="text" id="productunit" name="productunit" value="{{ old('productunit',$product->unit) }}"/>
                                                                    @else
                                                                    <input class="form-control" type="text" id="productunit" name="productunit" value="{{ old('productunit') }}"/>
                                                                    @endif --}}
                                                                    @error('productunit') <p class="red"> {{ $message }}</p> @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-lg-2">Deskripsi</label>
                                                            <div class="col-lg-10">
                                                                <div class="controls">
                                                                    <textarea 
                                                                    id="productdescription" 
                                                                    name="productdescription" 
                                                                    rows="3" 
                                                                    class="form-control @error('productdescription') border-danger @enderror"
                                                                    >@if ($product){{ old('productdescription',$product->description) }}@else{{ old('productdescription') }}@endif</textarea>
                                                                    @error('productdescription') <p class="red"> {{ $message }}</p> @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card" style="border: 1px solid #e2e2e2;" hidden>
                                                    <div class="card-header p-1" style="background: #f5f5f5;">
                                                        <h4 class="card-title text-bold-700">Saldo Awal</h4>
                                                    </div>
                                                    <div class="card-content collapse show">
                                                        <div class="card-body p-1">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <fieldset class="form-group form-group-style mb-0">
                                                                        <label for="productbalancequantity">Kuantitas</label>
                                                                        @if ($product)
                                                                        <input type="text" class="form-control" id="productbalancequantity" name="productbalancequantity" placeholder="0" value="{{ old('productbalancequantity',$product->balance_quantity) }}">
                                                                        @else
                                                                        <input type="text" class="form-control" id="productbalancequantity" name="productbalancequantity" placeholder="0" value="{{ old('productbuyprice') }}">
                                                                        @endif
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <fieldset class="form-group form-group-style mb-0">
                                                                        <label for="productbalanceaverageprice">Harga Beli Rata2</label>
                                                                        @if ($product)
                                                                        <input type="text" class="form-control" id="productbalanceaverageprice" name="productbalanceaverageprice" placeholder="Rp. 0" value="{{ old('productbalanceaverageprice',$product->balance_average_price) }}">
                                                                        @else
                                                                        <input type="text" class="form-control" id="productbalanceaverageprice" name="productbalanceaverageprice" placeholder="Rp. 0" value="{{ old('productbalanceaverageprice') }}">
                                                                        @endif
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label for="productbalancedate"><strong class="text-uppercase">Tanggal</strong></label>
                                                                    @if ($product)
                                                                    <input type="text" class="form-control" id="productbalancedate" name="productbalancedate" value="{{ old('productbalancedate',$product->balance_date) }}" style="background-color: white;" >
                                                                    @else
                                                                    <input type="text" class="form-control" id="productbalancedate" name="productbalancedate" value="{{ old('productbalancedate',date('d-m-Y')) }}" style="background-color: white;" >
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="form-section">Harga & Pengaturan</h4>
                                                <div class="card box-shadow-0" style="border: 1px solid #e2e2e2;">
                                                    <div class="card-header p-1" style="background: #f5f5f5;">
                                                        <h4 class="card-title">Saya Beli Produk Ini</h4>
                                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                        <div class="heading-elements">
                                                            <ul class="list-inline mb-0" style="margin-top: -10px;">
                                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-content collapse show">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <fieldset class="form-group form-group-style mb-0">
                                                                        <label for="productbuyprice">Harga Beli Satuan</label>
                                                                        @if ($product)
                                                                        <input type="text" class="form-control" id="productbuyprice" name="productbuyprice" placeholder="Rp. 0" value="{{ old('productbuyprice',$product->buy_price) }}">
                                                                        @else
                                                                        <input type="text" class="form-control" id="productbuyprice" name="productbuyprice" placeholder="Rp. 0" value="{{ old('productbuyprice') }}">
                                                                        @endif
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <fieldset class="form-group form-group-style mb-0">
                                                                        <label for="productbuytax">Pajak Beli</label>
                                                                        <select class="form-control" id="productbuytax" name="productbuytax">
                                                                            <option value="" selected> Pilih Pajak</option>
                                                                            <option value="PPN">PPN</option>
                                                                        </select>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card box-shadow-0" style="border: 1px solid #e2e2e2;">
                                                    <div class="card-header p-1" style="background: #f5f5f5;">
                                                        <h4 class="card-title">Saya Jual Produk Ini</h4>
                                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                        <div class="heading-elements">
                                                            <ul class="list-inline mb-0" style="margin-top: -10px;">
                                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-content collapse show">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <fieldset class="form-group form-group-style mb-0">
                                                                        <label for="productsellprice">Harga Jual Satuan</label>
                                                                        @if ($product)
                                                                        <input type="text" class="form-control" id="productsellprice" name="productsellprice" placeholder="Rp. 0" value="{{ old('productsellprice',$product->sell_price) }}">
                                                                        @else
                                                                        <input type="text" class="form-control" id="productsellprice" name="productsellprice" placeholder="Rp. 0" value="{{ old('productsellprice') }}">
                                                                        @endif
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <fieldset class="form-group form-group-style mb-0">
                                                                        <label for="productselltax">Pajak Jual</label>
                                                                        <select class="form-control" id="productselltax" name="productselltax">
                                                                            <option value="" selected> Pilih Pajak</option>
                                                                            <option value="PPN">PPN</option>
                                                                        </select>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card box-shadow-0" style="border: 1px solid #e2e2e2;" hidden>
                                                    <div class="card-header p-1" style="background: #f5f5f5;">
                                                        <h4 class="card-title">Monitor Persediaan Barang</h4>
                                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                                        <div class="heading-elements">
                                                            <ul class="list-inline mb-0" style="margin-top: -10px;">
                                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-content collapse show">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <fieldset class="form-group form-group-style mb-0">
                                                                        <label for="productbufferquantity">Batas Stok Minimum</label>
                                                                        @if ($product)
                                                                        <input type="text" class="form-control" id="productbufferquantity" name="productbufferquantity" placeholder="0" value="{{ old('productbufferquantity',$product->buffer_quantity) }}">
                                                                        @else
                                                                        <input type="text" class="form-control" id="productbufferquantity" name="productbufferquantity" placeholder="0" value="{{ old('productbufferquantity') }}">
                                                                        @endif
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-actions right">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                    <button type="button" class="btn btn-success button-submit">Simpan</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="bs-callout-info callout-bordered callout-transparent p-1" id="kolom-detail-pekerjaan">
                                                <h5 class="text-bold-600">Nama Produk</h5>
                                                <p id="detail-nama-pekerjaan">Tulis nama produk beserta keterangannya, seperti tipe, merek, dan keterangan lainnya.</p>
                                                <h5 class="text-bold-600">Kode Produk</h5>
                                                <p id="detail-nama-pekerjaan">Masukkan Kode / SKU untuk setiap produk untuk mempermudah Anda dalam pelacakan.</p>
                                                <h6 class="text-bold-600">Kategori Produk</h6>
                                                <p id="detail-nama-pekerjaan">Tentukan kategori setiap produk untuk mengelompokkan produk per kategori.</p>
                                                <h6 class="text-bold-600">Produk Unit</h6>
                                                <p id="detail-nama-pekerjaan">Tentukan satuan yang akan digunakan untuk mendeskripsi jumlah produk.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('script')
        <script src="{{asset('vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
                $('#productbalancedate').pickadate({
                    format: 'dd-mm-yyyy'
                });
                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            });
            
            @if ($product)
            $('#productunit').val("{{old('productunit',$product->unit)}}").change();
            $('#productcategory').val("{{old('productcategory',$product->category)}}").change();
            @endif
            
            @if ($product)
            $('#atasan_id').val("{{old('atasan_id',$product->atasan_id)}}").change();
            @else
            $('#atasan_id').val("{{old('atasan_id')}}").change();
            @endif
            
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();    
            $('.button-submit').dblclick(function(e){
                $('#form-product').submit();
            });
        </script>
        @endsection
        