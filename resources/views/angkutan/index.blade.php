@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <small>Transaksi</small>
            <h2 class="content-header-title">Angkutan</h2>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('angkutan/new') }}" type="button" class="btn btn-info round box-shadow-2 px-2 btn-glow"><i class="ft-plus icon-left"></i> Buat Angkutan Baru</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        @if (session('success'))
        <div class="alert alert-success mb-2" role="alert">
            {{ session('success') }}
        </div>
        @endif
        
        @if (session('error'))
        <div class="alert alert-warning mb-2" role="alert">
            {{ session('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="row">
                                <div class="col-12 col-md-4 pb-1 pl-1 pr-1">
                                    <div class="input-group">
                                        <select name="periode_laporan" id="periode_laporan" class="form-control">
                                            @foreach($periode as $data)
                                            @if ($data->tanggal == now()->format('Y-m'))
                                            <option value="{{ $data->tanggal }}" selected>{{ $data->month }} {{ $data->year }}</option>
                                            @else
                                            <option value="{{ $data->tanggal }}">{{ $data->month }} {{ $data->year }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <div class="input-group-append">
                                            <button class="btn btn-success" type="button" onclick="reload_list_data()">Cari!</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 text-right p-1">
                                    <button class="btn btn-info" id="buttonDetail"><i class="ft-eye"></i> DETAIL</button>
                                    <button class="btn btn-warning" id="buttonPrintPayment"><i class="ft-printer"></i> PAY</button>
                                    <button class="btn btn-success" id="buttonPrint"><i class="ft-printer"></i> INV</button>
                                    <button class="btn btn-success" id="buttonPrintDO"><i class="ft-printer"></i> DO</button>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered selection-deletion-row display table-xs" id="table-angkutan" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>ID</th>
                                        <th>Tanggal</th>
                                        <th>No. PO</th>
                                        <th>No. Angkutan</th>
                                        <th>Pengirim</th>
                                        <th>Status</th>
                                        <th>Total (dalam IDR)</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var table_data;
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    $('#buttonEdit').click( function () {
        var rows = table.rows('.selected').data().length;
        if(rows > 0){
            var edit = table.row('.selected').data()[0];
            var status = table.row('.selected').data()[10];
            if(status == 'OPEN')
            {					
                $('#mymodal').modal('show');
                get_data_to_update(edit);
            }else{
                swal("Info!", "Delivery Order yang anda pilih sedang berjalan", "success");
            } 
        }
    });
    
    function load_list_data(){
        table_data = $('#table-angkutan').DataTable({ 
            "responsive": true,
            // "autoWidth": true,
            "processing": true,
            "ajax": {
                "url": "{{ url('datatable-angkutan') }}",
                "type": "POST",
                "data": function ( d ) {
                    d.tanggal = $('#periode_laporan').val();
                },
                "headers": {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                "error": function (jqXHR, textStatus, errorThrown) {
                    var err = JSON.parse(jqXHR.responseText);
                }
            },
            "columnDefs": [{ 
                "targets": [ 0 ],
                "visible": false
            }],
        });
        
        $('#table-angkutan tbody').on( 'dblclick', 'tr', function () {
            table_data.$('tr.detail').removeClass('detail');
            $('#table-angkutan tbody').removeClass('detail');
            $(this).addClass('detail');
            get_detail();
        });
        
        $('.selection-deletion-row tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_data.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
        
    }
    
    function reload_list_data(){
        table_data.ajax.reload(null, false);
    }
    
    function get_detail(){
        if(table_data.rows('.detail').data().length > 0){
            location.assign("{{ url('angkutan/detail') }}/"+table_data.row('.detail').data()[0]);
        }
    }
    
    $('#buttonPrint').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            $.get("{{ url('angkutan-check') }}/"+table_data.row('.selected').data()[0], function(response, status, xhr){
                if(response){
                    window.open("{{ url('angkutan-po') }}/"+table_data.row('.selected').data()[0]);
                }else{
                    swal("Info!", "Cetak Invoice di menu penjualan", "info");
                }
            });
        }
    });
    
    $('#buttonPrintDO').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            window.open("{{ url('angkutan-do') }}/"+table_data.row('.selected').data()[0]);
        }
    });

    $('#buttonPrintPayment').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            $.get("{{ url('angkutan-check-pay') }}/"+table_data.row('.selected').data()[0], function(response, status, xhr){
                if(response){
                    window.open("{{ url('angkutan-payment') }}/"+table_data.row('.selected').data()[0]);
                }else{
                    swal("Info!", "No pembayaran/bill belum diisi", "info");
                }
            });

        }
    });

    $('#buttonDetail').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            location.assign("{{ url('angkutan/detail') }}/"+table_data.row('.selected').data()[0]);
        }
    });
    
    function deleteData(id){
        $('#id_surat_jalan').val(id);
        $('#modal-persetujuan').modal('show');
    }
    
    $("#form-persetujuan").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('approval/store') }}",
            type: "POST",
            data: new FormData($('#form-persetujuan')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(data) {
                swal("Sukses!", "Data berhasil di proses!.", "success");
                $('#modal-persetujuan').modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    });
    
    $('#button-proses-modal').click(function(){
        $('#button-submit-modal').click();
    })
    
</script>
@endsection
