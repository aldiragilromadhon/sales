<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Jalan</title>
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
</head>
<style>
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        color: black;
    }
    body{
        font-size: 12px;
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        background-color: white;
        color: black;
    }
    table{
        width: 100%;
    }
    .table-bordered, .border{
        border: 1px solid black;
    }
    .table th, .table td {
        padding: 0.25rem;
    }
    .border .border-dark{
        color: black;
        border: 1px solid black;
    }
</style>
<body>
    <div class="ml-1 mr-1 p-1 border border-dark" style="color: black;border: 1px solid black;">
        <table>
            <tr>
                <td class="text-center" width="30%"><img src="{{ public_path('images/'.$settings['2']['value']) }}" class="text-center" alt="company logo" style="width: 80%"></td>
                <td width="50%" class="text-left">
                    <h4 class="m-0 p-0">{{ $settings['1']['value'] }}</h4>
                    <p class="m-0 p-0">email : cvbintangalkacasejahtera@gmail.com</p>
                    <p class="m-0 p-0">Jl Dr Cipto Perumahan BTN Blok L/11</p>
                    <p class="m-0 p-0">Kab Sumenep - Provinsi Jawa timur</p>
                </td>
                <td class="text-center" width="20%" style="vertical-align: top; padding-left:15px;">
                </td>
            </tr>
        </table>
        <hr/>
        <table>
            <tr style="text-align: center">
                <td class="text-center">
                    <h4 class="m-0"><u>REKAP TRUCK, SURAT JALAN DAN BERAT TONASE</u></h4>
                </td>
            </tr>
        </table>
        
        @php
        $no_tgl = 7;
        $no = 0;
        $nomor = 0;
        $tgl = 0;
        $tonase = 0;
        $karung = 0;
        $tonase_week = 0;
        $karung_week = 0;
        @endphp
        @foreach ($suratjalan as $s)
        
        @if ($tgl != $s->tgl_transaksi && $no != 0)
        <tr>
            <td colspan="5"> Jumlah {{ get_name_day($nomor+1) }}</td>
            <td>{{ $tonase }}</td>
            <td>{{ $karung }}</td>
        </tr>
        @php
        $tonase = 0;
        $karung = 0;
        @endphp
        @endif
        @php
        $nomor = $s->tanggal;
        @endphp
        @if ($no_tgl > $s->tanggal)
        @if ($no_tgl != 7)
        <tr>
            <td colspan="5"> <b>Total Jumlah</b> </td>
            <td>{{ $tonase_week }}</td>
            <td>{{ $karung_week }}</td>
        </tr>
        
    </table>
    @php
    $tonase_week = 0;
    $karung_week = 0;
    @endphp
    @endif
    
    <table class="table table-bordered table-striped mt-1 text-center">
        <tr>
            <th rowspan="2" style="vertical-align: middle;">No</th>
            <th rowspan="2" style="vertical-align: middle;">Hari</th>
            <th rowspan="2" style="vertical-align: middle;">Tanggal</th>
            <th rowspan="2" style="vertical-align: middle;">Surat Jalan</th>
            <th rowspan="2" style="vertical-align: middle;">Truck</th>
            <th colspan="2">Berat</th>
        </tr>
        <tr>
            <th>Tonase</th>
            <th>Karung</th>
        </tr>
        @endif
        @php
        $count = 0;
        $tonase += $s->tonase;
        $karung += $s->karung;
        $tonase_week += $s->tonase;
        $karung_week += $s->karung;
        @endphp
        <tr>
            <td>{{ ++$no }}</td>
            
            @if ($tgl != $s->tgl_transaksi)
            @php
            $tgl = $s->tgl_transaksi;
            @endphp
            
            @foreach ($suratjalan as $sj)
            @if ($sj->tgl_transaksi == $s->tgl_transaksi)
            @php $count++; @endphp
            @endif
            @endforeach
            
            <td rowspan="{{ $count }}" style="vertical-align: middle;">{{ get_name_day($s->tanggal+1) }}</td>
            @endif
            
            <td>{{ $s->tgl_transaksi }}</td>
            <td>{{ ($s->no_parent?$s->no_parent:$s->no_transaksi) }}</td>
            <td>{{ $s->no_polisi }}</td>
            <td>{{ number_format($s->tonase,3,',','.') }}</td>
            <td>{{ number_format($s->karung,0,',','.') }}</td>
        </tr>
        @php
        $no_tgl = $s->tanggal;
        @endphp

        @endforeach
        <tr>
            <td colspan="5"> Jumlah {{ get_name_day($nomor+1) }}</td>
            <td>{{ number_format($tonase,3,',','.') }}</td>
            <td>{{ number_format($karung,0,',','.') }}</td>
        </tr>
        <tr>
            <td colspan="5"> <b>Total Jumlah</b> </td>
            <td>{{ number_format($tonase_week,3,',','.') }}</td>
            <td>{{ number_format($karung_week,0,',','.') }}</td>
        </tr>
    </table>
    
</table>
</div>
</body>
</html>