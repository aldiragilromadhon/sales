<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Delivery Order</title>
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
</head>
<style>
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        color: black;
    }
    body{
        font-size: 12px;
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        background-color: white;
        color: black;
    }
    table{
        width: 100%;
    }
    .table-bordered, .border{
        border: 1px solid black;
    }
    .table th, .table td {
        padding: 0.25rem;
    }
    .border .border-dark{
        color: black;
        border: 1px solid black;
    }
    footer {
        position: fixed; 
        bottom: -60px; 
        left: 20px; 
        right: 0px;
        height: 50px; 
        
        color: black;
        text-align: left;
    }
</style>
<body>
    <div class="ml-1 mr-1 p-1 border border-dark" style="color: black;border: 1px solid black;">
        <table>
            <tr>
                <td class="text-center" width="30%"><img src="{{ public_path('images/'.$settings['2']['value']) }}" class="text-center" alt="company logo" style="width: 80%"></td>
                <td width="50%" class="text-left">
                    <h5 class="m-0 p-0">{{ $settings['1']['value'] }}</h5>
                    <p class="m-0 p-0">email : cvbintangalkacasejahtera@gmail.com</p>
                    <p class="m-0 p-0">Jl Dr Cipto Perumahan BTN Blok L/11</p>
                    <p class="m-0 p-0">Kab Sumenep - Provinsi Jawa timur</p>
                </td>
                <td class="text-center" width="20%" style="vertical-align: top; padding-left:15px;">
                    <table hidden>
                        <tr>
                            <td class="right" style="text-align: center; font-weight: bold;" width="100%">
                                <table class="m-0 p-0">
                                    <tr>
                                        <td style="text-align: left; vertical-align: top;" width="10%">Tgl</td>
                                        <td style="text-align: left; vertical-align: top;" width="1%">:</td>
                                        <td style="text-align: left">{{ date('d F Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: top;" width="10%">Alamat</td>
                                        <td style="text-align: left; vertical-align: top;" width="1%">:</td>
                                        <td style="text-align: left;">{!! nl2br(e($pengiriman->detail_location_to)) !!}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr/>
        <table>
            <tr style="text-align: center">
                <td class="text-center">
                    <h4 class="m-0"><u>DELIVERY ORDER</u></h4>
                    <h5 class="m-0 p-0">{{ ($pengiriman->no_parent?$pengiriman->no_parent:$pengiriman->no_transaksi) }}</h5>
                </td>
            </tr>
        </table>
        <table class="mt-1">
            <tr>
                <td style="text-align: left" width="50%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="19%"><b>Pelanggan</b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $pengiriman->contact->nama }} / {{ $pengiriman->contact->perusahaan }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: left; vertical-align: top;" width="19%"><b>Alamat</b></td>
                            <td style="text-align: left; vertical-align: top;" width="1%">:</td>
                            <td style="text-align: left;" width="80%">{!! nl2br(e($pengiriman->detail_location_from)) !!}</td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: left" width="20%">
                </td>
                <td class="right" style="text-align: right; vertical-align: top;" width="30%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="70px"><b>Tanggal   </b></td><td>: {{ date('d F Y',strtotime($pengiriman->tgl_transaksi)) }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="text-align: left" width="50%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="19%"><b>No PO </b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $pengiriman->no_referensi }}</td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: left" width="20%">
                </td>
                <td class="right" style="text-align: right; vertical-align: top;" width="30%">
                    <table class="m-0 p-0">
                        <tr>
                            @if ($pengiriman->tgl_po)
                            <td width="70px"><b>Tanggal PO</b></td><td>: {{ date('d F Y',strtotime($pengiriman->tgl_po)) }}</td>
                            @endif
                        </tr>
                    </table>
                </td>
            </tr>
        </table>            
        @php
        $no = 1;
        @endphp
        <table class="table mb-0 mt-2">
            <thead>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='1%' rowspan="2">No</th>
                    <th class="border text-center" width='34%' rowspan="2">Keterangan</th>
                    <th class="border text-center" width='28%' colspan="2">Volume</th>
                </tr>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='14%'>Tonase</th>
                    <th class="border text-center" width='14%'>Karung</th>
                </tr>
            </thead>
            <tbody>
                @php
                $no = 0;
                @endphp
                @foreach ($pengiriman->barang as $b)
                <tr class="border">
                    <td class="border" style="text-align: center;">{{ ++$no }}</td>
                    <td class="border" style="text-align: left;">{{ $b->produk }}</td>
                    <td class="border" style="text-align: center;">{{ number_format($b->tonase,3,',','.') }}</td>
                    <td class="border" style="text-align: center;">{{ number_format($b->karung,0,',','.') }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        <table class="text-center mt-3" >
            <tr>
                <td width="30%"><b>Yang Menerima<br/>Gudang</b></td>
                <td width="5"></td>
                <td width="30%"><b>Yang Mengetahui<br/>Ekspedisi</b></td>
                <td width="5"></td>
                <td width="30%"><b>Yang Mengetahui</b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td height="50px"></td>
            </tr>
            <tr>
                <td>(.......................................)</td>
                <td></td>
                <td>(.......................................)</td>
                <td></td>
                <td>( {{ $pengiriman->user }} )</td>
            </tr>
        </table>
        
    </div>
    <p style="font-size: 10px; margin-left: 15px; "><i><b>Dokumen ini sah, diterbitkan oleh {{ $settings['1']['value'] }} secara elektronik melalui sistem dan tidak membutuhkan cap dan tandatangan basah.</b></i></p>             
    <p style="font-size: 8px; margin-left: 15px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p>
    {{-- <footer> --}}
        {{-- <p class="m-0 p-0" style="font-size: 8px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p> --}}
    {{-- </footer> --}}
</body>
</html>