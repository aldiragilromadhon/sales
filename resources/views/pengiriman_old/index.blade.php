@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <small>Transaksi</small>
            <h2 class="content-header-title">Pengiriman</h2>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('pengiriman/new') }}" type="button" class="btn btn-info round box-shadow-2 px-2 btn-glow"><i class="ft-plus icon-left"></i> Buat Pengiriman Baru</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-success ml-1 mb-1" id="buttonPrint"><i class="ft-printer"></i> Cetak</button>
                            <table class="table table-striped table-bordered selection-deletion-row display nowrap table-sm" id="table-pengiriman" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>ID</th>
                                        <th>Tanggal</th>
                                        <th>Nomor</th>
                                        <th>Pengirim</th>
                                        <th>Penerima</th>
                                        <th>Status</th>
                                        <th>Total (dalam IDR)</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var table_data;
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    $('#buttonEdit').click( function () {
        var rows = table.rows('.selected').data().length;
        if(rows > 0){
            var edit = table.row('.selected').data()[0];
            var status = table.row('.selected').data()[10];
            if(status == 'OPEN')
            {					
                $('#mymodal').modal('show');
                get_data_to_update(edit);
            }else{
                swal("Info!", "Delivery Order yang anda pilih sedang berjalan", "success");
            } 
        }
    });
    
    function load_list_data(){
        table_data = $('#table-pengiriman').DataTable({ 
            "responsive": true,
            "autoWidth": true,
            "processing": true,
            "ajax": {
                "url": "{{ url('datatable-pengiriman') }}",
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                "error": function (jqXHR, textStatus, errorThrown) {
                    var err = JSON.parse(jqXHR.responseText);
                }
            },
            "columnDefs": [{ 
                "targets": [ 0 ],
                "visible": false
            }],
        });
        
        $('#table-pengiriman tbody').on( 'dblclick', 'tr', function () {
            $('#table-pengiriman tbody').removeClass('detail');
            $(this).addClass('detail');
            get_detail();
        });
        
        $('.selection-deletion-row tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_data.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        
        
    }
    
    function reload_list_data(){
        table_data.ajax.reload(null, false);
    }
    
    function get_detail(){
        if(table_data.rows('.detail').data().length > 0){
            location.replace("{{ url('pengiriman/detail') }}/"+table_data.row('.detail').data()[0]);
        }
    }
    
    $('#buttonPrint').click( function () {
        if(table_data.rows('.selected').data().length > 0){
            window.open("{{ url('pengiriman-po') }}/"+table_data.row('.selected').data()[0]);
        }
    });
    
    function deleteData(id){
        $('#id_surat_jalan').val(id);
        $('#modal-persetujuan').modal('show');
    }
    
    $("#form-persetujuan").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('approval/store') }}",
            type: "POST",
            data: new FormData($('#form-persetujuan')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(data) {
                swal("Sukses!", "Data berhasil di proses!.", "success");
                $('#modal-persetujuan').modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    });
    
    $('#button-proses-modal').click(function(){
        $('#button-submit-modal').click();
    })
    
</script>
@endsection
