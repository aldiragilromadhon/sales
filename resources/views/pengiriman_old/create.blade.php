@extends('layouts.app')
@section('style')
<style type="text/css">
    .remove-td td {
        padding: 0.5rem 0.5rem;
    }
</style>
@endsection
@section('content')
<div class="content-wrapper pt-2">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mt-1 m-0">
                Buat Penagihan Pengiriman
            </h3>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-2">
            <div class="float-md-right">
                <a href="{{ url('pengiriman') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body ">
        @if (session('success'))
        <div class="alert alert-success mb-2" role="alert">
            {{ session('success') }}
        </div>
        @endif
        
        @if (session('error'))
        <div class="alert alert-warning mb-2" role="alert">
            {{ session('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div id="alert-form-pengiriman"></div>
                <div class="card">
                    <div class="card-content">
                        <form class="form form-horizontal" method="POST" validate id="form-pengiriman">
                            @csrf
                            <div class="card-body p-1" style="background-color: #B1DCF7">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <h5>Perusahaan <span class="red">*</span></h5>
                                            <div class="controls">
                                                <select name="pengirimancustomer" id="pengirimancustomer" class="form-control js-example-events select2" required style="width: 100%;">
                                                    <option value="">Pilih Kontak</option>
                                                    @foreach($contact as $data)
                                                    <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <h5>No Referensi / Purchase Order</h5>
                                        <input type="text" class="form-control" id="pengirimannomorreferensi" name="pengirimannomorreferensi" placeholder="">
                                    </div>
                                    <div class="col-lg-6 text-right center pt-2 pr-2">
                                        <p class="font-medium-3 text-bold-600" id="totalpengirimanatas">Total Rp. 0,00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-1">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Alamat Pengirim</label>
                                                <textarea class="form-control" id="pengirimanalamatpengiriman" name="pengirimanalamatpengiriman" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Alamat Penerima</label>
                                                <textarea class="form-control" id="pengirimanalamatpenerimaan" name="pengirimanalamatpenerimaan" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Tgl Transaksi</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="pengirimantanggal" name="pengirimantanggal" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover remove-td " id="table-product-pengiriman">
                                                <thead>
                                                    <tr>
                                                        <th width="25%">Asal</th>
                                                        <th width="25%">Tujuan</th>
                                                        <th width="9%">Berat</th>
                                                        <th width="18%">Harga</th>
                                                        <th width="22%">Total</th>
                                                        <th width="1%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7">
                                        {{-- <button type="button" class="btn btn-info btn-min-width box-shadow-2 mt-1" id="button-add-row-product-pengiriman"><i class="la la-plus-square"></i> Tambah Data</button> --}}
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="table-responsive">
                                            <table class="table table-borderless mb-0 p-0 m-0">
                                                <tbody>
                                                    <tr>
                                                        <td><h2 class="m-0">Sisa Tagihan</h2>
                                                            <input type="text" id="pengirimantotal" name="pengirimantotal" value="0" hidden>
                                                        </td>
                                                        <td class="type-info text-right"><h2 id="totaltagihanpengiriman" class="m-0">Rp. 0</h2></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <button type="submit" class="btn btn-success" >Buat Pengiriman</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var rowIdx = 0, sumdiskon = 0, sumpajak = 0, sumtotal = 0;
    $(document).ready(function () {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        addRowsTablePengiriman();
        $('#pengirimantanggal, #pengirimanjatuhtempo').pickadate({
            format: 'dd-mm-yyyy',
            today: '',
            close: 'Close',
            clear: ''
        });
    });
    
    $('#button-add-row-product-pengiriman').on('click', function () {
        addRowsTablePengiriman();
    });
    
    $('#table-product-pengiriman tbody').on('click', '.remove', function () {
        var row = $(this).closest('tr');
        row.remove();
        sumdiskon = 0;
        $("#table-product-pengiriman tbody input[name='producttotaldiskon[]']").map(function(){ return $(this).val();}).get().forEach(setDiskonPengiriman);
        sumpajak = 0;
        $("#table-product-pengiriman tbody input[name='producttotalpajak[]']").map(function(){ return $(this).val();}).get().forEach(setPajakPengiriman);
        sumtotal = 0;
        $("#table-product-pengiriman tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPengiriman);
        setGrandTotal();
    });
    
    
    $('#pengirimancustomer').on('change', function() {
        if ($(this).val()) {
            $.ajax("{{ url('get-address-contact') }}/"+$(this).val(),{
                success: function (data, status, xhr) {
                    $('#pengirimanalamatpengiriman').val(data);
                }
            });
        }
    });
    
    function addRowsTablePengiriman() {
        if(!$('.check-text-isset').filter(function(){ return !this.value.trim(); }).length){
            $('#table-product-pengiriman tbody').append(`
            <tr>
                <td>
                    <select class="form-control select2 select2-table check-text-isset" id="locationasal" name="locationasal" style="width: 100%;">
                        <option value=""><i>- Pilih Lokasi -</i></option>
                        @foreach($location as $p)
                        <option value="{{$p->id}}">{{$p->name}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select class="form-control select2 select2-table check-text-isset" id="locationtujuan" name="locationtujuan" style="width: 100%;">
                        <option value=""><i>- Pilih Lokasi -</i></option>
                        @foreach($location as $p)
                        <option value="{{$p->id}}">{{$p->name}}</option>
                        @endforeach
                    </select>
                </td>
                <td><input type="text" class="form-control class-money-format class-quantity" id="locationweight" name="locationweight" value="1"></td>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" class="form-control text-right" id="locationprice" name="locationprice" value="0">
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" class="form-control text-right" id="locationtotal" name="locationtotal" value="0">
                    </div>
                </td>
                <td><button type="button" class="btn btn-icon btn-pure danger remove"><i class="la la-close"></i></button></td>
            </tr>`);
            
            $('.select2-table').select2()
            .on("change", function(e) {
                // alert($(this).closest('tr').find('#locationasal').val()+'|'+$(this).closest('tr').find('#locationtujuan').val());
                if ($(this).closest('tr').find('#locationasal').val() && $(this).closest('tr').find('#locationtujuan').val()) {
                    $(this).closest('tr').find('#locationprice').load("{{ url('get-location-price') }}/"+$(this).closest('tr').find('#locationasal').val()+'/'+$(this).closest('tr').find('#locationtujuan').val(),function(response, status, xhr){
                        var res = JSON.parse(response);
                        if (res.status) {
                            $(this).val(formatRupiah(res.data.price, ''));
                            $(this).closest('tr').find('#locationtotal').val(formatRupiah(parseInt(res.data.price)*parseInt($(this).closest('tr').find('#locationweight').val()),''));
                        }else{
                            swal("Location Mapping belum didefinisikan!");
                        }
                    });
                }
            });
            
            $('.class-money-format').on("keyup", function(e) {
                var weight = $(this).closest('tr').find('#locationweight').val() == 0 ? 1 : $(this).closest('tr').find('#locationweight').val().toString().replace(/[^,\d]/g, "");
                var price = $(this).closest('tr').find('#locationprice').val() == 0 ? 1 : $(this).closest('tr').find('#locationprice').val().toString().replace(/[^,\d]/g, "");
                
                $(this).closest('tr').find('#locationtotal').val(formatRupiah(parseInt(weight)*parseInt(price),''));
                $(this).closest('tr').find('#totaltagihanpengiriman').html(formatRupiah(parseInt(weight)*parseInt(price),''));
                $(this).closest('tr').find('#pengirimantotal').val(formatRupiah(parseInt(weight)*parseInt(price),''));
            });
            
            $('.class-quantity').focus(function(e) {
                if ($(this).val() == 1) {
                    $(this).val('');
                }
            });
            $('.class-quantity').focusout(function(e){
                if ($(this).val() == '') {
                    $(this).val('1');
                }
            });
            
            ;
        }
    }
    
    $('#pengirimanpemotongan, #pengirimanuangmuka').on("keyup", function(e) {
        setGrandTotal();
        $('#showpengirimanpemotongan').html(formatRupiah($('#pengirimanpemotongan').val(), 'Rp '));
        $('#showpengirimanuangmuka').html(formatRupiah($('#pengirimanuangmuka').val(), 'Rp '));
    });
    
    
    
    
    function setTotalPengiriman(total) {
        temp_total = total.toString().replace(/[^,\d]/g, "").split(",");
        sumtotal += parseFloat(temp_total[0]+'.'+temp_total[1]);
        var temp_sum = sumtotal.toFixed(2).toString().split(".");
        $('#totalpengirimanatas, #totalpengirimansubtotal').html(formatRupiah(temp_sum[0]+','+temp_sum[1], 'Rp '));
        $('#hiddentotalpengirimansubtotal').val(sumtotal);
    }
    
    function setGrandTotal(){
        var pemotong = $('#pengirimanpemotongan').val() == '' ? 0 : $('#pengirimanpemotongan').val();
        var grand_total_temp = parseFloat(parseFloat($('#hiddentotalpengirimansubtotal').val()-$('#hiddendiskonpengirimanperbaris').val())+parseFloat($('#hiddenpajakpengirimanperbaris').val())-parseFloat(pemotong));
        var grand_total = grand_total_temp.toFixed(2).toString().split(".");
        $('#totalpengirimanbawah').html(formatRupiah(grand_total[0]+','+grand_total[1], 'Rp '));
        var uangmuka = $('#pengirimanuangmuka').val() == '' ? 0 : $('#pengirimanuangmuka').val();
        var sisa_uangmuka_temp = parseFloat(grand_total_temp-uangmuka);
        var sisa_uangmuka = sisa_uangmuka_temp.toFixed(2).toString().split(".");
        $('#sisatagihanpengiriman').html(formatRupiah(sisa_uangmuka[0]+','+sisa_uangmuka[1], 'Rp '));
        $('#pengirimansisatagihan').val(sisa_uangmuka_temp);
        
    }
    
    $( "#pengirimanuangmuka, #pengirimanpemotongan" ).focus(function(e) {
        if ($(this).val() == 0) {
            $(this).val('');
        }
    });
    $( "#pengirimanuangmuka, #pengirimanpemotongan" ).focusout(function(e){
        if ($(this).val() == '') {
            $(this).val('0');
        }
    });
    
    function formatRupiah(angka, prefix) {
        var number_string = angka.toString().replace(/[^,\d]/g, ""),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }
        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
    }
    
    $('#pengirimantipepembayaran, #pengirimantanggal').change( function(e) { 
        var temp_date = $("#pengirimantanggal").val().split('-');
        console.log(temp_date[2]+'-'+temp_date[1]+'-'+temp_date[0]);
        var date = new Date(temp_date[2]+'-'+temp_date[1]+'-'+temp_date[0]);
        date.setDate(date.getDate() + parseInt($('#pengirimantipepembayaran').val()));
        $("#pengirimanjatuhtempo").val(date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
    });
    
    
    $("#form-pengiriman").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-pengiriman') }}",
            type: "POST",
            data: new FormData($('#form-pengiriman')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                window.open("{{ url('pengiriman-po') }}/"+result.id);
                window.location = "{{ url('pengiriman') }}";
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
                $.each(err.errors, function (key, value) {
                    $('#alert-form-pengiriman').html('<div class="alert alert-danger mb-2" role="alert">'+key+' : '+value+'</div>');
                });
            }
        });
        
        
    });
    setTimeout(function(){ $('#hidden-menu').click() }, 1000);
    
</script>
@endsection
