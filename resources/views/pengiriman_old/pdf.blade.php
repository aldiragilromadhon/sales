<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
</head>
<style>
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        color: black;
    }
    body{
        font-size: 12px;
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        background-color: white;
        color: black;
    }
    table{
        width: 100%;
    }
    .table-bordered, .border{
        border: 1px solid black;
    }
    .table th, .table td {
        padding: 0.25rem;
    }
    .border .border-dark{
        color: black;
        border: 1px solid black;
    }
</style>
<body>
    <div class="ml-1 mr-1 p-1 border border-dark" style="color: black;border: 1px solid black;">
        <table>
            <tr>
                <td class="text-center" width="30%"><img src="{{asset('images/'.$settings['2']['value'])}}" class="text-center" alt="company logo" style="width: 80%"></td>
                <td width="60%" class="text-left">
                    <h4 class="m-0 p-0">{{ $settings['1']['value'] }}</h4>
                    <p class="m-0 p-0">email : cvbintangalkacasejahtera@gmail.com</p>
                    <p class="m-0 p-0">Jl Dr Cipto Perumahan BTN Blok L/11</p>
                    <p class="m-0 p-0">Kab Sumenep - Provinsi Jawa timur</p>
                </td>
                <td class="text-center" width="10%"></td>
            </tr>
        </table>
        <hr/>
        <table>
            <tr style="text-align: center">
                <td class="text-center">
                    <h4 class="m-0"><u>INVOICE</u></h4>
                    <h5 class="m-0 p-0">{{ $pengiriman->no_transaksi }}</h5>
                </td>
            </tr>
        </table>
        <table class="mt-1">
            <tr>
                <td style="text-align: left" width="45%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="33%">Kepada Yth</td>
                        </tr>
                        <tr>
                            <td width="33%"><b>Bpk. {{ $pengiriman->contact->nama }}</b></td>
                        </tr>
                        <tr>
                            <td width="33%">{!! nl2br(e($pengiriman->contact->alamat)) !!}</td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: left" width="30%">
                </td>
                <td class="right" style="text-align: right; vertical-align: top;" width="25%">
                    Tanggal : {{ date('d F Y',strtotime($pengiriman->tgl_transaksi)) }}
                </td>
            </tr>
        </table>
        @php
        $no = 1;
        @endphp
        <table class="table table-bordered mb-0 mt-2">
            <thead>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='50%'>Alamat Pengirim</th>
                    <th class="border text-center" width='50%'>Alamat Penerima</th>
                </tr>
            </thead>
            <tbody>
                <tr class="border">
                    <td class="border" style="text-align: center;">
                        {!! nl2br(e($pengiriman->detail_location_from)) !!}
                    </td>
                    <td class="border" style="text-align: center;">
                        {!! nl2br(e($pengiriman->detail_location_to)) !!}
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered mb-0 mt-1">
            <thead>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-right pr-2" width='65%'>BERAT</th>
                    <th class="border text-right pr-1" width='35%'>{{ $pengiriman->weight }} TON</th>
                </tr>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-right pr-2" width='65%'>HARGA</th>
                    <th class="border text-right pr-1" width='35%'>Rp {{ number_format($pengiriman->price,2) }}</th>
                </tr>
                <tr class="border" style="background-color: whitesmoke; font-size: 14px;">
                    <th class="border text-right pr-2" width='65%'>TOTAL</th>
                    <th class="border text-right pr-1" width='35%'>Rp {{ number_format($pengiriman->total_transaksi,2) }}</th>
                </tr>
            </thead>
        </table>

        <table class="text-center mt-1" >
            <tr>
                <td width="30%"></td>
                <td width="5"></td>
                <td width="30%"></td>
                <td width="5"></td>
                <td width="30%"><b>Yang Mengetahui</b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td height="50px"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>(.......................................)</td>
            </tr>
        </table>
        
    </div>
</body>
</html>