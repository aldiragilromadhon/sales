@extends('layouts.app')
@include('penjualan.transaction')
@section('content')
<div class="content-wrapper pt-1">
    <div class="content-header row pr-2">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">DETAIL PENJUALAN</h3>
            {{-- <small>Divisi Operasi & Teknik</small> --}}
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('penjualan') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="lists">
            @yield('transaction')
        </section>
    </div>
</div>

@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('.input-images-1').imageUploader();
    });
</script>
@endsection