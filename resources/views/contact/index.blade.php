@extends('layouts.app')
@section('content')
<div class="content-wrapper pt-0">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12">
                        <h3 class="content-header-title">MASTER KONTAK</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="float-md-right">
                            <a href="{{ url('contact/new') }}" class="btn btn-info round box-shadow-2 px-2"><i class="ft-plus icon-left"></i> Tambah</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-danger ml-1 mb-1" id="buttonDelete"><i class="ft-close"></i> Delete</button>
                            <table class="table table-striped table-bordered selection-deletion-row font-small-3 table-xs" id="table-contact" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NO</th>
                                        <th>Nama</th>
                                        <th>Tipe</th>
                                        <th>Alamat</th>
                                        <th>Email</th>
                                        <th>No Handphone</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-contact">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-contact" validate>
                <div class="modal-header">
                    <h4 class="modal-title" id="title-form-contact">Tambah Data</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h5>Nama contact <span class="required">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="id_contact" id="id_contact" hidden>
                                    <input type="text" name="contact" id="contact" class="form-control" required data-validation-required-message="This field is required" placeholder="Ton">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    
    var table_contact;
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    function load_list_data(){
        table_contact = $('#table-contact').DataTable({
            responsive: true,
            autoWidth: true,
            processing: true,
            serverSide: true,
            ordering: false, 
            ajax: {
                url: "{{ url('datatable-contact') }}",
                headers: {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // setTimeout(function(){ location.reload(); }, 1000);
                }
            },
            columnDefs: [{ 
                targets: [ 0 ],
                visible: false
            }]
        });

        $('#table-contact tbody').on( 'dblclick', 'tr', function () {
            $('#table-contact tbody').removeClass('detail');
            $(this).addClass('detail');
            if(table_contact.rows('.detail').data().length > 0){
                location.replace("{{ url('contact/detail') }}/"+table_contact.row('.detail').data()[0]);
            }
            
        });

        $('#table-contact tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_contact.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });        
        
    }
    
    function reload_list_data(){
        table_contact.ajax.reload(null, false);
    }
    
    $('#button-tambah-contact').click(function () {
        $('#form-contact').trigger("reset");
        $("#form-contact")[0].reset();
        $('#title-form-contact').html('Tambah Data');
    });
    
    $("#form-contact").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-contact') }}",
            type: "POST",
            data: new FormData($('#form-contact')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-contact").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
            }
        });
        
        
    });
    
    function getData(id){
        $('#title-form-contact').html('Edit Data');
        $.ajax({
            url : "{{ url('get-contact') }}/"+id,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-contact').modal('show');
                    $('#id_contact').val(result.data.id);
                    $('#contact').val(result.data.contact);
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-contact') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    $('#buttonDelete').click( function () {
        if(table_contact.rows('.selected').data().length > 0){
            deleteData(table_contact.row('.selected').data()[0]);
        }
    });
    
</script>
@endsection
