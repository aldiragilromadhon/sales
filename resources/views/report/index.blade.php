@extends('layouts.app')
@section('content')
<div class="content-wrapper pt-0">
    <div class="card">
        <div class="card-content">
            <div class="card-body pb-1">
                <div class="content-header row">
                    <div class="content-header-left col-12">
                        <h3 class="content-header-title">LAPORAN</h3>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card-block">
                            <fieldset>
                                {{-- <input type="text" class="form-control" placeholder="Button on right" aria-describedby="button-addon2"> --}}
                                {{-- <select name="periode_laporan" id="periode_laporan" class="form-control js-example-events select2">
                                    @foreach($periode as $data)
                                    @if ($data->tanggal == now()->format('Y-m'))
                                    <option value="{{ $data->tanggal }}" selected>{{ $data->month }} {{ $data->year }}</option>
                                    @else
                                    <option value="{{ $data->tanggal }}">{{ $data->month }} {{ $data->year }}</option>
                                    @endif
                                    @endforeach
                                </select> --}}
                                <div class='input-group'>
                                    <input type='text' class="form-control daterange" name="periode_laporan" id="periode_laporan"/>
                                    <div class="input-group-append">
                                        <button class="btn btn-success" type="button" onclick="filter()">Cari!</button>
                                        {{-- <span class="input-group-text">
                                            <span class="la la-calendar"></span>
                                        </span> --}}
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">Laba Rugi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">Pembelian</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false">Penjualan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="base-tab4" data-toggle="tab" aria-controls="tab4" href="#tab4" aria-expanded="false">Pengiriman</a>
                                </li>
                            </ul>
                            <div class="tab-content pt-2">
                                <div class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card border-blue bg-transparent" style="display: none">
                                                <div class="card-header">
                                                    <h4 class="card-title">Saldo Awal [Rp 1.000.000.000]</h4>
                                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                    <div class="heading-elements">
                                                        <ul class="list-inline mb-0">
                                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-content" style="">
                                                    <div class="card-body">
                                                        <ul class="list-group">
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card border-blue bg-transparent" style="">
                                                <div class="card-header">
                                                    <h4 class="card-title">Pembelian <b id="total-pembelian">[Rp 0]</b></h4>
                                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                    <div class="heading-elements">
                                                        <ul class="list-inline mb-0">
                                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-content" style="">
                                                    <div class="card-body">
                                                        <ul class="list-group" id="detail-pembelian">
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card border-blue bg-transparent" style="">
                                                <div class="card-header">
                                                    <h4 class="card-title">Penjualan <b id="total-penjualan">[Rp 0]</b></h4>
                                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                    <div class="heading-elements">
                                                        <ul class="list-inline mb-0">
                                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-content" style="">
                                                    <div class="card-body">
                                                        <ul class="list-group" id="detail-penjualan">
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card border-blue bg-transparent" style="display: none">
                                                <div class="card-header">
                                                    <h4 class="card-title">Saldo Akhir <b>[Rp 0]</b></h4>
                                                    <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                                                    <div class="heading-elements">
                                                        <ul class="list-inline mb-0">
                                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-content" style="">
                                                    <div class="card-body">
                                                        <ul class="list-group">
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card border-blue bg-transparent">
                                                <div class="card-header">
                                                    <h4 class="card-title">Pengiriman <b id="pengiriman">[Rp 0]</b></h4>
                                                </div>
                                            </div>
                                            <div class="card border-blue bg-transparent">
                                                <div class="card-header">
                                                    <h4 class="card-title">LABA / RUGI <b id="laba-rugi">[Rp 0]</b></h4>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                                    {{-- <div class="table-responsive"> --}}
                                        <table class="table table-striped table-bordered table-xs" id="table-pembelian" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Tanggal</th>
                                                    <th>No Transaksi</th>
                                                    <th>Nama</th>
                                                    <th>Status hari ini</th>
                                                    <th>Memo</th>
                                                    <th>Total</th>
                                                    <th>Sisa Tagihan</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        {{-- </div> --}}
                                    </div>
                                    <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                                        {{-- <div class="table-responsive"> --}}
                                            <table class="table table-striped table-bordered table-xs" id="table-penjualan" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Tanggal</th>
                                                        <th>No Transaksi</th>
                                                        <th>Nama</th>
                                                        <th>Status hari ini</th>
                                                        <th>Memo</th>
                                                        <th>Total</th>
                                                        <th>Sisa Tagihan</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            {{-- </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @endsection
        @section('script')
        <script type="text/javascript">
            
            var table_pembelian, table_penjualan;
            
            $(document).ready(function() {
                $('.daterange').daterangepicker({
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });
                load_list_laba_rugi();
                load_list_pembelian();
                load_list_penjualan();
                $('.buttons-page-length, .buttons-copy, .buttons-print, .buttons-excel').addClass('btn btn-success btn-md mr-1');
            });
            
            function filter(){
                load_list_laba_rugi();
                reload_list_pembelian();
                reload_list_penjualan();
            }
            
            function load_list_laba_rugi(){
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    url: "{{ url('list-laba-rugi') }}",
                    data: { tanggal : $('#periode_laporan').val() },
                    type: "POST",
                    success: function (data, status, xhr) {
                        if (data) {
                            $('#detail-pembelian').html(data.pembelian);
                            $('#total-pembelian').html('[Rp '+data.total_pembelian+' ]');
                            $('#detail-penjualan').html(data.penjualan);
                            $('#total-penjualan').html('[Rp '+data.total_penjualan+' ]');
                            $('#pengiriman').html('[Rp '+data.pengiriman+' ]');
                            $('#laba-rugi').html('[Rp '+data.laba+' ]');
                        }
                    }
                });
            }
            
            function load_list_pembelian(){
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
                table_pembelian = $('#table-pembelian').DataTable({ 
                    "responsive": true,
                    "processing": true,
                    // "scrollX": true,
                    "dom": 'Bfrtip',
                    "className": 'my-1',
                    "lengthMenu": [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                    ],
                    "buttons": [
                    'pageLength', 'copy', 'excel',  'print'
                    ],
                    "ajax": {
                        "url": "{{ url('list-laporan-pembelian') }}",
                        "type": "POST",
                        "data": function ( d ) {
                            d.tanggal = $('#periode_laporan').val();
                        },
                        "headers": {
                            'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                        },
                        "error": function (jqXHR, textStatus, errorThrown) {
                            var err = JSON.parse(jqXHR.responseText);
                        }
                    },
                });
            }
            
            function reload_list_pembelian(){
                table_pembelian.ajax.reload(null, false);
            }
            
            function load_list_penjualan(){
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
                table_penjualan = $('#table-penjualan').DataTable({ 
                    "responsive": true,
                    "processing": true,
                    // "scrollX": true,
                    "dom": 'Bfrtip',
                    "className": 'my-1',
                    "lengthMenu": [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                    ],
                    "buttons": [
                    'pageLength', 'copy', 'excel',  'print'
                    ],
                    "ajax": {
                        "url": "{{ url('list-laporan-penjualan') }}",
                        "type": "POST",
                        "data": function ( d ) {
                            d.tanggal = $('#periode_laporan').val();
                        },
                        "headers": {
                            'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                        },
                        "error": function (jqXHR, textStatus, errorThrown) {
                            var err = JSON.parse(jqXHR.responseText);
                        }
                    },
                });
            }
            
            function reload_list_penjualan(){
                table_penjualan.ajax.reload(null, false);
            }
            
        </script>
        @endsection
        