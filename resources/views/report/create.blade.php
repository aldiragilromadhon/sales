@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <small>Produk</small>
            <h3 class="content-header-title">Buat Kontak</h3>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('contact') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row justify-content-md-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-blue white pb-1 pt-1">
                        <h3 class="font-medium-3 text-bold-700 white">Formulir Pengisian Data</h3>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <form class="form" method="POST" action="{{ url('store-contact') }}" >
                                        @csrf
                                        <div class="form-body">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-3">Nama Panggilan <span class="red">*</span></label>
                                                    <div class="col-lg-9">
                                                        <div class="controls">
                                                            @if ($contact)
                                                            <input class="form-control" type="text" id="contactname" name="contactname" value="{{ old('contactname',$contact->name) }}"/>
                                                            @else
                                                            <input class="form-control" type="text" id="contactname" name="contactname" value="{{ old('contactname') }}"/>
                                                            @endif
                                                            @error('contactname') <p class="red"> {{ $message }}</p> @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-3">Tipe Kontak <span class="red">*</span></label>
                                                    <div class="col-lg-9">
                                                        <div class="controls">
                                                            {{-- @if ($contact) --}}
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label>
                                                                        <input type="checkbox" id="contacttipepelanggan" name="contacttipe[]" value="Pelanggan" @if(is_array(old('contacttipe')) && in_array(1, old('contacttipe'))) checked @endif> Pelanggan
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label>
                                                                        <input type="checkbox" id="contacttipecustomer" name="contacttipe[]" value="Supplier" @if(is_array(old('contacttipe')) && in_array(2, old('contacttipe'))) checked @endif> Supplier
                                                                    </label>
                                                                    
                                                                </div>
                                                            </div>
                                                            {{-- <input class="form-control" type="text" id="contactcode" name="contactcode" value="{{ old('contactcode',$contact->code) }}"/> --}}
                                                            {{-- @else --}}
                                                            {{-- <input class="form-control" type="text" id="contactcode" name="contactcode" value="{{ old('contactcode') }}"/> --}}
                                                            {{-- @endif --}}
                                                            @error('contacttipe') <p class="red"> {{ $message }}</p> @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-3">Nama Perusahaan</label>
                                                    <div class="col-lg-9">
                                                        <div class="controls">
                                                            @if ($contact)
                                                            <input class="form-control" type="text" id="contactcompany" name="contactcompany" value="{{ old('contactcompany',$contact->perusahaan) }}"/>
                                                            @else
                                                            <input class="form-control" type="text" id="contactcompany" name="contactcompany" value="{{ old('contactcompany') }}"/>
                                                            @endif
                                                            @error('contactcompany') <p class="red"> {{ $message }}</p> @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-3">Email</label>
                                                    <div class="col-lg-9">
                                                        <div class="controls">
                                                            @if ($contact)
                                                            <input class="form-control" type="text" id="contactemail" name="contactemail" value="{{ old('contactemail',$contact->email) }}"/>
                                                            @else
                                                            <input class="form-control" type="text" id="contactemail" name="contactemail" value="{{ old('contactemail') }}"/>
                                                            @endif
                                                            @error('contactemail') <p class="red"> {{ $message }}</p> @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-3">Telepon</label>
                                                    <div class="col-lg-9">
                                                        <div class="controls">
                                                            @if ($contact)
                                                            <input class="form-control" type="text" id="contacttelepon" name="contacttelepon" value="{{ old('contacttelepon',$contact->phone) }}"/>
                                                            @else
                                                            <input class="form-control" type="text" id="contacttelepon" name="contacttelepon" value="{{ old('contacttelepon') }}"/>
                                                            @endif
                                                            @error('contacttelepon') <p class="red"> {{ $message }}</p> @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-lg-3">Alamat</label>
                                                    <div class="col-lg-9">
                                                        <div class="controls">
                                                            <textarea 
                                                            id="contactalamat" 
                                                            name="contactalamat" 
                                                            rows="3" 
                                                            class="form-control @error('contactalamat') border-danger @enderror"
                                                            >@if ($contact){{ old('contactalamat',$contact->alamat) }}@else{{ old('contactalamat') }}@endif</textarea>
                                                            @error('contactalamat') <p class="red"> {{ $message }}</p> @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions right">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('vendors/js/forms/validation/jqBootstrapValidation.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    
    @if ($contact)
    $('#lokasi_id').val("{{old('lokasi_id',$contact->lokasi_id)}}").change();
    @else
    $('#lokasi_id').val("{{old('lokasi_id')}}").change();
    @endif
    
    @if ($contact)
    $('#atasan_id').val("{{old('atasan_id',$contact->atasan_id)}}").change();
    @else
    $('#atasan_id').val("{{old('atasan_id')}}").change();
    @endif
    
    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();    
</script>
@endsection
