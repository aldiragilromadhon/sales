@extends('layouts.app')
@section('content')
<div class="content-wrapper pt-0">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-12">
                        <h3 class="content-header-title">MASTER UNIT</h3>
                    </div>
                    <div class="content-header-right col-md-6 col-12">
                        <div class="float-md-right">
                            <button type="button" id="button-tambah-unit" name="button-tambah-unit" class="btn btn-info round box-shadow-2 px-2" data-toggle="modal" data-target="#modal-unit"><i class="ft-plus icon-left"></i> Tambah</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <button class="btn btn-warning ml-1 mb-1" id="buttonEdit"><i class="ft-edit"></i> Edit</button>
                            <button class="btn btn-danger ml-1 mb-1" id="buttonDelete"><i class="ft-close"></i> Delete</button>
                            <table class="table table-striped table-bordered selection-deletion-row display nowrap table-xs" id="table-unit" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>No.</th>
                                        <th>Nama</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-unit" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-unit" enctype="multipart/form-data" validate>
                <div class="modal-header bg-success white">
                    <h4 class="modal-title white" id="title-form-unit">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert-form"></div>
                            <div class="form-group">
                                <h5>Nama Unit <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="id_unit" id="id_unit" hidden>
                                    <input type="text" name="nama_unit" id="nama_unit" class="form-control" required data-validation-required-message="This field is required" placeholder="BALL">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    
    var table_unit;
    var default_image = 'app-assets/images/gallery/1.jpg'; 
    
    $(document).ready(function() {
        setTimeout(function () {
            load_list_data();
        }, 500);
    });
    
    function load_list_data(){
        table_unit = $('#table-unit').DataTable({
            processing: true,
            serverSide: true,
            ordering: false, 
            ajax: {
                url: "{{ url('datatable-unit') }}",
                headers: {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    setTimeout(function(){ unit.reload(); }, 1000);
                }
            },
            columnDefs: [{ 
                targets: [ 0 ],
                visible: false
            }],
        });

        // $('#table-unit tbody').on( 'dblclick', 'tr', function () {
        //     $('#table-unit tbody').removeClass('detail');
        //     $(this).addClass('detail');
        //     if(table_unit.rows('.detail').data().length > 0){
        //         getData(table_unit.row('.detail').data()[0]);
        //     }
            
        // });
        
        $('#table-unit tbody').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table_unit.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });        
    }
    
    $('#buttonEdit').click( function () {
        if(table_unit.rows('.selected').data().length > 0){
            getData(table_unit.row('.selected').data()[0]);
        }
    });

    $('#buttonDelete').click( function () {
        if(table_unit.rows('.selected').data().length > 0){
            deleteData(table_unit.row('.selected').data()[0]);
        }
    });
    
    function reload_list_data(){
        table_unit.ajax.reload(null, false);
    }
    
    $('#button-tambah-unit').click(function () {
        $("#form-unit")[0].reset();
        $('#preview-file-user').attr('src', default_image);
    });
    
    $("#form-unit").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
            url: "{{ url('store-unit') }}",
            type: "POST",
            data: new FormData($('#form-unit')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-unit").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
            }
        });
        
        
    });
    
    function getData(id){
        $('#title-form-unit').html('Edit Data');
        $.ajax({
            url : "{{ url('get-unit') }}/"+id,
            dataType: "JSON",
            success: function(result) {
                if (result.status) {
                    $('#modal-unit').modal('show');
                    $('#id_unit').val(result.data.id);
                    $('#nama_unit').val(result.data.name);
                }else{
                    swal("Info!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-unit') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal("Batal!", "Your data is safe", "info");
            } 
        });
    }
    
    
</script>
@endsection
