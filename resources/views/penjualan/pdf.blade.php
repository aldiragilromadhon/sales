<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
</head>
<style>
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        color: black;
    }
    body{
        font-size: 12px;
        font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
        background-color: white;
        color: black;
    }
    table{
        width: 100%;
    }
    .table-bordered, .border{
        border: 1px solid black;
    }
    .table th, .table td {
        padding: 0.25rem;
    }
    .border .border-dark{
        color: black;
        border: 1px solid black;
    }
    footer {
        position: fixed; 
        bottom: -60px; 
        left: 20px; 
        right: 0px;
        height: 50px; 
        
        color: black;
        text-align: left;
    }
</style>
<body>
    <div class="ml-1 mr-1 p-1 border border-dark" style="color: black;border: 1px solid black;">
        <table>
            <tr>
                <td class="text-center" width="30%"><img src="{{public_path('images/'.$settings['2']['value'])}}" class="text-center" alt="company logo" style="width: 80%"></td>
                <td width="50%" class="text-left">
                    <h4 class="m-0 p-0">{{ $settings['1']['value'] }}</h4>
                    <p class="m-0 p-0">email : cvbintangalkacasejahtera@gmail.com</p>
                    <p class="m-0 p-0">Jl Dr Cipto Perumahan BTN Blok L/11</p>
                    <p class="m-0 p-0">Kab Sumenep - Provinsi Jawa timur</p>
                </td>
                <td class="text-center" width="20%" style="vertical-align: top;">
                    <table hidden>
                        <tr>
                            <td class="right" style="text-align: center; font-weight: bold;" width="100%">
                                <table class="m-0 p-0">
                                    <tr>
                                        <td>Tgl Cetak</td>
                                        <td width="1%">:</td>
                                        <td>{{ date('d F Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td width="1%">:</td>
                                        <td>{!! nl2br(e($penjualan->alamat)) !!}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        <hr/>
        <table>
            <tr style="text-align: center">
                <td class="text-center">
                    <h4 class="m-0"><u>INVOICE</u></h4>
                    <h5 class="m-0 p-0">{{ $penjualan->no_transaksi }}</h5>
                </td>
            </tr>
        </table>
        <table class="mt-1">
            <tr>
                <td style="text-align: left;" width="65%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="19%"><b>Pelanggan</b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $penjualan->contact->nama }} / {{ $penjualan->contact->perusahaan }}</td>
                        </tr>
                        <tr>
                            <td width="19%" style="vertical-align: top;"><b>Alamat</b></td>
                            <td width="1%" style="vertical-align: top;">:</td>
                            <td width="80%">{!! nl2br(e($penjualan->alamat)) !!}</td>
                        </tr>
                    </table>
                </td>
                <td class="right" style="text-align: right; vertical-align: top;" width="35%">
                    <table class="m-0 p-0">
                        <tr>
                            <td><b>Tanggal</b></td>
                            <td width="1%">:</td>
                            <td style="text-align: right">{{ date('d F Y',strtotime($penjualan->tgl_transaksi)) }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="text-align: left;" width="65%">
                    <table class="m-0 p-0">
                        <tr>
                            <td width="19%"><b>No. PO</b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $penjualan->no_referensi }}</td>
                        </tr>
                        @if ($pengiriman)
                        <tr>
                            <td width="19%"><b>No. DO</b></td>
                            <td width="1%">:</td>
                            <td width="80%">{{ $penjualan->no_transaksi }}</td>
                        </tr>
                        @endif
                    </table>
                </td>
                <td class="right" style="text-align: right; vertical-align: top;" width="35%">
                    <table class="m-0 p-0">
                        <tr>
                            <td><b>Tanggal PO</b></td>
                            <td width="1%">:</td>
                            <td style="text-align: right">{{ date('d F Y',strtotime($penjualan->tgl_po)) }}</td>
                        </tr>
                        @if ($pengiriman)
                        <tr>
                            <td><b>Tanggal DO</b></td>
                            <td width="1%">:</td>
                            <td style="text-align: right">{{ date('d F Y',strtotime($penjualan->tgl_transaksi)) }}</td>
                        </tr>
                        @endif
                    </table>
                </td>
            </tr>
        </table>        
        @php
        $no = 1;
        @endphp
        <table class="table mt-2 mb-2">
            <thead>
                <tr class="border" style="background-color: whitesmoke;">
                    <th class="border text-center" width='1%'>No.</th>
                    <th class="border text-center" width='33%'>Nama Barang</th>
                    <th class="border text-center" width='15%'>Volume</th>
                    <th class="border text-center" width='17%'>Harga</th>
                    <th class="border text-center" width='6%'>Diskon</th>
                    <th class="border text-center" width='6%'>Pajak</th>
                    <th class="border text-center" width='22%'>Jumlah Harga</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($penjualan->transaction_product as $item)
                <tr class="border">
                    <td class="border" style="text-align: center;">{{$no++}}</td>
                    <td class="border">{{$item->product->name}}</td>
                    <td class="border" style="text-align: center;">{{ number_format($item->quantity,3,',','.') }}</td>
                    <td class="border" style="text-align: right;">{{ number_format($item->price,0,',','.') }}</td>
                    <td class="border" style="text-align: center;">{{ $item->diskon +0 }}%</td>
                    <td class="border" style="text-align: center;">{{ $item->pajak +0 }}%</td>
                    <td class="border" style="text-align: right;">{{ number_format($item->total,0,',','.') }}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3" rowspan="{{ ($penjualan->uang_muka > 0 ? 6 : 4) }}" style="padding-right: 0px;">
                        No Rekening : <b>{{ $settings['4']['value'] }}</b><br/>
                        Nama Bank   : <b>{{ $settings['5']['value'] }}</b><br/>
                        Atas Nama   : <b>{{ $settings['6']['value'] }}</b><br/>
                        Senilai : <b>{{ $terbilang }} rupiah</b><br/>
                        Term Of Payment : <b>{{ date('d F Y',strtotime($penjualan->tgl_jatuh_tempo)) }}</b><br/>
                    </td>
                    <td style="text-align: left; border-style: none; padding-left: 50px;" colspan="3">TOTAL</td>
                    <td class="border" style="text-align: right;">
                        {{ number_format($penjualan->sisa_tagihan+$penjualan->uang_muka,0,',','.') }}
                    </td>
                </tr>
                @php
                $total_diskon = 0;
                foreach ($penjualan->transaction_product as $diskon) {
                    $total_diskon += ($diskon->diskon*$diskon->total)/100;
                }
                @endphp
                
                @if ($total_diskon > 0)
                <tr>
                    <td style="text-align: left; border-style: none; padding-left: 50px;" colspan="3">DISKON</td>
                    <td class="border" style="text-align: right;">
                        {{ number_format($total_diskon,0,',','.') }}
                    </td>
                </tr>
                @endif
                
                @php
                $total_pajak = 0;
                foreach ($penjualan->transaction_product as $pajak) {
                    $total_pajak += ($pajak->pajak*$pajak->total)/100;
                }
                @endphp
                @if ($total_pajak > 0)
                <tr>
                    <td style="text-align: left; border-style: none; padding-left: 50px;" colspan="3">PAJAK</td>
                    <td class="border" style="text-align: right;">
                        {{ number_format($total_pajak,0,',','.') }}
                    </td>
                </tr>
                @endif
                <tr>
                    <td style="text-align: left; border-style: none; padding-left: 50px;" colspan="3"><b>TOTAL JUMLAH</b></td>
                    <td style="text-align: right;" class="border">{{ number_format($penjualan->sisa_tagihan+$penjualan->uang_muka,0,',','.') }}</td>
                </tr>
                @if ($penjualan->uang_muka > 0)
                <tr>
                    <td style="text-align: left; border-style: none; padding-left: 50px;" colspan="3"><b>UANG MUKA</b></td>
                    <td style="text-align: right;" class="border">{{ number_format($penjualan->uang_muka,0,',','.') }}</td>
                </tr>
                <tr>
                    <td style="text-align: left; border-style: none; padding-left: 50px;" colspan="3"><b>SISA TAGIHAN</b></td>
                    <td style="text-align: right;" class="border">{{ number_format($penjualan->sisa_tagihan,0,',','.') }}</td>
                </tr>
                @endif
            </tfoot>
        </table>
        <table class="text-center mt-1" >
            <tr>
                <td width="30%"><b>Yang Menerima</b></td>
                <td width="5"></td>
                <td width="30%"></td>
                <td width="5"></td>
                <td width="30%"><b>Yang Mengetahui</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td height="50px"></td>
                </tr>
                <tr>
                    <td>(.......................................)</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>( {{ $penjualan->user }} )</td>
                </tr>
            </table>
        </div>
        
    </div>
    <p style="font-size: 10px; margin-left: 15px; "><i><b>Dokumen ini sah, diterbitkan oleh {{ $settings['1']['value'] }} secara elektronik melalui sistem dan tidak membutuhkan cap dan tandatangan basah.</b></i></p>             
    <p style="font-size: 8px; margin-left: 15px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p>
    {{-- <footer> --}}
        {{-- <p class="m-0 p-0" style="font-size: 8px;"><i><b>Dokumen ini dicetak pada : {{ date('Y-m-d H:i:s') }}</b></i></p> --}}
    {{-- </footer> --}}
</body>
</html>