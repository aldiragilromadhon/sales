@extends('layouts.app')
@section('style')
<style type="text/css">
    .remove-td td {
        padding: 0.5rem 0.5rem;
    }    
</style>
@endsection
@section('content')
<div class="content-wrapper pt-2">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title mt-1 m-0">
                Buat Penagihan Penjualan
            </h3>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <a href="{{ url('penjualan') }}" type="button" class="btn btn-info round box-shadow-2 px-2"><i class="ft-arrow-left icon-left"></i> Kembali</a>
            </div>
        </div>
    </div>
    <div class="content-body">
        @if (session('success'))
        <div class="alert alert-success mb-2" role="alert">
            {{ session('success') }}
        </div>
        @endif
        
        @if (session('error'))
        <div class="alert alert-warning mb-2" role="alert">
            {{ session('error') }}
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div id="alert-form-penjualan"></div>
                <div class="card">
                    <div class="card-content">
                        <form class="form form-horizontal" method="POST" validate id="form-penjualan">
                            @csrf
                            <div class="card-body p-1" style="background-color: #B1DCF7">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <h5>Pelanggan <span class="red">*</span></h5>
                                            <div class="controls">
                                                <select name="penjualancustomer" id="penjualancustomer" class="form-control js-example-events select2" required>
                                                    <option value="">Pilih Kontak</option>
                                                    @foreach($contact as $data)
                                                    <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <h5>No Ref / Purchase Order</h5>
                                        <input type="text" class="form-control" id="penjualannomorreferensi" name="penjualannomorreferensi" placeholder="">
                                    </div>
                                    <div class="col-lg-6 text-right center pt-2 pr-2">
                                        <p class="font-medium-3 text-bold-600" id="totalpenjualanatas">Total Rp. 0</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="card-body p-1">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Alamat Pelanggan</label>
                                                <textarea class="form-control" id="penjualanalamatsupplier" name="penjualanalamatsupplier" rows="3"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Alamat Pengiriman</label>
                                                <textarea class="form-control" id="penjualanalamatpengiriman" name="penjualanalamatpengiriman" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label>Tgl Purchase Order</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="penjualantanggalpo" name="penjualantanggalpo" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Tgl Transaksi</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="penjualantanggal" name="penjualantanggal" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Tgl Jatuh Tempo</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" id="penjualanjatuhtempo" name="penjualanjatuhtempo" value="{{ date('d-m-Y') }}" style="background-color: white;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label><b>No Pembelian</b></label>
                                                <select class="form-control select2" id="penjualanparent" name="penjualanparent" style="width: 100%">
                                                    <option value="">Pilih Pembelian</option>
                                                    @foreach($pembelian as $pemb)
                                                    <option value="{{ $pemb->no_transaksi }}">{{ $pemb->no_transaksi }} / {{ $pemb->no_referensi }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Mengetahui</label>
                                            <input type="text" class="form-control" id="penjualanuser" name="penjualanuser" required style="background-color: white;">
                                        </div>
                                        <div class="form-group">
                                            <label>Syarat Pembayaran</label>
                                            <div class="controls">
                                                <select class="form-control select2" id="penjualantipepembayaran" name="penjualantipepembayaran" style="width: 100%">
                                                    <option value="0" selected>Cash On Delivery</option>
                                                    <option value="15">Net 15</option>
                                                    <option value="30">Net 30</option>
                                                    <option value="60">Net 60</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3"  id="detail-contact">
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover remove-td " id="table-product-penjualan">
                                                <thead>
                                                    <tr>
                                                        <th width="30%">Produk</th>
                                                        <th width="15%">Kuantitas</th>
                                                        <th width="15%">Harga Satuan</th>
                                                        <th width="5%">Diskon</th>
                                                        <th width="5%">Pajak</th>
                                                        <th width="24%">Jumlah</th>
                                                        <th width="1%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="button" class="btn btn-info btn-min-width box-shadow-2 mt-1" id="button-add-row-product-penjualan"><i class="la la-plus-square"></i> Tambah Data</button>
                                        
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-lg-4">
                                        <fieldset class="form-group form-group-style">
                                            <label for="textbox2">Pesan</label>
                                            <textarea class="form-control" id="penjualanpesan" name="penjualanpesan" rows="3"></textarea>
                                        </fieldset>
                                        <fieldset class="form-group form-group-style">
                                            <label for="textbox2">Memo</label>
                                            <textarea class="form-control" id="penjualanmemo" name="penjualanmemo" rows="3"></textarea>
                                        </fieldset>
                                    </div>
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="table-responsive">
                                            <table class="table table-borderless mb-0 p-0 m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Sub Total</td>
                                                        <td class="type-info text-right">
                                                            <p id="totalpenjualansubtotal" class="m-0">Rp. 0</p>
                                                            <input type="text" id="hiddentotalpenjualansubtotal" name="penjualansubtotal" value="0" hidden>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Diskon per baris</td>
                                                        <td class="type-info text-right">
                                                            <p id="diskonpenjualanperbaris" class="m-0">Rp. 0</p>
                                                            <input type="text" id="hiddendiskonpenjualanperbaris" value="0" hidden>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pajak per baris</td>
                                                        <td class="type-info text-right">
                                                            <p id="pajakpenjualanperbaris" class="m-0">Rp. 0</p>
                                                            <input type="text" id="hiddenpajakpenjualanperbaris" value="0" hidden>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><p style="margin-bottom: 5px">Pemotongan / Diskon</p>
                                                            <input type="text" class="form-control input-sm" id="penjualanpemotongan" name="penjualanpemotongan" placeholder="" value="0">
                                                        </td>
                                                        <td class="type-info text-right pt-2" id="showpenjualanpemotongan">Rp. 0</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td class="type-info text-right"><p id="totalpenjualanbawah" class="m-0">Rp. 0</p></td>
                                                    </tr>
                                                    <tr>
                                                        <td><p style="margin-bottom: 5px">Uang Muka</p>
                                                            <input type="text" class="form-control input-sm" id="penjualanuangmuka" name="penjualanuangmuka" placeholder="" value="0">
                                                        </td>
                                                        <td class="type-info text-right pt-2" id="showpenjualanuangmuka">Rp. 0</td>
                                                    </tr>
                                                    <tr>
                                                        <td><h2 class="m-0">Sisa Tagihan</h2>
                                                            <input type="text" id="penjualansisatagihan" name="penjualansisatagihan" value="0" hidden>
                                                        </td>
                                                        <td class="type-info text-right"><h2 id="sisatagihanpenjualan" class="m-0">Rp. 0</h2></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions right">
                                    <button type="button" class="btn btn-warning mr-1"><i class="ft-x"></i> Cancel</button>
                                    <button type="submit" class="btn btn-primary" >Buat Penjualan</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var rowIdx = 0, sumdiskon = 0, sumpajak = 0, sumtotal = 0;
    $(document).ready(function () {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        addRowsTablePenjualan();
        $('#penjualantanggalpo, #penjualantanggal, #penjualanjatuhtempo').pickadate({
            format: 'dd-mm-yyyy',
            today: '',
            close: 'Close',
            clear: ''
        });
        if( /Android|iPhone|iPad|iPod|IEMobile/i.test(navigator.userAgent) ) {
            $('.select2').select2({
                dropdownAutoWidth : true,
                width: '100%'
            });
        }else{
            $('.select2').select2({
                dropdownAutoWidth : true,
            });
            setTimeout(function(){ $('#hidden-menu').click() }, 1000);
        }
    });
    
    $('#penjualancustomer').on('change', function() {
        if ($(this).val()) {
            $.ajax("{{ url('get-address-contact') }}/"+$(this).val(),{
                success: function (data, status, xhr) {
                    if (data) {
                        $('#penjualanalamatsupplier').val((data.alamat));
                        $('#penjualanalamatpengiriman').val((data.alamat));
                        $('#detail-contact').html('<div class="bs-callout-info callout-bordered callout-transparent p-1 mt-2">'+
                            '<h6 class="black text-bold-600 font-medium-2">'+data.nama+'</h6>'+
                            '<h6 class="black font-small-3 mb-1">'+data.perusahaan+'</h6>'+
                            '<p>'+nl2br(data.alamat)+'</p>'+'<p>'+(data.email?data.email:'')+'</p>'+'<p>'+(data.phone?data.phone:'')+'</p></div>'
                            );
                            
                        }
                    }
                });
            }
        });
        
        $('#button-add-row-product-penjualan').on('click', function () {
            addRowsTablePenjualan();
        });
        
        $('#table-product-penjualan tbody').on('click', '.remove', function () {
            var row = $(this).closest('tr');
            row.remove();
            sumdiskon = 0;
            $("#table-product-penjualan tbody input[name='producttotaldiskon[]']").map(function(){ return $(this).val();}).get().forEach(setDiskonPenjualan);
            sumpajak = 0;
            $("#table-product-penjualan tbody input[name='producttotalpajak[]']").map(function(){ return $(this).val();}).get().forEach(setPajakPenjualan);
            sumtotal = 0;
            $("#table-product-penjualan tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPenjualan);
            setGrandTotal();
        });
        
        function addRowsTablePenjualan() {
            if(!$('.check-text-isset').filter(function(){ return !this.value.trim(); }).length){
                $('#table-product-penjualan tbody').append(`
                <tr>
                    <td style="min-width: 300px;">
                        <select class="form-control select2 select2-table check-text-isset" id="productpenjualan" name="productpenjualan[]">
                            <option value=""><i>- Pilih Produk -</i></option>
                            @foreach($product as $p)
                            <option value="{{$p->id}}">{{$p->name}}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="text" class="form-control class-money-format class-quantity" id="productquantity" name="productquantity[]" value="1" style="min-width: 170px;">
                        <input type="text" id="productsisaquantity" name="productsisaquantity[]" value="0" hidden>
                        <input type="text" id="productbufferquantity" name="productbufferquantity[]" value="0" hidden>
                    </td>
                    <td>
                        <div class="input-group" style="min-width: 170px;">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input type="text" class="form-control text-right class-money-format class-quantity" id="productharga" name="productharga[]" value="0">
                        </div>
                    </td>
                    <td>
                        <div class="input-group" style="min-width: 170px;">
                            <input type="text" class="form-control text-right class-money-format" id="productdiskon" name="productdiskon[]" value="">
                            <input type="text" id="producttotaldiskon" name="producttotaldiskon[]" value="0" hidden>
                            <div class="input-group-prepend">
                                <span class="input-group-text" style="padding: 10px">%</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="input-group" style="min-width: 170px;">
                            <input type="text" class="form-control text-right class-money-format" id="productpajak" name="productpajak[]" value="">
                            <input type="text" id="producttotalpajak" name="producttotalpajak[]" value="0" hidden>
                            <div class="input-group-prepend">
                                <span class="input-group-text" style="padding: 10px">%</span>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="input-group" style="min-width: 170px;">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input type="text" class="form-control text-right" id="producttotal" name="producttotal[]" value="0">
                        </div>
                    </td>
                    <td><button type="button" class="btn btn-icon btn-pure danger remove"><i class="la la-close"></i></button></td>
                </tr>`);
                
                $('.select2-table').select2()
                .on("change", function(e) {
                    if ($(this).val() != '') {
                        $(this).closest('tr').find('#productharga').load("{{ url('sell-price-product') }}/"+$(this).val(),function(response, status, xhr){
                            var data_product = JSON.parse(response);
                            $(this).val(data_product.sell_price);
                            $(this).closest('tr').find('#productsisaquantity').val(data_product.balance);
                            $(this).closest('tr').find('#productbufferquantity').val(data_product.buffer);
                            if(data_product.balance < data_product.buffer){
                                swal("Peringatan!", "Stok kurang dari buffer!", "warning");
                            }
                            var harga = data_product.sell_price == 0 ? 1 : data_product.sell_price;
                            var quantity = $(this).closest('tr').find('#productquantity').val() == 0 ? [1,0] : $(this).closest('tr').find('#productquantity').val().toString().replace(/[^,\d]/g, "").split(",");
                            var total_temp = (harga * parseFloat(quantity[0]+'.'+quantity[1]));
                            var total = total_temp.toFixed(0).toString().split(".");
                            var diskon = $(this).closest('tr').find('#productdiskon').val();
                            if (diskon>0) {
                                $(this).closest('tr').find('#producttotaldiskon').val((total_temp*diskon/100).toFixed(0));
                            }else{
                                $(this).closest('tr').find('#producttotaldiskon').val('0');
                            }
                            
                            var total_diskon = $(this).closest('tr').find('#producttotaldiskon').val();
                            var pajak = $(this).closest('tr').find('#productpajak').val();
                            if (pajak>0) {
                                $(this).closest('tr').find('#producttotalpajak').val((total_temp*pajak/100).toFixed(0));
                            }else{
                                $(this).closest('tr').find('#producttotalpajak').val('0');
                            }
                            
                            $(this).closest('tr').find('#producttotal').val(formatRupiah(total[0]+','+total[1], ''));
                            $(this).closest('tr').find('#productharga').val(formatRupiah(harga, ''));
                            sumdiskon = 0;
                            $("#table-product-penjualan tbody input[name='producttotaldiskon[]']").map(function(){ return $(this).val();}).get().forEach(setDiskonPenjualan);
                            sumpajak = 0;
                            $("#table-product-penjualan tbody input[name='producttotalpajak[]']").map(function(){ return $(this).val();}).get().forEach(setPajakPenjualan);
                            sumtotal = 0;
                            $("#table-product-penjualan tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPenjualan);
                            setGrandTotal();
                        });
                    }                
                });
                
                $('.class-money-format').on("keyup", function(e) {
                    if ($(this).val() != '') {
                        var harga = $(this).closest('tr').find('#productharga').val() == 0 ? 1 : $(this).closest('tr').find('#productharga').val().toString().replace(/[^,\d]/g, "");
                        var quantity = $(this).closest('tr').find('#productquantity').val() == 0 ? '1,00' : $(this).closest('tr').find('#productquantity').val().toString().replace(/[^,\d]/g, "").split(",");
                        var sisastok = $(this).closest('tr').find('#productsisaquantity').val();
                        var bufferstok = $(this).closest('tr').find('#productbufferquantity').val();
                        if(parseFloat(sisastok) < parseFloat(quantity)){
                            swal("Peringatan!", "Stok tidak tersedia dengan kuantitas tersebut!", "warning");
                            quantity = sisastok <= 1 ? '0,00' : '1,00';
                            $(this).closest('tr').find('#productquantity').val(quantity);
                        }
                        if(parseFloat(sisastok) < (parseFloat(bufferstok)+parseFloat(quantity))){
                            swal("Peringatan!", "Stok kurang dari buffer!", "warning");
                        }
                        
                        var total_temp = harga * parseFloat(quantity[0]+'.'+quantity[1]);
                        var total = total_temp.toFixed(0).toString().split(".");
                        
                        var diskon = $(this).closest('tr').find('#productdiskon').val();
                        if (diskon>0) {
                            $(this).closest('tr').find('#producttotaldiskon').val((total_temp*diskon/100).toFixed(0));
                        }else{
                            $(this).closest('tr').find('#producttotaldiskon').val('0');
                        }
                        
                        var total_diskon = $(this).closest('tr').find('#producttotaldiskon').val();
                        var pajak = $(this).closest('tr').find('#productpajak').val();
                        if (pajak>0) {
                            $(this).closest('tr').find('#producttotalpajak').val(((total_temp-total_diskon)*pajak/100).toFixed(0));
                        }else{
                            $(this).closest('tr').find('#producttotalpajak').val('0');
                        }
                        
                        $(this).closest('tr').find('#producttotal').val(formatRupiah(total[0]+','+total[1], ''));
                        $(this).closest('tr').find('#productharga').val(formatRupiah(harga, ''));
                        sumdiskon = 0;
                        $("#table-product-penjualan tbody input[name='producttotaldiskon[]']").map(function(){ return $(this).val();}).get().forEach(setDiskonPenjualan);
                        sumpajak = 0;
                        $("#table-product-penjualan tbody input[name='producttotalpajak[]']").map(function(){ return $(this).val();}).get().forEach(setPajakPenjualan);
                        sumtotal = 0;
                        $("#table-product-penjualan tbody input[name='producttotal[]']").map(function(){ return $(this).val();}).get().forEach(setTotalPenjualan);
                        setGrandTotal();
                    }
                });
                
                $('.class-quantity').focus(function(e) {
                    if ($(this).val() == 1) {
                        $(this).val('');
                    }
                });
                $('.class-quantity').focusout(function(e){
                    if ($(this).val() == '') {
                        $(this).val('1');
                    }
                });
                
                ;
            }    
        }
        
        $('#penjualanpemotongan, #penjualanuangmuka').on("keyup", function(e) {
            setGrandTotal();
            $('#showpenjualanpemotongan').html(formatRupiah(parseFloat($('#penjualanpemotongan').val()).toFixed(0).toString().split("."), 'Rp '));
            $('#showpenjualanuangmuka').html(formatRupiah(parseFloat($('#penjualanuangmuka').val()).toFixed(0).toString().split("."), 'Rp '));
        });
        
        
        
        function setDiskonPenjualan(diskon) {
            sumdiskon += parseFloat(diskon);
            var temp_sum_diskon = sumdiskon.toFixed(0).toString().split(".");
            $('#diskonpenjualanperbaris').html(formatRupiah(temp_sum_diskon[0]+','+temp_sum_diskon[1], 'Rp '));
            $('#hiddendiskonpenjualanperbaris').val(sumdiskon);
        }
        
        function setPajakPenjualan(pajak) {
            sumpajak += parseFloat(pajak);
            var temp_sum_pajak = sumpajak.toFixed(0).toString().split(".");
            $('#pajakpenjualanperbaris').html(formatRupiah(temp_sum_pajak[0]+','+temp_sum_pajak[1], 'Rp '));
            $('#hiddenpajakpenjualanperbaris').val(sumpajak);
        }
        
        function setTotalPenjualan(total) {
            temp_total = total.toString().replace(/[^,\d]/g, "").split(",");
            sumtotal += parseFloat(temp_total[0]+'.'+temp_total[1]);
            var temp_sum = sumtotal.toFixed(0).toString().split(".");
            $('#totalpenjualanatas, #totalpenjualansubtotal').html(formatRupiah(temp_sum[0]+','+temp_sum[1], 'Rp '));
            $('#hiddentotalpenjualansubtotal').val(sumtotal);
        }
        
        function setGrandTotal(){
            var pemotong = $('#penjualanpemotongan').val() == '' ? 0 : $('#penjualanpemotongan').val();
            var grand_total_temp = parseFloat(parseFloat($('#hiddentotalpenjualansubtotal').val()-$('#hiddendiskonpenjualanperbaris').val())+parseFloat($('#hiddenpajakpenjualanperbaris').val())-parseFloat(pemotong));
            var grand_total = grand_total_temp.toFixed(0).toString().split(".");
            $('#totalpenjualanbawah').html(formatRupiah(grand_total[0]+','+grand_total[1], 'Rp '));
            var uangmuka = $('#penjualanuangmuka').val() == '' ? 0 : $('#penjualanuangmuka').val();
            var sisa_uangmuka_temp = parseFloat(grand_total_temp-uangmuka);
            var sisa_uangmuka = sisa_uangmuka_temp.toFixed(0).toString().split(".");
            $('#sisatagihanpenjualan').html(formatRupiah(sisa_uangmuka[0]+','+sisa_uangmuka[1], 'Rp '));
            $('#penjualansisatagihan').val(sisa_uangmuka_temp);
            
        }
        
        $( "#penjualanuangmuka, #penjualanpemotongan" ).focus(function(e) {
            if ($(this).val() == 0) {
                $(this).val('');
            }
        });
        $( "#penjualanuangmuka, #penjualanpemotongan" ).focusout(function(e){
            if ($(this).val() == '') {
                $(this).val('0');
            }
        });
        
        function formatRupiah(angka, prefix) {
            var number_string = angka.toString().replace(/[^,\d]/g, ""),
            split = number_string.split(","),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);
            if (ribuan) {
                separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }
            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
        }
        
        $('#penjualantipepembayaran, #penjualantanggal').change( function(e) { 
            var temp_date = $("#penjualantanggal").val().split('-');
            console.log(temp_date[2]+'-'+temp_date[1]+'-'+temp_date[0]);
            var date = new Date(temp_date[2]+'-'+temp_date[1]+'-'+temp_date[0]);
            date.setDate(date.getDate() + parseInt($('#penjualantipepembayaran').val()));
            $("#penjualanjatuhtempo").val(date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
        });
        
        
        $("#form-penjualan").submit(function(e){
            e.preventDefault();
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                url: "{{ url('store-penjualan') }}",
                type: "POST",
                data: new FormData($('#form-penjualan')[0]),
                processData: false,
                contentType: false,
                dataType: "JSON",
                success: function(result) {
                    swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                    // window.open("{{ url('penjualan-inv') }}/"+result.id);
                    window.location = "{{ url('penjualan') }}";
                },
                error: function (jqXHR, textStatus, errorThrown ){
                    var err = JSON.parse(jqXHR.responseText);
                    swal("ERROR!", err.message, "error");
                    $.each(err.errors, function (key, value) {
                        $('#alert-form-penjualan').html('<div class="alert alert-danger mb-2" role="alert">'+key+' : '+value+'</div>');
                    });
                }
            });
        });
        
    </script>
    @endsection
    