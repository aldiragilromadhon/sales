
@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-1">
            <h3 class="content-header-title">MASTER USER</h3>
        </div>
        <div class="content-header-right col-md-6 col-12 mb-1">
            <div class="float-md-right">
                <button type="button" id="button-tambah-user" name="button-tambah-user" class="btn btn-info round box-shadow-2 px-2" data-toggle="modal" data-target="#modal-user"><i class="ft-plus icon-left"></i> Tambah</button>
            </div>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <table class="table table-striped table-bordered table-sm" id="table-user" style="width: 100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th class="text-center">~</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modal-user" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" id="form-user" enctype="multipart/form-data" validate>
                <div class="modal-header bg-success white">
                    <h4 class="modal-title white" id="title-form-user">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="alert-form"></div>
                            <div class="form-group">
                                <h5>Nama <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="name_user" id="name_user" class="form-control" required data-validation-required-message="This field is required" placeholder="Nama Lengkap" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Email <span class="red">*</span></h5>
                                <div class="controls">
                                    <input type="email" name="email_user" id="email_user" class="form-control" required data-validation-required-message="This field is required" placeholder="admin@info.com" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>Tipe <span class="red">*</span></h5>
                                <select class="form-control" name="tipe_user" id="tipe_user" required>
                                    <option value="2" selected>Sales</option>
                                    <option value="1">Admin</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    
    var tabel_Kategori;
    var default_image = 'images/gallery/1.jpg'; 
    
    $(document).ready(function() {
        $('.select2').select2();
        setTimeout(function () {
            load_list_data();
        }, 500);
        
        $("#file_user").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview-file-user').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
    
    function load_list_data(){
        tabel_Kategori = $('#table-user').DataTable({ 
            "responsive": true,
            "processing": true,
            "ajax": {
                "url": "{{ route('datatable-user') }}",
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN':jQuery('meta[name="csrf-token"]').attr('content')
                },
                "error": function (jqXHR, textStatus, errorThrown) {
                    // setTimeout(function(){ location.reload(); }, 1000);
                }
            },
            "columnDefs": [{ 
                "targets": [ -1 ],
                "orderable": false
            }]
        });
    }
    
    function reload_list_data(){
        tabel_Kategori.ajax.reload(null, false);
    }
    
    $('#button-tambah-user').click(function () {
        $('#jabatan_id').val('').change();
        $('#atasan_id').val('0').change();
        $("#form-user")[0].reset();
        $('#title-form-user').html('Tambah Data');
        $('#preview-file-user').attr('src', default_image);
        $('#file_user').val('');
    });
    
    $("#form-user").submit(function(e){
        e.preventDefault();
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')}});
        if ($('#id_user').val()) {
            var link_url = "{{ route('update.user') }}";
        }else{
            var link_url = "{{ route('store.user') }}";
        }
        $.ajax({
            url: link_url,
            type: "POST",
            data: new FormData($('#form-user')[0]),
            processData: false,
            contentType: false,
            dataType: "JSON",
            success: function(result) {
                swal("Sukses!", "Data berhasil di"+result.message+"!", "success");
                $("#modal-user").modal('hide');
                reload_list_data();
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("INFO!", err.message, "warning");
                $.each(err.errors, function (key, value) {
                    $('#alert-form').html('<div class="alert alert-danger mb-2" role="alert">'+key+' : '+value+'</div>');
                });
            }
        });
    });
    
    function getData(id){
        $('#title-form-user').html('Edit Data');
        $.ajax({
            url : "{{ url('get-user') }}/"+id,
            dataType: "JSON",
            success: function(data) {
                if (data) {
                    $('#modal-user').modal('show');
                    $('#id_user').val(data.id);
                    $('#id_emp').val(data.emp.id);
                    $('#role_user').val(data.role).change();
                    $('#jabatan_id').val(data.emp.jabatan_id).change();
                    $('#atasan_id').val(data.emp.atasan_id).change();
                    $('#nip_user').val(data.emp.nip);
                    $('#alias_user').val(data.emp.alias);
                    $('#name_user').val(data.emp.name);
                    $('#phone_user').val(data.emp.phone_no);
                    $('#email_user').val(data.emp.email);
                    if (data.emp.file) {
                        $('#preview-file-user').attr('src', "{{ asset('images/uploads') }}/"+data.emp.file);
                    }else{
                        $('#preview-file-user').attr('src', default_image);
                    }
                }else{
                    swal("Gagal!", "Data gagal di ambil!.", "warning");
                }
            },
            error: function (jqXHR, textStatus, errorThrown ){
                var err = JSON.parse(jqXHR.responseText);
                swal("ERROR!", err.message, "error");
            }
        });
    }
    
    function deleteData(id){
        swal({
            title: "Are you sure?",
            text: "Anda akan menghapus data!",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "Batal!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm: {
                    text: "Hapus",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                $.ajax({
                    url : "{{ url('delete-user') }}/"+id,
                    success: function(data) {
                        if (data.message) {
                            swal("Sukses!", "Data telah terhapus!.", "success");
                        }else{
                            swal("Gagal!", "Data gagal dihapus!.", "warning");
                        }
                        reload_list_data();
                    },
                    error: function (jqXHR, textStatus, errorThrown ){
                        var err = JSON.parse(jqXHR.responseText);
                        swal("ERROR!", err.message, "error");
                    }
                });
            } else {
                swal({ title: "Batal!", text: "Anda batal menghapus data!.", timer: 3000, showConfirmButton: false });
            } 
        });
    }
    
    
</script>
@endsection
