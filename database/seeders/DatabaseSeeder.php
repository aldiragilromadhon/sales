<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Menu;
use App\Models\Setting;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
    * Seed the application's database.
    *
    * @return void
    */
    public function run()
    {
        User::create([
            'name' => 'RONALD CHERNENKO',
            'email' => 'chernenkoronald@gmail.com',
            'password' => bcrypt('admin')
        ]);
        
        Menu::create([
            'id' => 1,
            'modul_id' => '0',
            'parent_id' => '0',
            'menu' => 'Dashboard',
            'target' => 'home',
            'icon' => 'la la-tachometer',
            'urutan' => '1',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 2,
            'modul_id' => '0',
            'parent_id' => '0',
            'menu' => 'Laporan',
            'target' => 'report',
            'icon' => 'la la-file-archive-o',
            'urutan' => '2',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 3,
            'modul_id' => '1',
            'parent_id' => '0',
            'menu' => 'Pembelian',
            'target' => 'pembelian',
            'icon' => 'la la-shopping-cart',
            'urutan' => '3',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 4,
            'modul_id' => '1',
            'parent_id' => '0',
            'menu' => 'Penjualan',
            'target' => 'penjualan',
            'icon' => 'la la-tags',
            'urutan' => '4',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 5,
            'modul_id' => '1',
            'parent_id' => '0',
            'menu' => 'Pengiriman',
            'target' => 'pengiriman',
            'icon' => 'la la-truck',
            'urutan' => '5',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 6,
            'modul_id' => '2',
            'parent_id' => '0',
            'menu' => 'Lokasi',
            'target' => 'location',
            'icon' => 'la la-home',
            'urutan' => '6',
            'status' => '0',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 7,
            'modul_id' => '2',
            'parent_id' => '0',
            'menu' => 'Mapping',
            'target' => 'location-price',
            'icon' => 'la la-map',
            'urutan' => '7',
            'status' => '0',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 8,
            'modul_id' => '2',
            'parent_id' => '0',
            'menu' => 'Kontak',
            'target' => 'contact',
            'icon' => 'la la-user-plus',
            'urutan' => '8',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 9,
            'modul_id' => '2',
            'parent_id' => '0',
            'menu' => 'Produk',
            'target' => 'product',
            'icon' => 'la la-archive',
            'urutan' => '9',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 10,
            'modul_id' => '2',
            'parent_id' => '0',
            'menu' => 'Satuan',
            'target' => 'unit',
            'icon' => 'la la-file',
            'urutan' => '10',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 11,
            'modul_id' => '2',
            'parent_id' => '0',
            'menu' => 'Kategori',
            'target' => 'category',
            'icon' => 'la la-thumb-tack',
            'urutan' => '11',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 12,
            'modul_id' => '2',
            'parent_id' => '0',
            'menu' => 'Truck',
            'target' => 'renttruck',
            'icon' => 'la la-list',
            'urutan' => '12',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Menu::create([
            'id' => 13,
            'modul_id' => '3',
            'parent_id' => '0',
            'menu' => 'Setting',
            'target' => 'setting',
            'icon' => 'la la-gear',
            'urutan' => '13',
            'status' => '1',
            'create_by' => '1'
        ]);

        Menu::create([
            'id' => 14,
            'modul_id' => '3',
            'parent_id' => '0',
            'menu' => 'User',
            'target' => 'user',
            'icon' => 'la la-user',
            'urutan' => '14',
            'status' => '1',
            'create_by' => '1'
        ]);
        
        Setting::create([
            'name' => 'Foto',
            'value' => 'logonewalkaca.png'
        ]);
        
        Setting::create([
            'name' => 'Nama',
            'value' => 'CV Bintang Alkaca Sejahtera'
        ]);
        
        Setting::create([
            'name' => 'Foto',
            'value' => 'logoalkaca.png'
        ]);
        
        Setting::create([
            'name' => 'Nomor',
            'value' => '1'
        ]);

        Setting::create([
            'name' => 'Rekening',
            'value' => '140-00-2045-7900'
        ]);

        Setting::create([
            'name' => 'Nama Bank',
            'value' => 'Mandiri'
        ]);

        Setting::create([
            'name' => 'Atas Nama',
            'value' => 'CV Bintang Alkaca Sejahtera'
        ]);

    }
}
