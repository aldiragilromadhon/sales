<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name',100);
            $table->string('code',200)->unique();
            $table->text('description')->nullable();
            $table->integer('category')->nullable();
            $table->integer('unit')->nullable();
            $table->integer('sell_price')->default(0);
            $table->string('sell_tax',20)->nullable();
            $table->integer('buy_price')->default(0);
            $table->string('buy_tax',20)->nullable();
            $table->integer('balance_quantity')->default(0);
            $table->integer('balance_average_price')->default(0);
            $table->date('balance_date')->nullable();
            $table->integer('buffer_quantity')->default(0);
            $table->string('image',200)->nullable();
            $table->integer('user_id');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
