<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratJalanBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_jalan_barangs', function (Blueprint $table) {
            $table->id();
            $table->integer('surat_jalan_id');
            $table->integer('pengiriman_barang_id');
            $table->decimal('tonase',12,3);
            $table->decimal('karung',12,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_jalan_barangs');
    }
}
