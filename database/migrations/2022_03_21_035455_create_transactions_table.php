<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('tipe_id');
            $table->integer('contact_id');
            $table->integer('user_id');
            $table->integer('truck_id');
            $table->string('email',100)->nullable();
            $table->text('alamat')->nullable();
            $table->text('alamat_kirim')->nullable();
            $table->string('no_parent',100)->nullable();
            $table->string('no_referensi',100)->nullable();
            $table->string('no_transaksi',200)->unique();
            $table->date('tgl_po')->nullable();
            $table->date('tgl_transaksi');
            $table->date('tgl_jatuh_tempo');
            $table->decimal('potongan',19,2);
            $table->decimal('pajak',19,2);
            $table->string('tipe_pajak')->nullable();
            $table->integer('nilai_pemotong');
            $table->decimal('uang_muka',19,2);
            $table->decimal('sisa_tagihan',19,2);
            $table->decimal('total_transaksi',19,2);
            $table->boolean('status')->default(0);
            $table->boolean('approve')->default(0);
            $table->string('user')->nullable();
            $table->text('pesan')->nullable();
            $table->text('memo')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
