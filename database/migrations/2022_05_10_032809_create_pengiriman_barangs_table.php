<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengirimanBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman_barangs', function (Blueprint $table) {
            $table->id();
            $table->integer('pengiriman_id');
            $table->string('produk',100);
            $table->decimal('tonase',12,3);
            $table->decimal('karung',12,3);
            $table->decimal('harga',12,2);
            $table->decimal('total',19,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman_barangs');
    }
}
