<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelians', function (Blueprint $table) {
            $table->id();
            $table->integer('supplier_id');
            $table->string('email_supplier',100)->nullable();
            $table->text('alamat_supplier')->nullable();
            $table->string('no_referensi',100)->nullable();
            $table->integer('user_id');
            $table->string('no_transaksi',200)->unique();
            $table->date('tgl_transaksi');
            $table->date('tgl_jatuh_tempo');
            $table->text('pesan');
            $table->text('memo');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelians');
    }
}
