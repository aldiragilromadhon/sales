<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_trucks', function (Blueprint $table) {
            $table->id();
            $table->string('perusahaan',100);
            $table->string('merek',100)->nullable();
            $table->string('no_polisi',12)->unique();
            $table->string('tahun',4)->nullable();
            $table->date('tgl_sewa')->nullable();
            $table->date('tgl_berakhir')->nullable();
            $table->string('status',2);
            $table->string('no_hp',20)->nullable();
            $table->string('foto',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent_trucks');
    }
}
