<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengirimenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengirimen', function (Blueprint $table) {
            $table->id();
            $table->integer('contact_id');
            $table->integer('user_id');
            $table->string('no_parent',100)->nullable();
            $table->string('no_referensi',100)->nullable();
            $table->string('no_transaksi',200)->unique();
            $table->integer('location_from_id');
            $table->integer('location_to_id');
            $table->decimal('subtotal',19,2);
            $table->decimal('potongan',19,2);
            $table->decimal('pajak',19,2);
            $table->string('tipe_pajak')->nullable();
            $table->decimal('total_potongan',19,2);
            $table->decimal('total_pajak',19,2);
            $table->decimal('total_transaksi',19,2);
            $table->string('detail_location_from');
            $table->string('detail_location_to');
            $table->date('tgl_po')->nullable();
            $table->date('tgl_transaksi');
            $table->date('tgl_jatuh_tempo');
            $table->boolean('status')->default(0);
            $table->boolean('approve')->default(0);
            $table->string('user')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengirimen');
    }
}
